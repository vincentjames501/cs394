To sync your database with the latest fixtures, run "runthis.bat" or "runthis.sh"

Due to how permissions are stored in fixtures, adding additional users will require
manual input in the fixtures. Regular database fixtures should not be affected.

