from django.contrib.auth.models import *
from django.contrib.contenttypes.models import ContentType

conType = ContentType.objects.get(name='user permission')

isWaiter = Permission(name='Waiter', codename='Waiter', content_type=conType)
isManager = Permission(name='Manager' , codename='Manager' , content_type=conType)
isCook = Permission(name='Cook' , codename='Cook' , content_type=conType)
isTable = Permission(name='Table' , codename='Table' , content_type=conType)

isWaiter.save()
isManager.save()
isCook.save()
isTable.save()


g1 = Group.objects.filter(name="Waiter")[0]
g2 = Group.objects.filter(name="Manager")[0]
g3 = Group.objects.filter(name="Cook")[0]
g4 = Group.objects.filter(name="Table")[0]

p1 = Permission.objects.filter(codename="Waiter")[0]
p2 = Permission.objects.filter(codename="Manager")[0]
p3 = Permission.objects.filter(codename="Cook")[0]
p4 = Permission.objects.filter(codename="Table")[0]

g1.permissions = [p1]
g2.permissions = [p2]
g3.permissions = [p3]
g4.permissions = [p4]

g1.save()
g2.save()
g3.save()
g4.save()

exit()