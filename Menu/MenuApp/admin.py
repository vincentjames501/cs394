from MenuApp.models import *
from django.contrib import admin

class RestaurantAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'street', 'city', 'state', 'zipcode', 'stateSalesTax', 'federalSalesTax')

class MenuAdmin(admin.ModelAdmin):
    list_display = ('id', 'restaurant')

class MenuPageAdmin(admin.ModelAdmin):
    list_display = ('id', 'menu','pageNumber')

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

class MenuItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'shortDescription','longDescription', 'price', 'rating', 'timesOrdered', 'image')

class NutritionFactAdmin(admin.ModelAdmin):
    list_display = ('id', 'type', 'fact')

class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'table', 'waiter', 'timePlaced', 'timeFinished', 'timePaid')

class OrderedItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'order', 'item', 'timePrepared', 'comments')

class SurveyAdmin(admin.ModelAdmin):
    list_display = ('id', 'restaurant')
    
class SurveyQuestionAdmin(admin.ModelAdmin):
    list_display = ('id', 'questionType', 'question', 'survey')

class SurveyAnswerAdmin(admin.ModelAdmin):
    list_display = ('id', 'question', 'answer', 'submissionTime')

class UserDetailAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'restaurant')

class AdditionalImageAdmin(admin.ModelAdmin):
    list_display = ('id', 'image')

admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(Menu, MenuAdmin)
admin.site.register(MenuPage, MenuPageAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(MenuItem, MenuItemAdmin)
admin.site.register(AdditionalImage, AdditionalImageAdmin)
admin.site.register(NutritionFact, NutritionFactAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderedItem, OrderedItemAdmin)
admin.site.register(Survey, SurveyAdmin)
admin.site.register(SurveyQuestion, SurveyQuestionAdmin)
admin.site.register(SurveyAnswer, SurveyAnswerAdmin)
admin.site.register(UserDetail, UserDetailAdmin)
