from django.contrib.auth.models import User, Group, Permission
from django.db import models

QUESTION_CHOICES = (
    ('T', 'Text Area'),
    ('R', 'Radio List'),
    ('C', 'Checkbox List')
)

NUTRITION_CHOICES = (
    ('n_serving_size', 'Serving Size'),
    ('n_calories', 'Calories'),
    ('n_calories_from_fat', 'Calories From Fat'),
    ('n_total_fat', 'Total Fat'),
    ('n_total_fat_percent', 'Total Fat %'),
    ('n_saturated_fat', 'Saturated Fat'),
    ('n_saturated_fat_percent', 'Saturated Fat %'),
    ('n_monounsaturated_fat', 'Monounsaturated Fat'),
    ('n_monounsaturated_fat_percent', 'Monounsaturated Fat %'),
    ('n_polyunsaturated_fat', 'Polyunsaturated Fat'),
    ('n_polyunsaturated_fat_percent', 'Polyunsaturated Fat %'),
    ('n_trans_fat', 'Trans Fat'),
    ('n_trans_fat_percent', 'Trans Fat %'),
    ('n_cholesterol', 'Cholesterol'),
    ('n_cholesterol_percent', 'Cholesterol %'),
    ('n_sodium', 'Sodium'),
    ('n_sodium_percent', 'Sodium %'),
    ('n_potassium', 'Potassium'),
    ('n_potassium_percent', 'Potassium %'),
    ('n_total_carbs', 'Total Carbs'),
    ('n_total_carbs_percent', 'Total Carbs %'),
    ('n_fiber', 'Fiber'),
    ('n_fiber_percent', 'Fiber %'),
    ('n_sugars', 'Sugars'),
    ('n_sugars_percent', 'Sugars %'),
    ('n_protein', 'Protein'),
    ('n_protein_percent', 'Protein %'),
    ('n_vitamin_a_percent', 'Vitamin A %'),
    ('n_vitamin_c_percent', 'Vitamin C %'),
    ('n_calcium_percent', 'Calcium %'),
    ('n_iron_percent', 'Iron %')
)

class UserDetail(models.Model):
    """
    Contains additional information about users in the database.
    Specifically which restaurant they belong to.
    
    Fields:
    
    user: Foreign key to the user table.
    restaurant: Foreign key to the restaurant table.
    """
    user = models.ForeignKey(User)
    restaurant = models.ForeignKey("Restaurant")
    def __unicode__(self):
        """
        Returns the username of this object.
        """
        return unicode(self.user.username)

class UserPermission(models.Model):
    """
    Serves as a database model to bind additional permissions to.
    uid: ID of the user.
    """
    uid = models.IntegerField(primary_key=True)
    def __unicode__(self):
        """
        Returns the user id (uid) of this object.
        """
        return unicode(self.uid)

class NutritionFact(models.Model):
    """
    Each NutritionFact represents a nutritional fact about a menu item.
    menuItem: foreignkey for the menu item being described.
    type: Type of fact
    fact: Fact value/description
    """
    menuItem = models.ForeignKey('MenuItem')
    type = models.CharField(max_length = 40, choices=NUTRITION_CHOICES)
    fact = models.CharField(max_length=30)
    def __unicode__(self):
        """
        Returns the fact data of this object.
        """
        return unicode(self.fact)
    def getRestaurant(self):
        """
        Returns the restaurant that this object belongs to.
        """
        return self.menuItem.category.menuPage.menu.restaurant

class MenuItem(models.Model):
    """
    Describes a food item on the menu.
    category: Foreign key for the category the item belongs to.
    name: Name of the item
    longDescription: Long description of the item
    shortDescription: Shorter description of the item
    price: The price of the item
    timesOrdered: Number of times the item has been ordered
    image: Path to the image attached to the item.
    additionalImages: Additional images attached to the item.
    """
    category = models.ForeignKey('Category')
    name = models.CharField(max_length=50)
    longDescription = models.CharField(max_length=1000, null=True, blank=True)
    shortDescription = models.CharField(max_length=300, null=True, blank=True)
    price = models.FloatField(default=0)
    rating = models.FloatField(default=0)
    timesOrdered = models.IntegerField(default=0)
    image = models.ImageField(upload_to="items/", null=True, blank=True, default="items/blank.jpg")
    additionalImages = models.ManyToManyField('AdditionalImage', null=True, blank=True)
    def __unicode__(self):
        """
        Returns this object's name.
        """
        return unicode(self.name)
    def getMenuPage(self):
        """
        Returns the menu page that this object appears on.
        """
        return self.category.menuPage
    def getMenu(self):
        """
        Returns the menu that this object exists in.
        """
        return self.category.menuPage.menu
    def getRestaurant(self):
        """
        Returns the restaurant this object belongs to.
        """
        return self.category.menuPage.menu.restaurant

class AdditionalImage(models.Model):
    """
    Model that stores additional images for menu items.
    image: Contains a path to an image uploaded on the server.
    """
    image = models.ImageField(upload_to="items/")
    def __unicode__(self):
        """
        Returns the image url of this object.
        """
        return unicode(self.image.url)

class Category(models.Model):
    """
    Model that represents the various categories that menu items fall under.
    Such as burgers, sides, or beverages.
    menuPage: Foreign key to the menu page the category belongs to.
    name: Name of the category
    """
    menuPage = models.ForeignKey('MenuPage')
    name = models.CharField(max_length=30)
    def __unicode__(self):
        """
        Returns this object's name.
        """
        return unicode(self.name)
    def getMenuPage(self):
        """
        Returns the menu page this object belongs to.
        """
        return self.menuPage
    def getMenu(self):
        """
        Returns the menu that this object exists in.
        """
        return self.menuPage.menu
    def getRestaurant(self):
        """
        Returns the restaurant this object belongs to.
        """
        return self.menuPage.menu.restaurant
    def getItems(self):
        """
        Returns a set of items that belong to this category.
        """
        return self.menuitem_set.all()
    class Meta:
        verbose_name_plural = "Categories"

class MenuPage(models.Model):
    """
    This model represents a page on a menu for the restaurant.
    menu: Foreignkey that points to the menu a page belongs.
    pageNumber: The page number of the page. This is distinct from the model id.
    """
    menu = models.ForeignKey('Menu')
    pageNumber = models.IntegerField()
    def __unicode__(self):
        """
        Returns the page number of this object.
        """
        return unicode(self.pageNumber)
    def getCategories(self):
        """
        Returns a set of categories that are on this menu page.
        """
        return self.category_set.all()
    def getRestaurant(self):
        """
        Returns the restaurant of the menu that this page belongs to.
        """
        return self.menu.restaurant

class Menu(models.Model):
    """
    This model represents a menu belonging to a particular restaurant.
    restaurant: The foreign key of the restaurant a menu belongs to.
    """
    restaurant = models.ForeignKey("Restaurant")
    def __unicode__(self):
        """
        Returns the name of the restaurant this menu belongs to.
        """
        return unicode(self.restaurant.name)
    def getMenuPages(self):
        """
        Returns a set of menu pages that belong to this menu.
        """
        return self.menupage_set.all()

class Order(models.Model):
    """
    This model represents a food order submitted by a user.
    table: foreign key to the user model. Specifically which table the order originated from.
    waiter: foreign key. Which waiter is serving this order.
    timePlaced: the time the order was placed.
    timeFinished: the time the order was finished being prepared.
    timePaid: the time the order was paid.
    totalPrice: the total price of the order
    totalDiscount: the total discount of the order
    discount: The number of discounts for the order.
    """
    table = models.ForeignKey(User)
    waiter = models.ForeignKey(UserDetail, blank=True, null=True)
    timePlaced = models.DateTimeField(auto_now_add=True)
    timeFinished = models.DateTimeField(blank=True, null=True)
    timePaid = models.DateTimeField(blank=True, null=True)
    totalPrice = models.FloatField(default=0, blank=True, null=True)
    totalDiscount = models.FloatField(default=0, blank=True, null=True)
    discount = models.IntegerField(default=0, blank=True, null=True)
    def __unicode__(self):
        """
        Returns the id of this Order object.
        """
        return unicode(self.id)
    def getOrderedItems(self):
        """
        Returns the set of OrderedItems belonging to this Order.
        """
        return self.ordereditem_set.all()

class OrderedItem(models.Model):
    """
    This modem describes a item attached to a order.
    order: Foreign key to the order this item is attached to.
    item: Foreign key to the menu item this item describes.
    timePrepared: Time the food item was prepared.
    comments: Comments attached to the food item.
    itemPrice: Price of the item
    itemPriceCharts: Price of the item
    """
    order = models.ForeignKey('Order')
    item = models.ForeignKey('MenuItem')
    timePrepared = models.DateTimeField(blank=True, null=True)
    comments = models.CharField(max_length=100, blank=True, null=True)
    itemPrice = models.FloatField(default=0, blank=True, null=True)
    itemPriceCharts = models.FloatField(default=0, blank=True, null=True)
    def __unicode__(self):
        """
        Returns the name of the menu item that this item derives from.
        """
        return unicode(self.item.name)
    def getOrder(self):
        """
        Returns the id of the order this ordereditem is in.
        """
        return unicode(self.order.id)

class Survey(models.Model):
    """
    A model describing surveys given to customers.
    restaurant: Foreign key to the restaurant a survey belongs to.
    """
    restaurant = models.ForeignKey("Restaurant")

class SurveyQuestion(models.Model):
    """
    A question that may appear on a survey.
    survey: foreign key to the survey the question belongs to.
    question: the question itself.
    questionType: the type of question.
    """
    survey = models.ForeignKey(Survey)
    question = models.TextField()
    questionType = models.CharField(max_length = 10, choices=QUESTION_CHOICES)
    def __unicode__(self):
        """
        Returns the id of this question.
        """
        return unicode(self.id)

class SurveyAnswer(models.Model):
    """
    An answer to a survey question
    question: The foreign key to the question that is answered.
    answer: The answer itself.
    submissionTime: When the answer was submitted.
    """
    question = models.ForeignKey('SurveyQuestion')
    answer = models.TextField(blank=True, null=True)
    submissionTime = models.DateTimeField(auto_now_add=True)
    def __unicode__(self):
        """
        Returns the answer of this object.
        """
        return unicode(self.answer)
    def getQuestion(self):
        """
        Returns the question that this answer answers.
        """
        return unicode(self.question)

class Restaurant(models.Model):
    """
    This model contains information pertaining to a restaurant using the system.
    name: Name of the restaurant
    street: Street address
    city: City the restaurant is in
    state: State the restaurant is in
    stateSalesTax: The sales tax of the state the restaurant is in
    federalSalesTax: The federal sales tax
    """
    name = models.CharField(max_length=50)
    street = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=2)
    zipcode = models.CharField(max_length=20)
    stateSalesTax = models.FloatField()
    federalSalesTax = models.FloatField()
    def __unicode__(self):
        """
        Returns the restaurant's name.
        """
        return unicode(self.name)
    def getMenuItems(self):
        """
        Returns the set of Menu Items belonging to this restaurant.
        """
        return MenuItem.objects.filter(category__menuPage__menu__restaurant=self)
    def getNutritionFacts(self):
        """
        Returns the set of nutrition facts belonging to this restaurant.
        """
        return NutritionFact.objects.filter(menuItem__category__menuPage__menu__restaurant=self)
    def getCategories(self):
        """
        Returns the set of categories belonging to this restaurant.
        """
        return Category.objects.filter(menuPage__menu__restaurant=self)
    def getMenu(self):
        """
        Returns the menu of this restaurant.
        """
        return Menu.objects.filter(restaurant=self)[0]
    def getMenuPages(self):
        """
        Returns the set of menu pages that belong to this restaurant's menu.
        """
        return MenuPage.objects.filter(menu__restaurant=self)
    def getRestaurantUsers(self):
        """
        Returns the set of users that belong to this restaurant.
        """
        userDetails = UserDetail.objects.filter(restaurant=self)
        users = []
        for userDetail in userDetails:
            users.append(userDetail.user)
        return users
