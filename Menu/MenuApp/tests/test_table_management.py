from django.test import TestCase
from django.test.client import Client
from MenuApp.models import *
from datetime import datetime
import time


class TableManagement(TestCase):
    fixtures = ['AdditionalImage.json', 'NutritionFact.json', 'test_setup.json', 'Category.json', 'Menu.json',
                'UserDetail.json', ' MenuItem.json', 'Restaurant.json', 'MenuPage.json', 'initial_data.json',
                'users.json']

    def setUp(self):
        """
        Setup and login
        """
        # Every test needs a client.
        self.client = Client()

        # Login with permissions.
        response = self.client.login(username='Seth', password='seth')
        self.assertTrue(response)


    def test_get_orders(self):
        """
        Testing if order comes back as expected
        """
        #Assert no orders in DB
        response = self.client.get('/Menu/tableManagement/')
        self.assertQuerysetEqual(response.context['orders'], [])

        d = datetime.now()

        #menuItems=Boca Burger, Dr. Pepper
        menuItem = MenuItem.objects.get(id=1)
        menuItem2 = MenuItem.objects.get(id=2)

        #Create order
        #Add 2 orderedItem to order.
        order = Order.objects.create(table=response.context['user'], timePlaced=d)
        OrderedItem.objects.create(order=order, item=menuItem, itemPrice=menuItem.price)
        OrderedItem.objects.create(order=order, item=menuItem2, itemPrice=menuItem2.price)

        #Assert the new order exists
        response = self.client.get('/Menu/tableManagement/')
        self.assertQuerysetEqual(response.context['orders'], ['<Order: 1>'])

        #Get the order from response and order items
        order1 = response.context['orders'][0]
        orderedItems = order1.getOrderedItems()

        #Assert orderedItems are what we think they are
        self.assertEqual(orderedItems[0].item.name, 'Boca Burger')
        self.assertEqual(orderedItems[0].itemPrice, 5.50)
        self.assertEqual(orderedItems[1].item.name, 'Dr. Pepper')
        self.assertEqual(orderedItems[1].itemPrice, 2.00)


    def test_get_order_update_single(self):
        """
        Testing if a single new order is returned after a time of last update is given.
        """
        d = datetime.now()
        resp = self.client.post('/Menu/getWaiterOrders', {'lastUpdateTime': d})

        v = resp.content

        boolean = 0

        #check if string for no orders is in the response
        if 'new_orders\": []' in v:
            boolean = 1

        self.assertEqual(1, boolean)

        #menuItems=Boca Burger, Dr. Pepper
        menuItem = MenuItem.objects.get(id=1)
        menuItem2 = MenuItem.objects.get(id=2)

        #Assert the new order exists
        response = self.client.get('/Menu/tableManagement/')


        #Create order
        #Add 2 orderedItem to order.
        order = Order.objects.create(table=response.context['user'],
            waiter=UserDetail.objects.get(user=response.context['user']))
        OrderedItem.objects.create(order=order, item=menuItem, itemPrice=menuItem.price)
        OrderedItem.objects.create(order=order, item=menuItem2, itemPrice=menuItem2.price)

        #Request again. We expect to see that there is 1 new order with items Boca Burger and Dr. Pepper
        resp = self.client.post('/Menu/getWaiterOrders', {'lastUpdateTime': d})

        v = resp.content

        boolean = 0

        #check if Boca Burger is in the response
        if 'Boca Burger' in v:
            boolean = 1

        self.assertEqual(1, boolean)
        boolean = 0

        #check if Dr. Pepper is in the response
        if 'Dr. Pepper' in v:
            boolean = 1

        self.assertEqual(1, boolean)


    def test_get_order_update_multiple(self):
        """
        Testing if multiple new orders are returned after a time of last update is given.
        """
        d = datetime.now()
        resp = self.client.post('/Menu/getWaiterOrders', {'lastUpdateTime': d})

        v = resp.content

        boolean = 0

        #check if string for no orders is in the response
        if 'new_orders\": []' in v:
            boolean = 1

        self.assertEqual(1, boolean)

        #menuItems=Boca Burger, Dr. Pepper
        menuItem = MenuItem.objects.get(id=1)
        menuItem2 = MenuItem.objects.get(id=2)
        menuItem3 = MenuItem.objects.get(id=3)
        menuItem4 = MenuItem.objects.get(id=4)

        #Assert the new order exists
        response = self.client.get('/Menu/tableManagement/')

        #Create order
        #Add 2 orderedItem to order.
        order = Order.objects.create(table=response.context['user'],
            waiter=UserDetail.objects.get(user=response.context['user']))
        OrderedItem.objects.create(order=order, item=menuItem, itemPrice=menuItem.price)
        OrderedItem.objects.create(order=order, item=menuItem2, itemPrice=menuItem2.price)

        #Create order2
        #Add 2 orderedItem to order2.
        order2 = Order.objects.create(table=response.context['user'],
            waiter=UserDetail.objects.get(user=response.context['user']))
        OrderedItem.objects.create(order=order2, item=menuItem3, itemPrice=menuItem3.price)
        OrderedItem.objects.create(order=order2, item=menuItem4, itemPrice=menuItem4.price)

        #Request again. We expect to see that there is 1 new order with items Boca Burger and Dr. Pepper
        resp = self.client.post('/Menu/getWaiterOrders', {'lastUpdateTime': d})

        v = resp.content

        boolean = 0

        #check if Boca Burger is in the response
        if 'Boca Burger' in v:
            boolean = 1

        self.assertEqual(1, boolean)
        boolean = 0

        #check if Dr. Pepper is in the response
        if 'Dr. Pepper' in v:
            boolean = 1

        self.assertEqual(1, boolean)
        boolean = 0

        #check if Bob's Steakburger is in the response
        if 'Bob\'s Steakburger' in v:
            boolean = 1

        self.assertEqual(1, boolean)
        boolean = 0

        #check if French Fries is in the response
        if 'French Fries' in v:
            boolean = 1

        self.assertEqual(1, boolean)

    def test_get_order_items(self):
        """
        Testing to make sure all ordered items for an order are returned
        """
        response = self.client.get('/Menu/tableManagement/')

        #menuItems=Boca Burger, Dr. Pepper, Bob's Steakburger, French Fries
        menuItem = MenuItem.objects.get(id=1)
        menuItem2 = MenuItem.objects.get(id=2)
        menuItem3 = MenuItem.objects.get(id=3)
        menuItem4 = MenuItem.objects.get(id=4)


        #Create order
        #Add 4 orderedItem to order.
        order = Order.objects.create(table=response.context['user'])
        OrderedItem.objects.create(order=order, item=menuItem, itemPrice=menuItem.price)
        OrderedItem.objects.create(order=order, item=menuItem2, itemPrice=menuItem2.price)
        OrderedItem.objects.create(order=order, item=menuItem3, itemPrice=menuItem3.price)
        OrderedItem.objects.create(order=order, item=menuItem4, itemPrice=menuItem4.price)

        #Assert the new order exists
        response = self.client.get('/Menu/tableManagement/')
        self.assertQuerysetEqual(response.context['orders'], ['<Order: 1>'])

        #Get the order from response and order items
        order1 = response.context['orders'][0]
        orderedItems = order1.getOrderedItems()

        #Assert orderedItems are what we think they are
        self.assertEqual(orderedItems[0].item.name, 'Boca Burger')
        self.assertEqual(orderedItems[0].itemPrice, 5.50)
        self.assertEqual(orderedItems[1].item.name, 'Dr. Pepper')
        self.assertEqual(orderedItems[1].itemPrice, 2.00)
        self.assertEqual(orderedItems[2].item.name, "Bob's Steakburger")
        self.assertEqual(orderedItems[2].itemPrice, 6.50)
        self.assertEqual(orderedItems[3].item.name, 'French Fries')
        self.assertEqual(orderedItems[3].itemPrice, 0.99)

    def test_change_item_price(self):
        """
        Testing to make sure item price changes properly
        """
        response = self.client.get('/Menu/tableManagement/')

        #menuItems=Boca Burger, Dr. Pepper
        menuItem = MenuItem.objects.get(id=1)
        menuItem2 = MenuItem.objects.get(id=2)

        #Create order
        #Add 2 orderedItem to order.
        order = Order.objects.create(table=response.context['user'])
        OrderedItem.objects.create(order=order, item=menuItem, itemPrice=menuItem.price)
        OrderedItem.objects.create(order=order, item=menuItem2, itemPrice=menuItem2.price)


        #Make sure default prices are correct
        orderedItems = Order.objects.get(id=1).getOrderedItems()
        self.assertEqual(orderedItems[0].itemPrice, 5.50)
        self.assertEqual(orderedItems[1].itemPrice, 2.00)


        #change price of first item to $2.00
        #change price of second item to $3.00
        self.client.post('/Menu/updatePrice', {'orderId': 1, 'id': 0, 'price': 2.00})
        self.client.post('/Menu/updatePrice', {'orderId': 1, 'id': 1, 'price': 3.00})

        #Make sure item prices changed as expected
        orderedItems = Order.objects.get(id=1).getOrderedItems()
        self.assertEqual(orderedItems[0].itemPrice, 2.00)
        self.assertEqual(orderedItems[1].itemPrice, 3.00)


    def test_change_item_price_negative(self):
        """
        Test to make sure ordered item cannot have a price less than zero
        """
        response = self.client.get('/Menu/tableManagement/')

        #menuItems=Boca Burger, Dr. Pepper
        menuItem = MenuItem.objects.get(id=1)
        menuItem2 = MenuItem.objects.get(id=2)

        #Create order
        #Add 2 orderedItem to order.
        order = Order.objects.create(table=response.context['user'])
        OrderedItem.objects.create(order=order, item=menuItem, itemPrice=menuItem.price)
        OrderedItem.objects.create(order=order, item=menuItem2, itemPrice=menuItem2.price)


        #Make sure default prices are correct
        orderedItems = Order.objects.get(id=1).getOrderedItems()
        self.assertEqual(orderedItems[0].itemPrice, 5.50)
        self.assertEqual(orderedItems[1].itemPrice, 2.00)


        #change price of first item to $-2.00
        resp = self.client.post('/Menu/updatePrice', {'orderId': 1, 'id': 0, 'price': -2.00})
        self.assertEqual(resp.content, '{\n    "message": "Fail: Price cannot be negative!"\n}')

        #make sure the price did not change
        orderedItems = Order.objects.get(id=1).getOrderedItems()
        self.assertEqual(orderedItems[0].itemPrice, 5.50)
        self.assertEqual(orderedItems[1].itemPrice, 2.00)


    def test_apply_coupon_to_order(self):
        """
        Testing to make sure applying a discount properly updates the correct order prices
        """
        response = self.client.get('/Menu/tableManagement')

        #Create order with totalPrice = $20
        order = Order.objects.create(table=response.context['user'], totalPrice=20)

        self.assertEqual(order.totalPrice, 20)

        #Apply 50% discount
        resp = self.client.get('/Menu/applyDiscount', {'orderId': 1, 'discount': 50})
        #Make sure request went through
        self.assertEqual(resp.status_code, 200)

        order2 = Order.objects.get(id=1)

        #Assert 50% discount and totalDiscount reflects discount
        self.assertEqual(order2.discount, 50)
        self.assertEqual(order2.totalDiscount, 10)
        self.assertEqual(order2.totalPrice, 20)


    def test_close_order_by_Id(self):
        """
        Test to make sure closing an order works
        """
        response = self.client.get('/Menu/tableManagement')
        d = datetime.now()

        #Create order with finish time
        Order.objects.create(table=response.context['user'], timeFinished=d)

        #Close order
        resp = self.client.post('/Menu/closeOrder', {'orderId': 1})
        self.assertEqual(resp.content, '{"Status": 1, "Message": "Order closed"}')


    def test_close_invalid_order_Id(self):
        """
        Test to make sure you cannot close an order with a wrong ID
        """
        #Close order with invalid
        resp = self.client.post('/Menu/closeOrder', {'orderId': 1})
        self.assertEqual(resp.content, '{"Status": 2, "Message": "No order by that Id!"}')

    def test_close_notFinished(self):
        """
        Tests to make sure an order cannot be closed until it is finished in the kitchen
        """
        response = self.client.get('/Menu/tableManagement')

        #Create order with no finish time
        Order.objects.create(table=response.context['user'])

        #Try to close order that hasn't been finished my kitchen
        resp = self.client.post('/Menu/closeOrder', {'orderId': 1})
        self.assertEqual(resp.content,
            '{"Status": 0, "Message": "Cannot close! Order has not finished in the kitchen yet!"}')

    def test_close_update(self):
        """
        Testing if closed order is returned after a time of last update is given.
        """
        d = datetime.now()
        resp = self.client.post('/Menu/getWaiterOrders', {'lastUpdateTime': d})

        v = resp.content

        boolean = 0

        #check if string for no orders is in the response
        if 'finished_orders\": []' in v:
            boolean = 1

        self.assertEqual(1, boolean)

        #Assert the new order exists
        response = self.client.get('/Menu/tableManagement/')

        #Create order with a timePaid time
        Order.objects.create(table=response.context['user'],
            waiter=UserDetail.objects.get(user=response.context['user']), timePaid=d)

        #Request again. We expect to see that there is 1 finished order
        resp = self.client.post('/Menu/getWaiterOrders', {'lastUpdateTime': d})

        v = resp.content
        boolean = 0

        #check if order id 1 is returned
        if 'id\": 1' in v:
            boolean = 1

        self.assertEqual(1, boolean)

