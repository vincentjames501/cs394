from django.test import TestCase
from django.test.client import Client

class RedirectTest(TestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()

    def test_details(self):
        # Issue a GET request.
        response = self.client.get('/Menu/index')
        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/Menu/manager')
        self.assertEqual(response.status_code, 302 )
        self.assertRedirects(response, '/Menu/login?next=/Menu/manager')

        response = self.client.get('/Menu/tableManagement')
        self.assertEqual(response.status_code, 302 )
        self.assertRedirects(response, '/Menu/login?next=/Menu/tableManagement')

        response = self.client.get('/Menu/kitchen')
        self.assertEqual(response.status_code, 302 )
        self.assertRedirects(response, '/Menu/login?next=/Menu/kitchen')

class LoginTest(TestCase):
    fixtures = ['AdditionalImage.json','NutritionFact.json', 'SurveyAnswer.json', 'Category.json', 'Order.json', 'SurveyQuestion.json', 'Menu.json', 'OrderedItem.json','UserDetail.json',' MenuItem.json','Restaurant.json','all_data.json', 'MenuPage.json','Survey.json','initial_data.json', 'users.json']

    def setUp(self):
        # Every test needs a client.
        self.client = Client()
    def main_test(self):
        response = self.client.get('/Menu/loginPage/')
        self.assertEqual(response.status_code, 200)
        self.client.login(username='Seth', password='seth')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/Menu/manager/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/Menu/tableManagement/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/Menu/kitchen/')
        self.assertEqual(response.status_code, 200)

        #log out
        self.client.logout()
        response = self.client.get('/Menu/kitchen')
        self.assertEqual(response.status_code, 302 )
        self.assertRedirects(response, '/Menu/login?next=/Menu/kitchen')

        #Check Proper Permission Accesses

        #manager (Bob)
        self.client.login(username='Bob', password='password')
        response = self.client.get('/Menu/manager')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/Menu/tableManagement')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/Menu/kitchen')
        self.assertEqual(response.stats_code, 200)
        self.client.logout()

        #Waiter (Tom)
        self.client.login(username='Tom', password='password')
        response = self.client.get('/Menu/tableManagement')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/Menu/manager')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/Menu/login?next=/Menu/manager')
        response = self.client.get('/Menu/kitchen')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/Menu/login?next=/Menu/kitchen')
        self.client.logout()


        #Cook (TestCook)
        self.client.login(username='TestCook', password='password')
        response = self.client.get('/Menu/kitchen')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/Menu/manager')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/Menu/login?next=/Menu/manager')
        response = self.client.get('/Menu/tableManagement')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/Menu/login?next=/Menu/tableManagement')
        self.client.logout()

