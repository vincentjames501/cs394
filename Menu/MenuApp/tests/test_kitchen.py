from django.test import TestCase
from django.test.client import Client
from datetime import datetime
from django.utils import simplejson
from MenuApp.models import *

"""
Tests for the Kitchen Web Application

This file contains all the tests for the kitchen functionality. The tests are described in more detailed in the test
plan document for the project.
"""

class KitchenTestNoData(TestCase):
    """
    Set of test cases that require no data besides user information.
    """
    fixtures = ['UserDetail.json','Restaurant.json', 'initial_data.json','users.json']

    def setUp(self):
        # Every test needs a client.
        self.client = Client()

        # Login with Kitchen permissions.
        response = self.client.get('/Menu/loginPage/')
        self.assertEqual(response.status_code, 200)
        response = self.client.login(username='Seth', password='seth')
        self.assertTrue(response)

    # Use Case: Discover Order Priority
    def test_kitchen_no_orders(self):
        """
        Verify no orders are being sent when none are available.
        """
        # Check that no orders are returned.
        response = self.client.get('/Menu/kitchen/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('orders' in response.context)
        self.assertTrue('currentTime' in response.context)
        self.assertQuerysetEqual(response.context['orders'], [])

    # Use Case: Determine Individual Order Items
    def test_getOrderItems_no_Id(self):
        """
        Verify the application gracefully handles when no order ID is given
        """
        response = self.client.get('/Menu/getOrderItems/')
        self.assertEqual(response.status_code, 200)

    # Use Case: Complete Order
    def test_finishOrder_with_nonexistent_Id(self):
        """
        Verify that the database is not altered when this function is given a nonexistent order ID
        """
        # Send a made up order.
        response = self.client.post('/Menu/getKitchenUpdate/', {'completed_order': 20} )
        # Verify that it handles it gracefully by performing a normal update.
        self.assertEqual(response.status_code, 200)

    def test_finishItem_with_nonexistent_Id(self):
        """
        Verify that the database is not altered when this function is given a nonexistent item ID
        """
        # Send a made up item.
        response = self.client.post('/Menu/getKitchenUpdate/', {'completed_item': 20} )
        # Verify that it handles it gracefully by performing a normal update.
        self.assertEqual(response.status_code, 200)

    def test_getKitchenUpdate_no_lastUpdateTime(self):
        """
        Verify that if no time is given for the last updated time, that the application handles it gracefully.
        """
        response = self.client.post('/Menu/getKitchenUpdate/' )
        self.assertEqual(response.status_code, 200)

    def test_getKitchenUpdate_no_orderId(self):
        """
        Verify that if no order ID is given, the application performs a normal update
        """
        time = datetime.now()
        response = self.client.post('/Menu/getKitchenUpdate/', {'lastUpdateTime': time})
        self.assertEqual(response.status_code, 200)

    def test_getKitchenUpdate_no_itemId(self):
        """
        Verify that if no item ID is given, the application performs a normal update
        """
        time = datetime.now()
        response = self.client.post('/Menu/getKitchenUpdate/', {'lastUpdateTime': time})
        self.assertEqual(response.status_code, 200)

    # Use Case: Reference A Completed Order
    def test_getCompletedOrders_none(self):
        """
        Check that no completed orders are sent when none are available
        """
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context, None)


class KitchenTestSingleData(TestCase):
    """
    Set of test cases that initially require no data besides user information. Order and Order item data is added
    per test case when needed.
    """
    fixtures = ['AdditionalImage.json', 'NutritionFact.json', 'test_setup.json', 'Category.json', 'Menu.json',
                'UserDetail.json', ' MenuItem.json', 'Restaurant.json', 'MenuPage.json', 'initial_data.json',
                'users.json']

    def setUp(self):
        # Every test needs a client.
        self.client = Client()
        # Login with Kitchen permissions.
        response = self.client.get('/Menu/loginPage/')
        self.assertEqual(response.status_code, 200)
        response = self.client.login(username='Seth', password='seth')
        self.assertTrue(response)

    # Use Case: Discover Order Priority
    def test_kitchen_one_order(self):
        """
        Verify a single order is sent correctly.
        """
        response = self.client.get('/Menu/kitchen/')
        self.assertEqual(response.status_code, 200)

        # Check that nothing is received with no orders.
        self.assertQuerysetEqual(response.context['orders'], [])

        # Create an order.
        time = datetime.now()
        currentUser = response.context['user']
        menuItem = MenuItem.objects.get(id=1)
        menuItem2 = MenuItem.objects.get(id=2)
        order = Order.objects.create(table=currentUser, waiter=UserDetail.objects.get(user=currentUser), timePlaced=time)
        OrderedItem.objects.create(order=order, item=menuItem, itemPrice=menuItem.price)
        OrderedItem.objects.create(order=order, item=menuItem2, itemPrice=menuItem2.price)

        # Check that the added order is now sent.
        response = self.client.get('/Menu/kitchen/')
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['orders'], ['<Order: 1>'])


    # Use Case: Determine Individual Order Items
    def test_getOrderItems_single_item(self):
        """
        Verify that a single item is being sent for an order with only one item
        """
        # Add an order.
        response = self.client.get('/Menu/kitchen/')
        time = datetime.now()
        currentUser = response.context['user']
        order = Order.objects.create(table=currentUser, waiter=UserDetail.objects.get(user=currentUser), timePlaced=time)

        # Check that no items are received.
        response = self.client.get('/Menu/getOrderItems/', {'order_id': 1})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, '[]')

        # Add an item to the order.
        menuItem = MenuItem.objects.get(id=1)
        OrderedItem.objects.create(order=order, item=menuItem, itemPrice=menuItem.price)

        # Check that it is received.
        response = self.client.get('/Menu/getOrderItems/', {'order_id': 1})
        self.assertEqual(response.status_code, 200)
        # Load the response.
        json_recvd = simplejson.loads(response.content)
        json1 = simplejson.dumps(
            [
                    {
                    "time_prepared": "Not Prepared",
                    "id": 1,
                    "comments": None,
                    "name": "Boca Burger"
                    },
            ]
        )
        json2 = simplejson.loads(json1)
        self.assertEqual(json_recvd[0], json2[0])


    # Use Case: Reference A Completed Order
    def test_getCompletedOrders_one(self):
        """
        Check that one completed order is sent when one is available
        """
        response = self.client.get('/Menu/kitchen/')
        self.assertEqual(response.status_code, 200)

        # Check that nothing is received with no orders.
        self.assertQuerysetEqual(response.context['orders'], [])

        # Create an order.
        time = datetime.now()
        currentUser = response.context['user']
        menuItem = MenuItem.objects.get(id=1)
        menuItem2 = MenuItem.objects.get(id=2)
        order = Order.objects.create(table=currentUser, waiter=UserDetail.objects.get(user=currentUser), timePlaced=time)
        OrderedItem.objects.create(order=order, item=menuItem, itemPrice=menuItem.price)
        OrderedItem.objects.create(order=order, item=menuItem2, itemPrice=menuItem2.price)

        # Complete the order.
        response = self.client.post('/Menu/getKitchenUpdate/', {'lastUpdateTime' : time, 'completed_order' : 1} )
        self.assertEqual(response.status_code, 200)

        # Check that it is retrieved.
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertContains(response, '"id": 1')

class KitchenTestFullData(TestCase):
    """
    Set of test cases that require all data to be present.
    """
    fixtures = ['AdditionalImage.json','NutritionFact.json', 'Category.json', 'Order.json', 'Menu.json',
                'OrderedItem.json','UserDetail.json',' MenuItem.json','Restaurant.json','all_data.json',
                'MenuPage.json', 'initial_data.json', 'users.json']

    def setUp(self):
        # Every test needs a client.
        self.client = Client()
        # Login with Kitchen permissions.
        response = self.client.get('/Menu/loginPage/')
        self.assertEqual(response.status_code, 200)
        response = self.client.login(username='Seth', password='seth')
        self.assertTrue(response)

    # Use Case: Discover Order Priority
    def test_kitchen_many_orders(self):
        """
        Verify that orders from the user's restaurant, that are not finished, are being sent, along with the
        current time.
        """
        # Check that the correct orders and timestamp are returned.
        response = self.client.get('/Menu/kitchen/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('orders' in response.context)
        self.assertTrue('currentTime' in response.context)
        self.assertQuerysetEqual(response.context['orders'], ['<Order: 4>', '<Order: 5>', '<Order: 6>', '<Order: 8>', '<Order: 9>'])
        self.assertIsNot(response.context['currentTime'], [])

    # Use Case: Determine Individual Order Items
    def test_getOrderItems_multiple_items(self):
        """
        Verify that multiple items are being sent for orders with multiple items.
        """
        # Get the items for the order #3.
        response = self.client.get('/Menu/getOrderItems/', {'order_id': 4})
        self.assertEqual(response.status_code, 200)
        # Load the response.
        json_recvd = simplejson.loads(response.content)
        # Prepare a response to test it against.
        json1 = simplejson.dumps(
            [
                {
                    "time_prepared": "Apr. 22, 2012, 03:57 PM",
                    "id": 8,
                    "comments": "Extra cheese",
                    "name": "Bob's Steakburger"
                },
                    {
                    "time_prepared": "Apr. 22, 2012, 03:57 PM",
                    "id": 14,
                    "comments": "",
                    "name": "French Fries"
                },
                    {
                    "time_prepared": "Not Prepared",
                    "id": 15,
                    "comments": "",
                    "name": "Dr. Pepper"
                },
                    {
                    "time_prepared": "Apr. 22, 2012, 03:57 PM",
                    "id": 16,
                    "comments": "",
                    "name": "Curly Fries"
                }
            ]
        , )
        json2 = simplejson.loads(json1)

        # Test the received json vs the test json.
        for num in range(1,3):
            self.assertEqual(json_recvd[num], json2[num])

    # Use Case: Complete Order
    def test_getKitchenUpdate_complete_order(self):
        """
        Verify that a sent order ID causes the order to be finished and the finished order ID is sent out.
        """
        time = datetime.now()
        # Verify order #3 is not in the completed orders.
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('"id": 4', response.content)

        # Complete order #3
        response = self.client.post('/Menu/getKitchenUpdate/', {'lastUpdateTime' : time, 'completed_order' : 4} )
        self.assertEqual(response.status_code, 200)

        # Verify it is now in completed orders.
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '"id": 4')

    def test_finishItem_does_not_complete_order(self):
        """
        Verify that when an item is finished, then time prepared in properly updated and if the item is not the
        last of the order, that it does not finish the order
        """
        time = datetime.now()
        # Verify order #3 is not in the completed orders.
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('"id": 4', response.content)

        # Complete an item that belongs to order #3.
        response = self.client.post('/Menu/getKitchenUpdate/', {'lastUpdateTime' : time, 'completed_item' : 11})
        self.assertEqual(response.status_code, 200)

        # Verify order #3 is still not in the completed orders.
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('"id": 4', response.content)

    def test_finishItem_completes_order(self):
        """
        Verify that when an item is finished, then time prepared in properly updated and if the item is the last
        item in the order, that it also properly updates the time finished of the order
        """
        time = datetime.now()
        # Verify order #3 is not in the completed orders.
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('"id": 4', response.content)

        # Complete all items belonging to order #3.
        response = self.client.post('/Menu/getKitchenUpdate/', {'lastUpdateTime' : time, 'completed_item' : 15})
        self.assertEqual(response.status_code, 200)

        # Verify order #3 is now in completed orders.
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '"id": 4')

    def test_finishOrder(self):
        """
        Verify that the order timeFinished is properly updated.
        """
        time = datetime.now()
        # Verify order #3 is not in the completed orders.
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('"id": 4', response.content)

        # Complete order #3
        response = self.client.post('/Menu/getKitchenUpdate/', {'lastUpdateTime' : time, 'completed_order' : 4} )
        self.assertEqual(response.status_code, 200)

        # Verify it is now in completed orders.
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '"time_finished": ')

    def test_getKitchenUpdate_complete_item(self):
        """
        Verify that a sent item ID causes the item to be finished and that its ID is sent back out
        """
        time = datetime.now()

        # Complete an item.
        response = self.client.post('/Menu/getKitchenUpdate/', {'lastUpdateTime' : time, 'completed_item' : 11})
        self.assertEqual(response.status_code, 200)

        # Verify that the finished item is sent back to the sender.
        self.assertContains(response, '"id": 11')

    # Use Case: Reference A Completed Order
    def test_getCompletedOrders_multiple(self):
        """
        Check that multiple completed orders are sent when they are available.
        """
        time = datetime.now()
        # Verify order #3 is not in the completed orders.
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('"id": 4', response.content)

        # Verify order #4 is not in the completed orders.
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('"id": 5', response.content)

        # Complete order #3 and order #4.
        response = self.client.post('/Menu/getKitchenUpdate/', {'lastUpdateTime' : time, 'completed_order' : 4} )
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/Menu/getKitchenUpdate/', {'lastUpdateTime' : time, 'completed_order' : 5} )
        self.assertEqual(response.status_code, 200)

        # Verify order #3 and #4 are now in completed orders.
        response = self.client.get('/Menu/getCompletedOrders/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '"id": 4')
        self.assertContains(response, '"id": 5')
