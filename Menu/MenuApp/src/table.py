from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils import simplejson
from MenuApp.models import *


@login_required(login_url="/Menu/login")
@permission_required("MenuApp.Table",login_url="/Menu/login")
def getMenu(request):
    detailsForUser = UserDetail.objects.filter(user__username=request.user.username)
    if not detailsForUser:
        return HttpResponse(simplejson.dumps({"message": "Could not find associated restaurant"}),
            mimetype="application/json")
    restaurant = detailsForUser[0].restaurant
    json = simplejson.dumps([{'name': restaurant.name,
                              'menus': [{'menuPages': [{'pageNumber': menuPage.pageNumber, 'categories': [
                                  {'name': category.name, 'menuItems': [
                                  {'id':menuItem.id,'name': menuItem.name, 'shortDescription': menuItem.shortDescription,'longDescription': menuItem.longDescription,
                                   'price': unicode(menuItem.price), 'rating': unicode(menuItem.rating),
                                   'image': menuItem.image.url,
                                   'additionalImages': [{'image': additionalImage.image.url} for additionalImage in
                                                                                             menuItem.additionalImages.all()]
                                  , 'timesOrdered': menuItem.timesOrdered,
                                   'nutritionFacts': [{'type': fact.type, 'fact': fact.fact} for fact in
                                                                                             menuItem.nutritionfact_set.all()]}
                              for menuItem in category.menuitem_set.all()]} for category in
                                                                            menuPage.category_set.all()]} for menuPage
                                                                                                          in
                                                                                                          menu.menupage_set.all()]}
                              for menu in restaurant.menu_set.all()]

    }], indent=4)
    return HttpResponse(json, mimetype="application/json")

@login_required(login_url="/Menu/login")
@permission_required("MenuApp.Table",login_url="/Menu/login")
def getAdditionalImages(request):
    itemNumber = request.GET["itemNumber"]
    item = MenuItem.objects.get(id=itemNumber)
    additionalImages = item.additionalImages.all()
    json = simplejson.dumps([{"src":additionalImage.image.url} for additionalImage in additionalImages], indent=4)
    return HttpResponse(json, mimetype="application/json")

@login_required(login_url="/Menu/login")
@permission_required("MenuApp.Table",login_url="/Menu/login")
def getMenuItemNutritionFacts(request):
    itemNumber = request.GET["itemNumber"]
    item = MenuItem.objects.get(id=itemNumber)
    nutritionFacts = item.nutritionfact_set.all()
    json = simplejson.dumps([{"type":nutritionFact.type, "fact":nutritionFact.fact} for nutritionFact in nutritionFacts], indent=4)
    return HttpResponse(json, mimetype="application/json")

def getRestaurantFromRequest(request):
    userDetail = UserDetail.objects.get(user=request.user)
    if userDetail:
        return userDetail.restaurant
    return None

@login_required(login_url="/Menu/login")
@permission_required("MenuApp.Table",login_url="/Menu/login")
def menu(request):
    restaurant = getRestaurantFromRequest(request)
    menu = restaurant.getMenu()
    return render_to_response('menu.html', RequestContext(request, {"menu":menu}))


