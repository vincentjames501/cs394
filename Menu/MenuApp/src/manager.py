from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse
from django.template.context import RequestContext
from django.utils import simplejson
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from MenuApp.models import *
from django import forms
import datetime

""" Manager web application functions

This module handles the various requests from the web application including:
Querying the database and returning JSON structures.
Taking POST data from the web application and updating the database.
Uploading images submitted from the web application.
    
"""

UPLOADPATH = "media/items/"

class UploadImageForm(forms.Form):
    """
    This class binds an uploaded file to a form so that it may be validated.
    """
    id = forms.IntegerField()
    image = forms.ImageField()

@login_required(login_url="/Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def charts(request):
    """
    Renders the charts webpage to the web applicaton.
    """
    return render_to_response('charts.html', RequestContext(request))

@login_required(login_url="/Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def manager(request):
    """
    Renders the manager webpage to the web application.
    """
    return render_to_response('manager.html', RequestContext(request))

@login_required(login_url="/Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def filterByDate(request):
    """
    Filters chart/statistic data by start and end dates
    sent by the web application. Returns a new JSON object
    to the webpage.
    """
    startDate=datetime.datetime.strptime(request.GET["startDate"], "%m/%d/%Y")
    endDate=datetime.datetime.strptime(request.GET["endDate"] + ' 23:59:59', "%m/%d/%Y %H:%M:%S")
    orders = OrderedItem.objects.filter(order__timePaid__gte = startDate).filter(order__timePaid__lte = endDate)
    json = simplejson.dumps( [{
        'Category': order.item.category.name, 'Item': order.item.name, 'Price': str(order.itemPriceCharts), 'Date': str(order.order.timeFinished), 'Waiter': str(order.order.waiter)
    } for order in orders], indent=4 )
    return HttpResponse(json, mimetype="application/json")

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def userManagement(request):
    """
    Renders the userManagement webpage to the webapplication.
    """
    return render_to_response('userManagement.html', RequestContext(request))

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
@csrf_exempt
def downloadCSV(request):
    """
    Handles the creation of a comma separated value file
    for the user to download. Returns the attachment to the webpage.
    """
    response = HttpResponse(content=request.POST['data'],mimetype='application/force-download')
    response['Content-Disposition'] = 'attachment; filename='+getRestaurantFromRequest(request).name+'.csv'
    return response

def getRestaurantFromRequest(request):
    """
    Obtains the restaurant by checking the foreign key of
    the passed user from the webpage request. Returns None if not found,
    otherwise returns the restaurant object.
    """
    userDetail = UserDetail.objects.get(user=request.user)
    if userDetail:
        return userDetail.restaurant
    return None

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def getUsers(request):
    """
    Queries the database for the list of users for the current
    restaurant. Returns a JSON object to the webpage containing
    the information of the each user belonging to the restaurant.
    """
    restaurant = getRestaurantFromRequest(request)
    users = restaurant.getRestaurantUsers()
    json = simplejson.dumps({"rows":[{"id":user.id, "name":user.username, "type":user.groups.all()[0].name if len(user.groups.all())>0 else ""} for user in users]}, indent=4)
    return HttpResponse(json, mimetype="application/json")

@csrf_exempt
@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def updateUser(request):
    """
    Takes the POST values from the web application to update a user in
    the database. Three different options are supported:
    Edit: Edits user information
    Add: Adds a new user
    Delete: Deletes a user

    Returns a success message to the webpage if successful.
    """
    restaurant = getRestaurantFromRequest(request)
    operation = request.POST['oper']
    json = simplejson.dumps({"message":"success"}, indent=4)
    if operation == "edit":
        user = User.objects.filter(id=request.POST['id'])[0]
        user.username= request.POST['name']
        if request.POST['password']:
            user.set_password(request.POST['password'])
        user.save()
        if len(user.groups.all()) > 0:
            g = Group.objects.get(name=user.groups.all()[0])
            g.user_set.remove(user)
            g.save()
        g = Group.objects.get(name=request.POST['type'])
        g.user_set.add(user)
        g.save()
    elif operation == "del":
        User.objects.filter(id=request.POST['id'])[0].delete()
    elif operation == "add":
        user = User.objects.create_user(username=request.POST['name'], email='', password=request.POST['password'])
        user.save()
        g = Group.objects.get(name=request.POST['type'])
        g.user_set.add(user)
        g.save()
        UserDetail(user=user, restaurant=restaurant).save()
        json = simplejson.dumps({"message":"success", "id":user.id}, indent=4)
    return HttpResponse(json, mimetype="application/json")

@csrf_exempt
@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def updateMenuPage(request):
    """
    Takes the POST values from the web application to update a menu page in
    the database. Three different options are supported:
    Edit: Edits a menu page.
    Add: Adds a new menu page.
    Delete: Deletes a menu page.
    
    Returns a success message to the webpage if successful.
    """
    restaurant = getRestaurantFromRequest(request)
    operation = request.POST['oper']
    json = simplejson.dumps({"message":"success"}, indent=4)
    if operation == "edit":
        menuPage = MenuPage.objects.get(id=request.POST['id'])
        menuPage.pageNumber= request.POST['pageNumber']
        menuPage.save()
    elif operation == "del":
        MenuPage.objects.get(id=request.POST['id']).delete()
    elif operation == "add":
        menu = restaurant.getMenu()
        menuPage = MenuPage(menu=menu, pageNumber=request.POST['pageNumber'])
        menuPage.save()
        json = simplejson.dumps({"message":"success", "id":menuPage.id}, indent=4)
    return HttpResponse(json, mimetype="application/json")

@csrf_exempt
@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def updateCategory(request):
    """
    Takes the POST values from the web application to update a category
    in the database. Three different options are supported:
    Edit: Edits a category.
    Add: Adds a new category.
    Delete: Deletes a category.
    
    Returns a success message to the webpage if successful.
    """
    restaurant = getRestaurantFromRequest(request)
    operation = request.POST['oper']
    json = simplejson.dumps({"message":"success"}, indent=4)
    if operation == "edit":
        category = Category.objects.get(id=request.POST['id'])
        category.menuPage = MenuPage.objects.get(menu__restaurant=restaurant, pageNumber=request.POST['pageNumber'])
        category.name = request.POST['name']
        category.save()
    elif operation == "del":
        Category.objects.get(id=request.POST['id']).delete()
    elif operation == "add":
        menuPage = MenuPage.objects.get(menu__restaurant=restaurant, pageNumber=request.POST['pageNumber'])
        category = Category(menuPage=menuPage, name=request.POST['name'])
        category.save()
        json = simplejson.dumps({"message":"success", "id":category.id}, indent=4)
    return HttpResponse(json, mimetype="application/json")

@csrf_exempt
@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def uploadImage(request):
    """
    Handles the uploading of an image.
    First checks to see if an image was uploaded, if so, it checks
    to make sure the file is an image of some format (jpg/png/gif).
    If the image is validated, the uploaded attachment is written by
    calling uploadFile(). The path of the corresponding menu item
    is also updated to point at the newly uploaded file.
    
    Returns a success message to the webpage if successful.
    """

    json = simplejson.dumps({'message':'success'})
    
    # print request.POST
    # print request.FILES
    # Handle upload if image was submitted
    if 'image' in request.FILES:
        # Check to make sure uploaded image is actually an image
        # This will verify beyond checking the file extension.
        form = UploadImageForm(request.POST, request.FILES)
        if form.is_valid():
            menuItem = MenuItem.objects.get(id=request.POST['id'])
            uploadFile(request.FILES['image'])
            menuItem.image = '/media/items/' + request.FILES['image'].name
            menuItem.save()
            json = simplejson.dumps({"message":"success", "id":menuItem.id}, indent=4)   
    return HttpResponse(json, mimetype="application/json")

def uploadFile(f):
    """
    Takes a file passed in as 'f' and writes it to /media/items/
    Writes the file chunk by chunk.
    """
    name = f.name
    filePath = UPLOADPATH + name
    destination = open(filePath, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()
    
@csrf_exempt
@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def updateMenuItems(request):
    """
    Takes the POST values from the web application to update a menu item
    in the database. Three different options are supported:
    Edit: Edits a menu item.
    Add: Adds a new menu item.
    Delete: Deletes a menu item.
    
    Returns a success message to the webpage if successful.
    """
    
    restaurant = getRestaurantFromRequest(request)
    operation = request.POST['oper']
    json = simplejson.dumps({"message":"success"}, indent=4)

    if operation == "edit":
        menuItem = MenuItem.objects.get(id=request.POST['id'])
        menuItem.category = Category.objects.get(id=request.POST['categoryName'])
        menuItem.name = request.POST['name']
        menuItem.shortDescription = request.POST['description']
        menuItem.longDescription = request.POST['longDescription']
        menuItem.price = request.POST['price']
        menuItem.rating = request.POST['rating']
        menuItem.timesOrdered = request.POST['timesOrdered']
        menuItem.save()
        json = simplejson.dumps({"message":"success", "id":menuItem.id}, indent=4)
    elif operation == "del":
        MenuItem.objects.get(id=request.POST['id']).delete()
    elif operation == "add":
        category = Category.objects.get(id=request.POST["categoryName"])
        menuItem = MenuItem(category=category, name=request.POST['name'], shortDescription=request.POST['description'], longDescription=request.POST['longDescription'] ,price=request.POST['price'], rating=request.POST['rating'], timesOrdered=request.POST['timesOrdered'])
        menuItem.save()
        json = simplejson.dumps({"message":"success", "id":menuItem.id}, indent=4)
    return HttpResponse(json, mimetype="application/json")

@csrf_exempt
@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def updateNutritionFacts(request):
    """
    Takes the POST values from the web application to update a menu item's
    nutritional fact in the database. Three different options are supported:
    Edit: Edits a nutritional fact.
    Add: Adds a nutritional fact.
    Delete: Deletes a nutritional fact.
    
    Returns a success message to the webpage if successful.
    """
    restaurant = getRestaurantFromRequest(request)
    operation = request.POST['oper']
    json = simplejson.dumps({"message":"success"}, indent=4)

    if operation == "edit":
        nutritionFact = NutritionFact.objects.get(id=request.POST['id'])
        nutritionFact.menuItem = MenuItem.objects.get(id=request.POST['menuItemName'])
        nutritionFact.type = request.POST['type']
        nutritionFact.fact = request.POST['fact']
        nutritionFact.save()
    elif operation == "del":
        NutritionFact.objects.get(id=request.POST['id']).delete()
    elif operation == "add":
        menuItem = MenuItem.objects.get(id=request.POST['menuItemName'])
        nutritionFact = NutritionFact(menuItem=menuItem, type=request.POST['type'], fact=request.POST['fact'])
        nutritionFact.save()
        json = simplejson.dumps({"message":"success", "id":nutritionFact.id}, indent=4)
    return HttpResponse(json, mimetype="application/json")

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def menuManagement(request):
    """
    Renders the menuManagement page to the web applicaton.
    """
    return render_to_response('menuManagement.html', RequestContext(request, {"menu": Menu.objects.filter(restaurant = getRestaurantFromRequest(request))}))

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def addMenu(request):
    """
    Adds a menu to the restaurant.
    """
    restaurant = getRestaurantFromRequest(request)
    Menu(restaurant = restaurant).save()
    json = simplejson.dumps({'message':'success'})
    return HttpResponse(json, mimetype="application/json")

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def deleteMenu(request):
    """
    Deletes the menu from a given restaurant.
    Purges all data such as menu items attached to the menu.
    """
    restaurant = getRestaurantFromRequest(request)
    Menu.objects.get(restaurant=restaurant).delete()
    json = simplejson.dumps({'message':'success'})
    return HttpResponse(json, mimetype="application/json")

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def menuPageManagement(request):
    """
    Renders the menuPageManagement page to the web application.
    """
    return render_to_response('menuPageManagement.html', RequestContext(request, {"menuPages": Menu.objects.get(restaurant = getRestaurantFromRequest(request)).getMenuPages()}))

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def getMenuPages(request):
    """
    Queries the database for menu pages for the given restaurant.
    Returns a JSON object of the menu pages obtained.
    """
    menuPages = getRestaurantFromRequest(request).getMenuPages()
    json = simplejson.dumps({"rows":[{"id":menuPage.id, "pageNumber":menuPage.pageNumber} for menuPage in menuPages]}, indent=4)
    return HttpResponse(json, mimetype="application/json")

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def getCategories(request):
    """
    Queries the database for categories for the given restaurant.
    Returns a JSON object of the categories obtained.
    """
    restaurant = getRestaurantFromRequest(request)
    menuPages = restaurant.getMenuPages()
    categories = restaurant.getCategories()
    pageNumbers = {}
    for menuPage in menuPages:
        pageNumbers[menuPage.pageNumber] = menuPage.pageNumber
    json = simplejson.dumps({"rows":[{"id":category.id, "pageNumber":category.menuPage.pageNumber, 'name':category.name} for category in categories], 'pageNumbers':pageNumbers}, indent=4)
    return HttpResponse(json, mimetype="application/json")

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Table",login_url="/Menu/login")
@csrf_exempt
def createNewOrder(request):
    """
    Creates a new order with information contained in the POST fields from
    the web application.
    Saves the new order in the database.
    """
    orderItems = simplejson.loads(request.POST["orderItems"])
    order = Order(table=request.user, waiter=UserDetail.objects.get(user=request.user))
    order.save()
    tPrice = 0

    for orderItem in orderItems:
        for i in range(int(orderItem["quantity"])):
            menuItem = MenuItem.objects.filter(id=orderItem["id"])[0]
            OrderedItem(order=order, item=menuItem, comments=orderItem["comments"], itemPrice=menuItem.price, itemPriceCharts=menuItem.price).save()
            tPrice +=menuItem.price


    order.totalPrice = tPrice
    order.save()
    return HttpResponse(simplejson.dumps({"Message":"Thank you for your order"}), mimetype="application/json")

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def categoryManagement(request):
    """
    Returns the categoryManagement webpage to the web application.
    """
    return render_to_response('categoryManagement.html', RequestContext(request))

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def getMenuItems(request):
    """
    Queries the database for the menu items belonging to categories in
    the restaurant.

    Returns a JSON object containing the menu items along with a list
    of categories the menu items belong to.
    """
    restaurant = getRestaurantFromRequest(request)
    categories = restaurant.getCategories()
    menuItems = restaurant.getMenuItems()
    itemCategory = {}

    for category in categories:
        itemCategory[category.id] = category.name
        
    json = simplejson.dumps({"rows":[{"id":menuItem.id, "categoryID":menuItem.category.id, "categoryName":menuItem.category.name, 'name':menuItem.name, 'description':menuItem.shortDescription, 'longDescription':menuItem.longDescription,'price':menuItem.price, 'rating':menuItem.rating, 'timesOrdered':menuItem.timesOrdered, 'image':menuItem.image.url} for menuItem in menuItems], 'itemCategory':itemCategory}, indent=4)
    return HttpResponse(json, mimetype="applicaton/json")
    
@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def getNutritionFacts(request):
    """
    Queries the database for the nutritional facts belonging to menu items
    in the restaurant.

    Returns a JSON object containing the nutritional facts, along with
    a list of menu items that they belong to.
    """
    restaurant = getRestaurantFromRequest(request)
    nutritionFacts = restaurant.getNutritionFacts()
    menuItems = restaurant.getMenuItems()
    item = {}

    for menuItem in menuItems:
        item[menuItem.id] = menuItem.name

    json = simplejson.dumps({"rows":[{"id":nutritionFact.id, "menuItemID":nutritionFact.menuItem.id, 'menuItemName':nutritionFact.menuItem.name, 'type':nutritionFact.type,'fact':nutritionFact.fact} for nutritionFact in nutritionFacts], 'item':item}, indent=4)
    return HttpResponse(json, mimetype="application/json")

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def menuItemManagement(request):
    """
    Returns the menuItemManagement page to the web applicaton.
    """
    return render_to_response('menuItemManagement.html', RequestContext(request))

@login_required(login_url="Menu/login")
@permission_required("MenuApp.Manager",login_url="/Menu/login")
def nutritionFactManagement(request):
    """
    Returns the nutritionFactManagement webpage to the web application
    """
    return render_to_response('nutritionFactManagement.html', RequestContext(request))
