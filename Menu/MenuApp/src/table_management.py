from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from MenuApp.models import *
from django.views.decorators.csrf import csrf_exempt
from django.utils import simplejson
from datetime import datetime

"""
Table Management Web Application view functions

This view handles requests for the table management page. It contains functions that:
Render the page
Keep the page up-to-date automatically
Edit order's prices and apply discounts
"""


@login_required(login_url="/Menu/login")
@permission_required("MenuApp.Waiter", login_url="/Menu/login")
def waiter(request):
    """
    Renders the initial data for the table management.
    """
    restaurant = UserDetail.objects.filter(user=request.user)[0].restaurant

    # Gather all the order items for a given restaurant.
    order_items = OrderedItem.objects.filter(item__category__menuPage__menu__restaurant=restaurant)

    orders = Order.objects.filter(timePaid__isnull=True,
        id__in=[item.order.id for item in order_items]).order_by('timePlaced')

    currentTime = str(datetime.now())

    return render_to_response('tableManagement.html',
        RequestContext(request, {'orders': orders, 'order_items': order_items, 'currentTime': currentTime}))



@login_required(login_url="Menu/login")
@csrf_exempt
@permission_required("MenuApp.Waiter", login_url="/Menu/login")
def getWaiterOrders(request):
    """
    Returns JSON of all new orders placed after timeOfLastUpdate,
    and all orders finished after timeOfLastUpdate
    """
    restaurant = UserDetail.objects.filter(user=request.user)[0].restaurant

    # Read the timestamp received. This will be used to determine
    #   what new orders have arrived.
    timeOfLastUpdate = datetime.strptime(request.POST['lastUpdateTime'], '%Y-%m-%d %H:%M:%S.%f')

    # Retrieve all the orders for the restaurant, that are not finished, and have been placed since
    #   the last update.
    new_orders = Order.objects.filter(timePaid__isnull=True, waiter__restaurant=restaurant,
        timePlaced__gte=timeOfLastUpdate)

    # Generate a timestamp for when you got this update.
    updateTime = datetime.now()

    # Retrieve all orders for the restaurant, that have been finished since the last update.
    finished_orders = Order.objects.filter(waiter__restaurant=restaurant,
        timePaid__gte=timeOfLastUpdate)

    json = simplejson.dumps({
        "new_orders": [
            {
            "id": order.id,
            "discount": order.discount,
            "totalDiscount": "%0.2f" % order.totalDiscount,
            "totalPrice": "%0.2f" % order.totalPrice,
            "waiter": order.waiter.user.username,
            "items": [{
                "item": orderedItem.item.name,
                "price": orderedItem.itemPrice,
                "orderID": order.id
            } for orderedItem in order.getOrderedItems()]
        } for order in new_orders],
        "finished_orders": [
            {
            "id": order.id
        } for order in finished_orders],
        "updateTime": str(updateTime)},
        indent=4)

    return HttpResponse(json, mimetype="application/json")



@login_required(login_url="Menu/login")
@permission_required("MenuApp.Waiter", login_url="/Menu/login")
def getOrderIds(request):
    """
    Returns JSON with order id's of all unpaid orders
    """
    restaurant = UserDetail.objects.filter(user=request.user)[0].restaurant

    # Gather all the order items for a given restaurant.
    order_items = OrderedItem.objects.filter(item__category__menuPage__menu__restaurant=restaurant)

    orders = Order.objects.filter(timePaid__isnull=True,
        id__in=[item.order.id for item in order_items]).order_by('timePlaced')

    json = simplejson.dumps({
        "items": [
            {
            "id": order.id,
            } for order in orders]},
        indent=4)

    return HttpResponse(json, mimetype="application/json")


@login_required(login_url="Menu/login")
@csrf_exempt
@permission_required("MenuApp.Waiter", login_url="/Menu/login")
def closeOrder(request):
    """
    Closes an order based on the orderId passed from the request.
    Updates the timePaid for the order
    """
    d = datetime.now()

    #Make sure order exists
    try:
        order = Order.objects.get(id=request.POST['orderId'])
    except Order.DoesNotExist:
        return HttpResponse(simplejson.dumps({"Message": "No order by that Id!", "Status": 2}),
            mimetype="application/json")

    if order.timeFinished:
        order.timePaid = d
        order.save()
        return HttpResponse(simplejson.dumps({"Message": "Order closed", "Status": 1}), mimetype="application/json")
    else:
        return HttpResponse(
            simplejson.dumps({"Message": "Cannot close! Order has not finished in the kitchen yet!", "Status": 0}),
            mimetype="application/json")


@login_required(login_url="Menu/login")
@csrf_exempt
@permission_required("MenuApp.Waiter", login_url="/Menu/login")
def getOrderById(request):
    """
    Returns JSON of requested order information
    """
    order_items = OrderedItem.objects.filter(order__id=request.GET['id'])
    order = Order.objects.get(id=request.GET['id'])

    json = simplejson.dumps({"order": [
        {
        "id": item.item_id,
        "totalPrice": order.totalPrice,
        "discount": order.discount,
        "totalDiscount": order.totalDiscount,
        "name": item.item.name,
        "price": item.itemPrice,
        "orderId": item.order_id
    } for item in order_items]},
        indent=4)

    return HttpResponse(json, mimetype="application/json")


@login_required(login_url="Menu/login")
@csrf_exempt
@permission_required("MenuApp.Waiter", login_url="/Menu/login")
def getOrder(request):
    """
    Returns JSON of requested order and ordered items for the order
    """
    order_items = OrderedItem.objects.filter(order__id=request.GET['id'])
    order = Order.objects.get(id=request.GET['id'])

    disc = order.discount
    dPrice = "%0.2f" % order.totalDiscount
    tPrice = "%0.2f" % order.totalPrice
    json = simplejson.dumps({"order": [
        {
        "id": item.order_id,
        "totalPrice": tPrice,
        "totalDiscount": dPrice,
        "discount": disc,
        "items": [{
            "item": orderedItem.item.name,
            "price": orderedItem.itemPrice,
            "orderID": orderedItem.order_id
        } for orderedItem in order_items]

    } for item in order_items]},
        indent=4)

    return HttpResponse(json, mimetype="application/json")


@login_required(login_url="Menu/login")
@csrf_exempt
@permission_required("MenuApp.Waiter", login_url="/Menu/login")
def applyDiscount(request):
    """
    Applys a discount to the order. Updates totalDiscount field to reflect
    totalPrice less the discount
    """

    order = Order.objects.get(id=request.GET['orderId'])
    discount = float(request.GET['discount'])

    #Discount applied
    if order.discount > 0:
        total = 0
        for item in order.getOrderedItems():
            total += item.itemPrice
            item.itemPriceCharts = item.itemPrice * (100 - discount) / 100
            item.save()

        total2 = total * (100 - discount) / 100
        tPrice = "%0.2f" % total2
        order.totalDiscount = tPrice
    ##Discount removed
    else:
        for item in order.getOrderedItems():
            item.itemPriceCharts = item.itemPrice * (100 - discount) / 100
            item.save()

        total = order.totalPrice * (100 - discount) / 100
        tPrice = "%0.2f" % total
        order.totalDiscount = tPrice

    order.discount = discount
    order.save()

    json = simplejson.dumps({"message": "success"}, indent=4)
    return HttpResponse(json, mimetype="application/json")


@login_required(login_url="Menu/login")
@csrf_exempt
@permission_required("MenuApp.Waiter", login_url="/Menu/login")
def updatePrice(request):
    """
    Updates the price of an ordered item.
    Updates itemPriceCharts to reflect any discounts on the order. This field is
    used by managers to calculate the actaul money spent on an item
    """

    order = Order.objects.get(id=request.POST['orderId'])
    order_items = OrderedItem.objects.filter(order__id=request.POST['orderId'])

    price = float(request.POST['price'])

    #Check if price is negative
    if price < 0:
        json = simplejson.dumps({"message": "Fail: Price cannot be negative!"}, indent=4)
        return HttpResponse(json, mimetype="application/json")

    total = 0

    it = 0
    ##Finds order item and sets price
    for item in order_items:
        if it == float(request.POST['id']):
            item.itemPrice = request.POST['price']
            if order.discount > 0:
                item.itemPriceCharts = price * (100 - order.discount) / 100
            else:
                item.itemPriceCharts = price
            item.save()
            total += price
        else:
            total += item.itemPrice

        it = it + 1

    if order.discount > 0:
        discount = total * (100 - order.discount) / 100
        order.totalDiscount = discount
    order.totalPrice = total
    order.save()

    json = simplejson.dumps({"message": "success"}, indent=4)
    return HttpResponse(json, mimetype="application/json")
