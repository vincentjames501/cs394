from django.template.context import RequestContext
from django.shortcuts import render_to_response

def contact(request):
    return render_to_response('contact.html', RequestContext(request))

