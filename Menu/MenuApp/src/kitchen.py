from datetime import datetime, timedelta
from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.utils import simplejson
from MenuApp.models import *

"""
Kitchen Web Application view functions

This view handles requsts for the kitchen page. It contains functions that:
Render the page
Keep the page up-to-date automtically
Communicate food preparation to the rest of the applicaiton.
"""

@login_required(login_url="/Menu/login")
@permission_required("MenuApp.Cook",login_url="/Menu/login")
def kitchen(request):
    """
    Constructs the kitchen page.

    The user must be logged in with Cook permissions to view this.
    The user must belong to a restaurant.
    """
    # Find the restaurant the user(cook) belongs to.
    restaurant = UserDetail.objects.filter(user=request.user)[0].restaurant

    # Find all orders that are completed, for the given restaurant, ordered by earliest placed.
    orders = Order.objects.filter(timeFinished__isnull=True,
        waiter__restaurant=restaurant).order_by('timePlaced')

    # Leave this datetime in an ugly format because it will not be viewed by users.
    currentTime = str(datetime.now())

    return render_to_response('kitchen.html',
        RequestContext(request, {'orders': orders, 'currentTime' : currentTime }))


@login_required(login_url="/Menu/login")
@csrf_exempt
@permission_required("MenuApp.Cook",login_url="/Menu/login")
def getKitchenUpdate(request):
    """
    Communicates with the kitchen page to keep it up-to-date.

    The user must be logged in with Cook permissions.
    The user must belong to a restaurant.
    Optionally accepts the datetime of the last update(last call to the function). If one is not provided, a datetime
    of 5 seconds ago will be used.
    Optionally accepts the ID of a completed order. If this is received, it will cause the order to be completed in the
    database. See finishOrder() for details.
    Optionally accepts the ID of a completed order item. If this is received, it will cause the order item to be
    completed in the database. See finishItem() for details.
    Returns a json encoded response of new orders, completed orders, completed items, and the time this update was
    performed.
    """
    # Find the restaurant the user(cook) belongs to.
    restaurant = UserDetail.objects.filter(user=request.user)[0].restaurant

    # Read the timestamp received from the kitchen page. This will be used to determine
    #   what new orders have arrived.
    if 'lastUpdateTime' in request.POST:
        timeOfLastUpdate = datetime.strptime(request.POST['lastUpdateTime'], '%Y-%m-%d %H:%M:%S.%f')
    else:
        timeOfLastUpdate = datetime.now() - timedelta(seconds=5)

    # Retrieve all the orders for the restaurant, that are not finished, and have been placed since
    #   the last update.
    new_orders = Order.objects.filter(timeFinished__isnull=True, waiter__restaurant=restaurant,
        timePlaced__gte=timeOfLastUpdate )

    # Generate a timestamp for when you got this update.
    updateTime = datetime.now()

    # Handle if an order has been finished.
    if request.POST.get('completed_order'):
        finishOrder( request.POST.get('completed_order') )

    # Handle if an item has been finished.
    if request.POST.get('completed_item'):
        finishItem( request.POST.get('completed_item') )

    # Retrieve all orders for the restaurant, that have been finished since the last update.
    finished_orders = Order.objects.filter(timeFinished__isnull=False, waiter__restaurant=restaurant,
        timeFinished__gte=timeOfLastUpdate)

    # Retrieve all items for the restaurant, that have been finished since the last update.
    finished_items = OrderedItem.objects.filter(timePrepared__isnull=False, order__waiter__restaurant=restaurant,
        timePrepared__gte=timeOfLastUpdate)

    json = simplejson.dumps({
        "new_orders": [
        {
            "id": order.id,
            "table": order.table.username,
            "waiter": order.waiter.user.username,
            "time_placed": conv_time(order.timePlaced),
        } for order in new_orders],
        "finished_orders": [
        {
            "id": order.id,
            "table": order.table.username,
            "waiter": order.waiter.user.username,
            "time_placed": conv_time(order.timePlaced),
            "time_finished": conv_time(order.timeFinished)
        } for order in finished_orders],
        "finished_items": [
        {
            "id": item.id,
            "time_prepared": conv_time(item.timePrepared)
        } for item in finished_items],
        "updateTime": str(updateTime)}, # Raw date format required.
        indent=4 )

    return HttpResponse(json, mimetype="application/json")

def finishOrder( order_id ):
    """
    Used by getKitchenUpdate to properly finish an order.

    This function modifies the database. Given an order id, it will finish that order and finish all order items
    in that order.
    """
    # Grab the order.
    try:
        order = Order.objects.get(id=order_id)
    except Order.DoesNotExist:
        return None
    # Get the current time
    curTime = datetime.now()
    # Record a time finished for the order.
    order.timeFinished = curTime
    # Get all the items for the given order.
    items = OrderedItem.objects.filter(order=order)
    # Loop through and record a time for each one if they aren't already prepared.
    for item in items:
        if item.timePrepared is None:
            item.timePrepared = curTime
            item.save()
    # Save the changes to the order.
    order.save()

def finishItem( item_id ):
    """
    Used by getKitchenUpdate to properly finish an order item.

    This function modifies the database. Given an order item id, it will finish that item, and check to see if it is
    the last unfinished item in the order. It is it, it invokes finishOrder().
    """
    # Get the item itself.
    try:
        item = OrderedItem.objects.get(id=item_id)
    except OrderedItem.DoesNotExist:
        return None
    # Get the order the item belongs to.
    parent_order_id = OrderedItem.getOrder(item)
    # Record a time for it.
    item.timePrepared = datetime.now()
    # Save our changes.
    item.save()
    # Check if finishing this item actually finished the order.
    other_items = OrderedItem.objects.filter(order__id=parent_order_id, timePrepared__isnull=True)
    if not other_items:
        finishOrder( parent_order_id )

@login_required(login_url="/Menu/login")
@csrf_exempt
@permission_required("MenuApp.Cook",login_url="/Menu/login")
def getCompletedOrders(request):
    """
    Sends the information needed to construct the Completed Orders tab on the kitchen page.

    User must be logged in with Cook permissions.
    User must belong to a restaurant.
    Sends a json encoded response of all completed orders.
    """
    # Find the restaurant the user(cook) belongs to.
    restaurant = UserDetail.objects.filter(user=request.user)[0].restaurant

    # Gather all the order items for a given restaurant.
    order_items = OrderedItem.objects.filter(item__category__menuPage__menu__restaurant=restaurant)

    # Find all orders that are completed, for the given restaurant, ordered by earliest placed.
    orders = Order.objects.filter(timeFinished__isnull=False,
        id__in=[item.order.id for item in order_items]).order_by('timePlaced')

    json = simplejson.dumps({
        "aaData": [
            {
                "id": order.id,
                "table": order.table.username,
                "waiter": order.waiter.user.username,
                "time_placed": conv_time(order.timePlaced),
                "time_finished": conv_time(order.timeFinished),
            } for order in orders]}, indent=4 )

    return HttpResponse(json, mimetype="application/json")

@login_required(login_url="/Menu/login")
@csrf_exempt
@permission_required("MenuApp.Cook",login_url="/Menu/login")
def getOrderItems(request):
    """
    Sends the information needed to construct the order details table.

    User must be logged in with Cook permissions.
    User must belong to a restaurant.
    The ID of the order must be in the GET or else this function will return a blank response.
    If a valid order ID is in the GET, a json encoded response of all items in a order will be sent.
    """
    if 'order_id' in request.GET:
        # Gather all the order items for a given order.
        order_items = OrderedItem.objects.filter(order__id=request.GET['order_id'])

        json = simplejson.dumps([
            {
                "id": item.id,
                "name": item.item.name,
                "time_prepared": conv_time(item.timePrepared),
                "comments": item.comments,
            } for item in order_items] , indent=4 )

        return HttpResponse(json, mimetype="application/json")
    else:
        return HttpResponse()

def conv_time( time ):
    """
    Used by many functions in this view to provide a uniform datetime format.

    Converts the hard to read default format of datetime into one similar to the Django default. This function was
    created so there is a single place to change, if a different format is requested.
    Accepts a raw datetime object.
    Returns a date formatted as "Feb. 17, 2012, 01:04 PM"
    """
    if time is None:
        return "Not Prepared"
    else:
        return datetime.strftime( time, "%b. %d, %Y, %I:%M %p")