from django.contrib.auth import authenticate, login, logout
from django.http import  HttpResponseRedirect
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from MenuApp.src.kitchen import kitchen
from MenuApp.src.manager import manager
from MenuApp.src.table_management import waiter

@csrf_exempt
def doLogin(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)

            # Set session length to 15minutes for manager
            # TODO: SESSION_COOKIE_AGE
            if request.user.has_perm("MenuApp.Manager"):
                request.session.set_expiry(900)

            if request.GET.has_key("next"):
                return HttpResponseRedirect(request.GET['next'])
            else:
                if request.user.has_perm("MenuApp.Manager"):
                    return manager(request)
                elif request.user.has_perm("MenuApp.Waiter"):
                    return waiter(request)
                elif request.user.has_perm("MenuApp.Cook"):
                    return kitchen(request)
                else:
                    return render_to_response('index.html', RequestContext(request))
        else:
            return render_to_response('loginPage.html', RequestContext(request, {'message': 'Invalid login!'}))
    else:
        return render_to_response('loginPage.html', RequestContext(request, {'message': 'Invalid login!'}))


def doLogout(request):
    logout(request)
    return render_to_response('loginPage.html', RequestContext(request))
