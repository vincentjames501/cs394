from django.template.context import RequestContext
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt

def index(request):
    return render_to_response("index.html", RequestContext(request))

@csrf_exempt
def loginPage(request):
    d = {}
    if request.GET.has_key("next"):
        d = dict(next="?next=" + request.GET["next"])
    return render_to_response('loginPage.html', RequestContext(request, d))








