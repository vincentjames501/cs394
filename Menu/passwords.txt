Passwords are saved in a hash:

----------------------
Superusers
----------------------

User: Superuser
Pass: 0123456

User: vjpwdf
Pass: 0123456

User: Seth
Pass: seth

User: war432
Pass: 0123456

User: jas2m8
Pass: 0123456

User: hssth5
Pass: 0123456

User: jmbvy9
Pass: 0123456

User: mdexb2
Pass: 0123456

User: sg6pb
Pass: 0123456

User: pme3n7
Pass: 0123456

User: ace266
Pass: 0123456


----------Fixtures Accounts-----
User: Tom 
pass: password
Role: Waiter

User: Kim
pass: password
Role: waiter

User: Table1
pass: password
Role: Table

user: Table2
pass: password
Role: Table

User: TestCook
pass: password
Role: Cook

User: Bob
pass: password
Role: Manager
