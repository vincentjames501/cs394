import os, sys
sys.path.append('/var/www/djangoapps/cs394')
sys.path.append('/var/www/djangoapps/cs394/Menu')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()