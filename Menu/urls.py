from django.conf.urls.defaults import patterns, include, url

from django.contrib import admin
import settings

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
     url(r'^$', 'MenuApp.views.index', name='home'),
     url(r'^Menu/index', 'MenuApp.views.index'),
     url(r'^Menu/login', 'MenuApp.views.loginPage'),
     url(r'^Menu/doLogin', 'MenuApp.src.auth.doLogin'),
     url(r'^Menu/doLogout', 'MenuApp.src.auth.doLogout'),
     url(r'^Menu/userManagement', 'MenuApp.src.manager.userManagement'),
     url(r'^Menu/menuManagement', 'MenuApp.src.manager.menuManagement'),
     url(r'^Menu/menuPageManagement', 'MenuApp.src.manager.menuPageManagement'),
     url(r'^Menu/categoryManagement', 'MenuApp.src.manager.categoryManagement'),
     url(r'^Menu/menuItemManagement', 'MenuApp.src.manager.menuItemManagement'),
     url(r'^Menu/nutritionFactManagement', 'MenuApp.src.manager.nutritionFactManagement'),
     url(r'^Menu/addMenu', 'MenuApp.src.manager.addMenu'),
     url(r'^Menu/deleteMenu', 'MenuApp.src.manager.deleteMenu'),
     url(r'^Menu/getUsers', 'MenuApp.src.manager.getUsers'),
     url(r'^Menu/getCategories', 'MenuApp.src.manager.getCategories'),
     url(r'^Menu/getMenuPages', 'MenuApp.src.manager.getMenuPages'),
     url(r'^Menu/getMenuItems', 'MenuApp.src.manager.getMenuItems'),
     url(r'^Menu/getCompletedOrders', 'MenuApp.src.kitchen.getCompletedOrders'),
     url(r'^Menu/getOrderItems', 'MenuApp.src.kitchen.getOrderItems'),
     url(r'^Menu/getNutritionFacts', 'MenuApp.src.manager.getNutritionFacts'),
     url(r'^Menu/updateMenuPage', 'MenuApp.src.manager.updateMenuPage'),
     url(r'^Menu/uploadImage', 'MenuApp.src.manager.uploadImage'),
     url(r'^Menu/updateCategory', 'MenuApp.src.manager.updateCategory'),
     url(r'^Menu/updateUser', 'MenuApp.src.manager.updateUser'),
     url(r'^Menu/updateMenuItems', 'MenuApp.src.manager.updateMenuItems'),
     url(r'^Menu/updateNutritionFacts', 'MenuApp.src.manager.updateNutritionFacts'),
     url(r'^Menu/contact', 'MenuApp.src.contact.contact'),
     url(r'^Menu/getMenu', 'MenuApp.src.table.getMenu'),
     url(r'^Menu/charts', 'MenuApp.src.manager.charts'),
     url(r'^Menu/manager', 'MenuApp.src.manager.manager'),
     url(r'^Menu/downloadCSV', 'MenuApp.src.manager.downloadCSV'),
     url(r'^Menu/filterByDate', 'MenuApp.src.manager.filterByDate'),
     url(r'^Menu/kitchen', 'MenuApp.src.kitchen.kitchen'),
     url(r'^Menu/getKitchenUpdate', 'MenuApp.src.kitchen.getKitchenUpdate'),
     url(r'^Menu/tableManagement', 'MenuApp.src.table_management.waiter'),
     url(r'^Menu/getWaiterOrders', 'MenuApp.src.table_management.getWaiterOrders'),
     url(r'^Menu/getOrderIds', 'MenuApp.src.table_management.getOrderIds'),
     url(r'^Menu/getOrderById', 'MenuApp.src.table_management.getOrderById'),
     url(r'^Menu/applyDiscount', 'MenuApp.src.table_management.applyDiscount'),
     url(r'^Menu/getOrder', 'MenuApp.src.table_management.getOrder'),
     url(r'^Menu/updatePrice', 'MenuApp.src.table_management.updatePrice'),
     url(r'^Menu/closeOrder', 'MenuApp.src.table_management.closeOrder'),
     url(r'^Menu/menu', 'MenuApp.src.table.menu'),
     url(r'^Menu/getAdditionalImages', 'MenuApp.src.table.getAdditionalImages'),
     url(r'^Menu/getItemNutritionFacts', 'MenuApp.src.table.getMenuItemNutritionFacts'),
     url(r'^Menu/createNewOrder', 'MenuApp.src.manager.createNewOrder'),
     url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),


    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
