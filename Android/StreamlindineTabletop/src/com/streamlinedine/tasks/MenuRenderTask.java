package com.streamlinedine.tasks;

import android.app.Activity;
import android.content.Context;
import android.widget.TableRow;
import android.widget.ViewFlipper;
import com.streamlinedine.R;
import com.streamlinedine.menu.Menu;
import com.streamlinedine.menu.category.Category;
import com.streamlinedine.menu.category.CategoryRow;
import com.streamlinedine.menu.item.MenuItem;
import com.streamlinedine.menu.item.MenuItemPlaceholder;
import com.streamlinedine.menu.item.MenuItemRow;
import com.streamlinedine.menu.page.MenuPage;
import com.streamlinedine.menu.page.MenuPageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Async task to handle rendering the menu.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/26/12
 */
public class MenuRenderTask extends LoadingStatusTask {
    ViewFlipper flipper;
    private Context context;

    /**
     * Default constructor
     * @param context the context
     */
    public MenuRenderTask(Context context) {
        super(context, true, "Rendering Menu");
        this.context = context;
        this.flipper = (ViewFlipper) ((Activity) context).findViewById(R.id.view_flipper);
    }

    /**
     * Overrides the on post execute method. A list of MenuPageViews is returned from
     * the doInBackground method. These views are then added to the view flipper of the menu.
     * @param o returned from doInBackground. This is a list of menu page views.
     */
    @Override
    protected void onPostExecute(Object o) {
        List<MenuPageView> menuPageViews = (ArrayList<MenuPageView>) o;
        int counter = 0;
        for (MenuPageView menuPageView : menuPageViews) {
            flipper.addView(menuPageView, counter);
            counter++;
        }
        flipper.setDisplayedChild(Menu.instance.getCurrentPage());
        super.onPostExecute(o);
    }

    /**
     * Overrides the doInBackground method. This creates a list of MenuPageViews to be
     * added to the menu.
     * @param objects additional parameters
     * @return Object. In this case, a list of MenuPageViews.
     */
    @Override
    protected Object doInBackground(Object... objects) {
        List<MenuPageView> menuPageViewList = new ArrayList<MenuPageView>();
        int numberColumns = 2;
        TableRow.LayoutParams categoryParams = new TableRow.LayoutParams();
        categoryParams.span = numberColumns;
        TableRow.LayoutParams menuParams = new TableRow.LayoutParams();
        menuParams.span = 1;
        menuParams.setMargins(10, 10, 10, 10);
        int counter = 0;
        for (MenuPage menuPage : Menu.instance.getMenuPages()) {
            MenuPageView menuPageView = new MenuPageView(context);
            for (Category category : menuPage.getCategories()) {
                List<MenuItem> menuItemList = category.getMenuItems();
                buildCategoryRow(categoryParams, menuPageView, category);
                buildInitialMenuItems(numberColumns, menuParams, menuPageView, menuItemList);
                buildLastRow(numberColumns, menuParams, menuPageView, menuItemList);
            }
            menuPageViewList.add(menuPageView);
        }
        return menuPageViewList;
    }

    /**
     * Builds the last row of menu items
     * @param numberColumns the number of columns of items to create
     * @param menuParams the layout params for each menu tile
     * @param menuPageView the menu page view to add the menu item tiles to
     * @param menuItems list of menu items
     */
    private void buildLastRow(int numberColumns, TableRow.LayoutParams menuParams, MenuPageView menuPageView, List<MenuItem> menuItems) {
        TableRow lastRow = new TableRow(context);
        int menuItemCount = menuItems.size();
        int numberLeftOver = menuItemCount % numberColumns;

        if (numberLeftOver > 0) {
            int leftOverStart = menuItemCount - numberLeftOver;
            for (int i = 0; i < numberColumns; i++) {
                if (i < numberLeftOver) {
                    MenuItem menuItem = menuItems.get(leftOverStart + i);
                    lastRow.addView(new MenuItemRow(menuItem, context), menuParams);
                } else {
                    lastRow.addView(new MenuItemPlaceholder(context), menuParams);
                }
            }
            menuPageView.addComponent(lastRow);
        }
    }

    /**
     * Builds the initial set of menu item tiles
     * @param numberColumns the number of columns to create
     * @param menuParams the layout params for each menu tile
     * @param menuPageView the menu page view to add the menu item tiles to
     * @param menuItems list of menu items
     */
    private void buildInitialMenuItems(int numberColumns, TableRow.LayoutParams menuParams, MenuPageView menuPageView, List<MenuItem> menuItems) {
        int menuItemCount = menuItems.size();
        int end = (menuItemCount / numberColumns) * numberColumns;
        TableRow menuItemsRow = new TableRow(context);
        for (int i = 0; i < end; i++) {
            MenuItem menuItem = menuItems.get(i);
            menuItemsRow.addView(new MenuItemRow(menuItem, context), menuParams);
            if (i % numberColumns == (numberColumns - 1)) {
                menuPageView.addComponent(menuItemsRow);
                menuItemsRow = new TableRow(context);
            }
        }
    }

    /**
     * Builds a category row
     * @param categoryParams the layout params for the category
     * @param menuPageView the menu page view to add the menu item to
     * @param category the category object
     */
    private void buildCategoryRow(TableRow.LayoutParams categoryParams, MenuPageView menuPageView, Category category) {
        TableRow categoryRow = new TableRow(context);
        categoryRow.addView(new CategoryRow(category, context), categoryParams);
        menuPageView.addComponent(categoryRow);
    }
}
