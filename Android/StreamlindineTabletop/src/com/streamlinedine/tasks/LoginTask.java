package com.streamlinedine.tasks;

import android.content.Context;
import android.content.Intent;
import com.streamlinedine.util.User;
import com.streamlinedine.util.server_communication.*;
import com.streamlinedine.activities.HomeMenuActivity;
import com.streamlinedine.dialogs.ErrorAlert;
import org.apache.http.HttpResponse;

/**
 * This is an async task used for logging in the tablet device.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/2/12
 */
public class LoginTask extends LoadingStatusTask {

    private Context context;
    private String username;
    private String password;

    /**
     * Default constructor
     * @param context the context
     * @param username the username to log in with
     * @param password the password to log in with
     */
    public LoginTask(Context context, String username, String password) {
        super(context, true, "Logging In");
        this.username = username;
        this.password = password;
        this.context = context;
    }

    /**
     * Overrides the LoadingStatusTask on post execute method. If the returned data
     * is an exception, and error message is shown. Otherwise, the user's username
     * and password is stored in the application. The home screen activity is then launched
     * @param o returned from doInBackground
     */
    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (o instanceof Exception) {
            ErrorAlert badLogin = new ErrorAlert(((Exception) o).getMessage(), context);
            badLogin.show();
        } else {
            User.username = username;
            User.password = password;
            Intent launchMenuIntent = new Intent(context, HomeMenuActivity.class);
            context.startActivity(launchMenuIntent);
        }
    }

    /**
     * Overrides the LoadingStatusTask do in background method. This method
     * actually logs into the application using the LoginService. If the login is unsuccessful
     * the exception is returned and handled in the onPostExecute method.
     * @param objects additional params
     * @return Object. Either nothing, or an exception.
     */
    @Override
    protected Object doInBackground(Object... objects) {
        try {
            Thread.sleep(2000);
            LoginService loginService = new LoginService();
            loginService.setUsername(this.username);
            loginService.setPassword(this.password);
            HttpResponse response = loginService.execute();
            String responseString = (response.getHeaders("Set-Cookie"))[0].toString();
            StreamlineService.cookie = responseString.substring(responseString.indexOf("sessionid="), responseString.indexOf(";"));
        } catch (Exception e) {
            return new Exception("Could not log in");
        }
        return null;
    }
}
