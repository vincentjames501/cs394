package com.streamlinedine.tasks;

import android.content.Context;
import com.streamlinedine.dialogs.ErrorAlert;
import com.streamlinedine.util.MenuBuilder;
import com.streamlinedine.util.server_communication.GetMenuService;
import com.streamlinedine.menu.Menu;

/**
 * Async task to handle menu retrieval
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/9/12
 */
public class RetrieveMenuTask extends LoadingStatusTask {
    private Context context;
    private MenuBuilder menuBuilder;

    /**
     * Default constructor
     * @param context the context
     */
    public RetrieveMenuTask(Context context) {
        super(context, true, "Retrieving Menu");
        this.context = context;
        menuBuilder = new MenuBuilder();
    }

    /**
     * Overrides the onPostExecute method. If the menu retrieval was successful, it
     * starts the MenuRenderTask.
     * @param o returned from doInBackground. If it is an exception, an error is shown.
     */
    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (o instanceof Exception) {
            ErrorAlert alert = new ErrorAlert(((Exception) o).getMessage(), context);
            alert.show();
        } else {
            new MenuRenderTask(context).execute();
        }
    }

    /**
     * Overrides the doInBackground method. Uses a GetMenuService to retrieve the menu.
     * Once it is retrieved, a MenuBuilder is used to create java-object representations of
     * the menu.
     * @param objects Additional parameters
     * @return Object. Can be an exception.
     */
    @Override
    protected Object doInBackground(Object... objects) {
        try {
            GetMenuService submitOrderService = new GetMenuService();
            Menu.instance = menuBuilder.buildMenu(submitOrderService.execute());
            Menu.instance.updateCurrentPage();
        } catch (Exception e) {
            return e;
        }
        return null;
    }
}
