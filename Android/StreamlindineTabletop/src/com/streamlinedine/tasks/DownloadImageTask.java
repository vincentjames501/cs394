package com.streamlinedine.tasks;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.View;
import com.streamlinedine.R;
import com.streamlinedine.custom_component.LoadingImage;
import com.streamlinedine.util.image.ImageDownloader;
import com.streamlinedine.util.image.ImageMetrics;

import java.io.IOException;

/**
 * An async task to handle downloading a bitmap image. This is
 * used for retrieving images for menu items
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/14/12
 */
public class DownloadImageTask extends AsyncTask {


    private String additionalImage;
    private LoadingImage loadingImage;

    /**
     * Default constructor
     * @param imageView the image view we are downloading the image for
     * @param additionalImage the string that needs to be downloaded
     */
    public DownloadImageTask(LoadingImage imageView, String additionalImage) {
        this.loadingImage = imageView;
        this.additionalImage = additionalImage;
    }

    /**
     * Overrides the on post execute method. If the image was successfully downloaded, the
     * image view is set to the downloaded image. The progress bar is then hidden. If there was
     * an error, the image is set to the default image (dr. pepper). The drawing state is refreshed
     * as well.
     * @param o returned from the doInBackground method
     */
    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (!(o instanceof Exception)) {
            loadingImage.getImageView().setImageBitmap(ImageMetrics.getScaledBitmap((Bitmap) o, 200, 200));

        } else {
            loadingImage.getImageView().setImageDrawable(loadingImage.getContext().getResources().getDrawable(R.drawable.dr_pepper));
        }
        loadingImage.getImageView().setVisibility(View.VISIBLE);
        loadingImage.getProgressBar().setVisibility(View.GONE);
        loadingImage.refreshDrawableState();
    }

    /**
     * Overrides the do in background method. Uses the ImageDownloader class to download the specified bitmap. If
     * there is an error, the exception is caught and returned to the onPostExecute method
     * @param objects additional parameters (not used)
     * @return Object. In this case, either a bitmap or exception
     */
    @Override
    protected Object doInBackground(Object... objects) {
        try {
            return ImageDownloader.downloadBitmap(additionalImage);
        } catch (IOException e) {
            e.printStackTrace();
            return e;
        }
    }
}
