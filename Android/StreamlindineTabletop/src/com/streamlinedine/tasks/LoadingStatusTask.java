package com.streamlinedine.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import com.streamlinedine.dialogs.CustomProgressDialog;

/**
 * This class is a base class for async tasks that use a progress dialog while
 * performing some function.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/26/12
 */
public abstract class LoadingStatusTask extends AsyncTask {
    private ProgressDialog progressDialog;
    private boolean showDialog;

    /**
     * Default constructor
     * @param context the context
     * @param showDialog boolean flag, if the progress dialog is to be shown
     * @param loadingMessage the message the progress dialog is to display
     */
    public LoadingStatusTask(Context context, boolean showDialog, String loadingMessage) {
        progressDialog = new CustomProgressDialog(context);
        progressDialog.setTitle(loadingMessage);
        this.showDialog = showDialog;
    }

    /**
     * Overrides the onPreExecute method. If the dialog is to be shown,
     * the dialog is shown.
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showDialog) {
            progressDialog.show();
        }
    }

    /**
     * Overrides the onPostExecute method. If the dialog is being shown, it is
     * dismissed.
     * @param o returned from doInBackground (not used here)
     */
    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (showDialog) {
            progressDialog.dismiss();
        }
    }
}
