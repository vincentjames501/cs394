package com.streamlinedine.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Simple error alert dialog
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 1/31/12
 */
public class ErrorAlert extends AlertDialog {

    /**
     * Default constructor
     * @param message the message to display
     * @param context the context
     */
    public ErrorAlert(String message, Context context) {
        super(context);
        setMessage(message);
        setButton("Ok", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
    }
}
