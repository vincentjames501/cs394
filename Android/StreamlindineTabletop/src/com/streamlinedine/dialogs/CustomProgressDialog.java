package com.streamlinedine.dialogs;

import android.app.ProgressDialog;
import android.content.Context;
import com.streamlinedine.R;

/**
 * Represents a custom progress dialog
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/10/12
 */
public class CustomProgressDialog extends ProgressDialog {

    /**
     * Default constructor. Creates an uncancelable, indeterminate progress dialog
     * @param context the context
     */
    public CustomProgressDialog(Context context) {
        super(context);
        setIndeterminate(true);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        setIcon(R.drawable.info_icon);
    }
}
