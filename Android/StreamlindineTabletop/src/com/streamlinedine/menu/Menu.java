package com.streamlinedine.menu;

import com.streamlinedine.menu.category.Category;
import com.streamlinedine.menu.item.MenuItem;
import com.streamlinedine.menu.page.MenuPage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The menu object. Has a static instance of the menu so as to be
 * accessible throughout the application.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 1/31/12
 */
public class Menu {

    public static Menu instance = new Menu();

    private String name;
    private List<MenuPage> menuPages;
    private MenuPage currentMenuPage;
    private int currentPage;
    private Map<Integer, MenuItem> menuItemMap;

    public Menu() {
        menuPages = new ArrayList<MenuPage>();
        menuItemMap = new HashMap<Integer, MenuItem>();
    }

    public void flipToNextPage() {
        currentPage = (currentPage + 1) % menuPages.size();
        updateCurrentPage();
    }

    public void flipToPreviousPage() {
        currentPage = ((currentPage - 1) + menuPages.size()) % menuPages.size();
        updateCurrentPage();
    }

    public void updateCurrentPage() {
        currentMenuPage = menuPages.get(currentPage);
    }

    public void addMenuPage(MenuPage menuPage) {
        menuPages.add(menuPage);
        for (Category category : menuPage.getCategories()) {
            for (MenuItem menuItem : category.getMenuItems()) {
                menuItemMap.put(menuItem.getItemId(), menuItem);
            }
        }
    }

    public List<MenuPage> getMenuPages() {
        return menuPages;
    }

    public void setMenuPages(List<MenuPage> menuPages) {
        this.menuPages = menuPages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MenuPage getCurrentMenuPage() {
        return currentMenuPage;
    }

    public void setCurrentMenuPage(MenuPage currentMenuPage) {
        this.currentMenuPage = currentMenuPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public Map<Integer, MenuItem> getMenuItemMap() {
        return menuItemMap;
    }

    public void setMenuItemMap(Map<Integer, MenuItem> menuItemMap) {
        this.menuItemMap = menuItemMap;
    }
}
