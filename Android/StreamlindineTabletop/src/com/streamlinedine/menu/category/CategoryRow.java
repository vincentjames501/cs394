package com.streamlinedine.menu.category;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.streamlinedine.R;

/**
 * This is the custom android view component that represents a category row.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/16/12
 */
public class CategoryRow extends LinearLayout {
    private Category category;
    private TextView categoryName;
    private ImageView categoryImage;

    public CategoryRow(Category category, Context context) {
        super(context);
        this.category = category;

        inflateLayout(context);
        categoryName = (TextView)findViewById(R.id.category_name);
        categoryName.setText(category.getCategoryName());
    }

    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.category_row, this);
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public TextView getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(TextView categoryName) {
        this.categoryName = categoryName;
    }

    public ImageView getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(ImageView categoryImage) {
        this.categoryImage = categoryImage;
    }
}
