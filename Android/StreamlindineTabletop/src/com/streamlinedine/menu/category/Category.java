package com.streamlinedine.menu.category;

import com.streamlinedine.menu.item.MenuItem;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a menu category. It consists of a category name
 * and a list of menu items associated to the category
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 1/31/12
 */
public class Category {
    private String categoryName;
    private List<MenuItem> menuItems;

    public Category(String name) {
        this.categoryName = name;
        menuItems = new ArrayList<MenuItem>();
    }

    public Category() {
        menuItems = new ArrayList<MenuItem>();
    }

    public void addMenuItem(MenuItem menuItem) {
        menuItems.add(menuItem);
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
}
