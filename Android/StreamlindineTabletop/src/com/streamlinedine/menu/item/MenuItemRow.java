package com.streamlinedine.menu.item;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.widget.*;
import com.streamlinedine.R;
import com.streamlinedine.util.image.ImageDownloader;
import com.streamlinedine.util.image.ImageMetrics;
import com.streamlinedine.util.button_listeners.AddItemButtonListener;
import com.streamlinedine.util.button_listeners.DescriptionButtonListener;

import java.text.NumberFormat;

/**
 * This is the custom android view component representing a menu item.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 1/31/12
 */
public class MenuItemRow extends LinearLayout {
    private MenuItem menuItem;

    private ImageView menuItemImage;
    private TextView menuItemName;
    private TextView menuItemDescription;
    private TextView menuItemPrice;

    private final float IMAGE_SCALE = 200f;

    public MenuItemRow(MenuItem menuItem, Context context) {
        super(context);
        this.menuItem = menuItem;

        inflateLayout(context);

        menuItemImage = (ImageView) findViewById(R.id.menu_item_image);
        menuItemImage.setOnClickListener(new DescriptionButtonListener(menuItem));
        menuItemName = (TextView) findViewById(R.id.menu_item_name);
        menuItemDescription = (TextView) findViewById(R.id.menu_item_description);
        menuItemPrice = (TextView) findViewById(R.id.menu_item_price);
        buildMenuItemImage();
        buildButton(R.id.add_menu_item_to_order_button, new AddItemButtonListener(menuItem));
        buildButton(R.id.menu_item_details_button, new DescriptionButtonListener(menuItem));
        menuItemName.setText(menuItem.getName());
        menuItemDescription.setText(menuItem.getShortDescription());
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        menuItemPrice.setText(formatter.format(menuItem.getPrice()));
    }

    private void buildButton(int buttonId, OnClickListener buttonClickListener) {
        Button button = (Button)findViewById(buttonId);
        button.setOnClickListener(buttonClickListener);
    }

    private void buildMenuItemImage() {
        String itemImageString = menuItem.getMainImage();
        try {
            Bitmap bitmap = ImageDownloader.downloadBitmap(itemImageString);
            bitmap = ImageMetrics.getScaledBitmap(bitmap, IMAGE_SCALE, IMAGE_SCALE);
            menuItemImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            menuItemImage.setImageResource(R.drawable.dr_pepper);
        }
    }

    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.menu_item_row, this);
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    public ImageView getMenuItemImage() {
        return menuItemImage;
    }

    public TextView getMenuItemName() {
        return menuItemName;
    }

    public void setMenuItemName(TextView menuItemName) {
        this.menuItemName = menuItemName;
    }

    public TextView getMenuItemDescription() {
        return menuItemDescription;
    }

    public void setMenuItemDescription(TextView menuItemDescription) {
        this.menuItemDescription = menuItemDescription;
    }

    public TextView getMenuItemPrice() {
        return menuItemPrice;
    }

    public void setMenuItemPrice(TextView menuItemPrice) {
        this.menuItemPrice = menuItemPrice;
    }
}
