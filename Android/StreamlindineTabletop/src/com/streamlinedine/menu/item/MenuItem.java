package com.streamlinedine.menu.item;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This represents a menu item.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 1/31/12
 */
public class MenuItem implements Serializable{
    private String name;
    private String shortDescription;
    private String longDescription;
    private String rating;
    private int timesOrdered;
    private float price;
    private Map<String, String> nutritionFactMap;
    private String mainImage;
    private List<String> additionalImages;
    private int itemId;

    public MenuItem() {
        nutritionFactMap = new HashMap<String, String>();
        additionalImages = new ArrayList<String>();
    }

    public void addAdditionalImage(String image) {
        additionalImages.add(image);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getTimesOrdered() {
        return timesOrdered;
    }

    public void setTimesOrdered(int timesOrdered) {
        this.timesOrdered = timesOrdered;
    }

    public Map<String, String> getNutritionFactMap() {
        return nutritionFactMap;
    }

    public void setNutritionFactMap(Map<String, String> nutritionFactMap) {
        this.nutritionFactMap = nutritionFactMap;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public List<String> getAdditionalImages() {
        return additionalImages;
    }

    public void setAdditionalImages(List<String> additionalImages) {
        this.additionalImages = additionalImages;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }
}
