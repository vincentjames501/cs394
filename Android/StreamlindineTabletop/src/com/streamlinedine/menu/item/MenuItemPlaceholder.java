package com.streamlinedine.menu.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import com.streamlinedine.R;

/**
 * This class is an invisible placeholder when rendering the menu items in columns.
 * This ensures that items are laid out in equal columns
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/26/12
 */
public class MenuItemPlaceholder extends LinearLayout{
    public MenuItemPlaceholder(Context context) {
        super(context);
        inflateLayout(context);
    }

    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.menu_item_placeholder, this);
    }
}
