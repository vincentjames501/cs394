package com.streamlinedine.menu.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.streamlinedine.R;
import com.streamlinedine.menu.Menu;
import com.streamlinedine.order.Order;
import com.streamlinedine.order.OrderItem;
import com.streamlinedine.util.button_listeners.EditOrderItemButtonListener;
import com.streamlinedine.util.button_listeners.RemoveOrderItemButtonListener;
import com.streamlinedine.util.image.ImageDownloader;
import com.streamlinedine.util.image.ImageMetrics;

import java.io.IOException;

/**
 * This represents the custom android view component for a user's order item.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/26/12
 */
public class OrderItemRow extends LinearLayout {
    private TextView itemName;
    private TextView itemCount;
    private TextView itemComment;
    private OrderItem orderItem;
    private ImageView imageView;
    private static final int MAX_COMMENT_LENGTH = 80;

    public OrderItemRow(Context context, OrderItem orderItem) {
        super(context);
        inflateLayout(context);
        itemName = (TextView) findViewById(R.id.order_item_name);
        itemCount = (TextView) findViewById(R.id.order_item_count);
        itemComment = (TextView) findViewById(R.id.order_item_comment);
        imageView = (ImageView) findViewById(R.id.order_item_row_image);

        ImageView removeButton = (ImageView) findViewById(R.id.remove_order_item_button);
        ImageView editButton = (ImageView) findViewById(R.id.edit_order_button);

        removeButton.setOnClickListener(new RemoveOrderItemButtonListener(orderItem, this, context));
        editButton.setOnClickListener(new EditOrderItemButtonListener(context, orderItem));

        MenuItem menuItem = Menu.instance.getMenuItemMap().get(orderItem.getId());
        itemName.setText(menuItem.getName());
        itemCount.setText(String.valueOf(orderItem.getQuantity()));

        String comment = orderItem.getComment();
        if (comment.length() >= 80) {
            itemComment.setText(comment.substring(0, MAX_COMMENT_LENGTH - 3) + "...");
        } else {
            itemComment.setText(comment);
        }
        //itemComment.setText(orderItem.getComment());
        try {
            imageView.setImageBitmap(ImageMetrics.getScaledBitmap(ImageDownloader.downloadBitmap(orderItem.getAssociatedMenuItem().getMainImage()), 150, 150));
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.orderItem = orderItem;
    }

    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.order_item_row, this);
    }

    public void removeItem(View button) {
        Order.order.getOrderItems().remove(orderItem);
        LinearLayout layout = (LinearLayout) findViewById(R.id.view_order_layout);
        layout.removeView(this);
    }

    public TextView getItemName() {
        return itemName;
    }

    public void setItemName(TextView itemName) {
        this.itemName = itemName;
    }

    public TextView getItemCount() {
        return itemCount;
    }

    public void setItemCount(TextView itemCount) {
        this.itemCount = itemCount;
    }

    public TextView getItemComment() {
        return itemComment;
    }

    public void setItemComment(TextView itemComment) {
        this.itemComment = itemComment;
    }
}
