package com.streamlinedine.menu.page;

import android.content.Context;
import android.view.*;
import android.widget.ScrollView;
import android.widget.TableLayout;
import com.streamlinedine.R;

/**
 * This is the custom android view component for a menu page.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/19/12
 */
public class MenuPageView extends ScrollView {
    private TableLayout layout;

    /**
     * Default constructor for the MenuPageView
     * @param context the context
     */
    public MenuPageView(Context context) {
        super(context);
        inflateLayout(context);
        layout = (TableLayout)findViewById(R.id.menu_view_layout);
    }

    /**
     * Adds an android view component to this page's layout
     * @param view the view to add
     */
    public void addComponent(View view) {
        layout.addView(view);
        layout.refreshDrawableState();
    }

    /**
     * Inflates this component's layout with the correct xml file
     * @param context the context to use for inflation
     */
    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.menu_view, this);
    }
}
