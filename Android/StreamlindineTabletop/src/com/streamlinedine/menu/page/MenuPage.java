package com.streamlinedine.menu.page;

import com.streamlinedine.menu.category.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * This represents a menu page
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 1/31/12
 */
public class MenuPage {
    private int pageNumber;
    private String name;
    private List<Category> categories;

    public MenuPage(int pageNumber, String name) {
        this.pageNumber = pageNumber;
        this.name = name;
    }

    public MenuPage() {
        categories = new ArrayList<Category>();
    }

    public void addCategory(Category category) {
        categories.add(category);
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
