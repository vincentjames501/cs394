package com.streamlinedine.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.streamlinedine.R;
import com.streamlinedine.util.Result;

/**
 * Settings activity, housing application controls
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 3/6/12
 */
public class SettingsActivity extends Activity {
    private Intent mainScreenIntent;

    /**
     * Overrides the on create method in activity. Sets all necessary parameters and sets content view
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
        setContentView(R.layout.settings_layout);
        mainScreenIntent = new Intent(getBaseContext(), MainScreenActivity.class);
    }

    /**
     * User clicks the logout button
     * @param button the button context
     */
    public void logoutClick(View button) {
        Intent intent = new Intent(this, ConfirmActivity.class);
        intent.putExtra("POSITIVE", "Continue");
        intent.putExtra("NEGATIVE", "Cancel");
        intent.putExtra("MESSAGE", "Are you sure you wish to log out?");
        startActivityForResult(intent, 1);
    }

    /**
     * Overrides the activity result function. This is called when the user clicks
     * the logout button. If the result is confirmed, the user is logged out
     * @param requestCode the request code the activity was started with
     * @param resultCode the result code the activity returned with
     * @param data the data returned from the activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Result.CONFIRM) {
            startActivity(mainScreenIntent);
            finish();
        }
    }

    /**
     * If the user clicks the refresh button. Sets a flag so the menu knows to
     * refresh the menu next time it is opened. A message informs the user
     * that this will occur.
     * @param button the button context
     */
    public void refreshClick(View button) {
        MenuActivity.shouldRefresh = true;
        Intent intent = new Intent(this, ConfirmActivity.class);
        intent.putExtra("POSITIVE", "Ok");
        intent.putExtra("MESSAGE", "Next time you visit the menu, it will refresh. Thanks.");
        startActivity(intent);
    }

    /**
     * Overrides the dispatch touch event function in the activity. This allows the user to click
     * outside of this activity to close it.
     * @param ev the motion event that occurred.
     * @return boolean if the motion event was handled
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (MotionEvent.ACTION_OUTSIDE == ev.getAction()) {
            finish();
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }
}
