package com.streamlinedine.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import com.streamlinedine.R;
import com.streamlinedine.dialogs.ErrorAlert;
import com.streamlinedine.tasks.LoginTask;


/**
 * This activity is used for logging into the application
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 1/31/12
 */
public class LoginDialogActivity extends Activity {

    /**
     * Overrides the onCreate method in activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login_dialog);
    }

    /**
     * When the user clicks the login button
     * @param view the button context
     */
    public void loginClick(View view) {
        try {
            String username = ((EditText) findViewById(R.id.login_username)).getText().toString();
            String password = ((EditText) findViewById(R.id.login_password)).getText().toString();
            if (username.equals("") || password.equals("")) {
                throw new Exception("Error, please provide a username and password");
            }
            attemptLogin(username, password);
        } catch (Exception e) {
            ErrorAlert alert = new ErrorAlert(e.getMessage(), this);
            alert.show();
        }
    }

    /**
     * This function attempts the user login
     * @param username the username
     * @param password the password
     */
    private void attemptLogin(String username, String password) {
        LoginTask loginTask = new LoginTask(LoginDialogActivity.this, username, password);
        loginTask.execute();
    }

//    public void cancelClick(View view) {
//        Intent confirmIntent = new Intent(this, ConfirmActivity.class);
//        confirmIntent.putExtra("POSITIVE", "Continue");
//        confirmIntent.putExtra("NEGATIVE", "Cancel");
//        confirmIntent.putExtra("MESSAGE", "Are you sure? Clicking \"Continue\" will close the application.");
//        startActivityForResult(confirmIntent, 1);
//    }

    /**
     * Overrides the back press button provided by android to prevent leaving the login screen
     */
    @Override
    public void onBackPressed() {
        return;
    }
}
