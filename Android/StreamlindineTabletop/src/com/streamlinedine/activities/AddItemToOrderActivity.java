package com.streamlinedine.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import com.streamlinedine.R;
import com.streamlinedine.custom_component.NumberPicker;
import com.streamlinedine.menu.item.MenuItem;
import com.streamlinedine.order.Order;
import com.streamlinedine.order.OrderItem;

/**
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/16/12
 */
public class AddItemToOrderActivity extends Activity {
    private MenuItem menuItem;


    /**
     * Overrides onCreate method in Activity.
     * Removes window title from activity
     * Sets the content view to add_item_to_order.xml
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_item_to_order);
        initActivity();
    }

    /**
     * Initializes activity member variables. In this case, retrieves the
     * menu item from the intent extras
     */
    public void initActivity() {
        menuItem = (MenuItem) getIntent().getExtras().get("MENU_ITEM");
    }

    /**
     * When the user clicks the button to add the item to their cart
     *
     * @param button the button context
     */
    public void addItemButtonClick(View button) {
        int numberForItem = Integer.valueOf(((NumberPicker) findViewById(R.id.add_item_number_picker)).getmText().getText().toString());
        String comment = ((EditText) findViewById(R.id.add_item_comment)).getText().toString();
        Order.order.addOrderItem(new OrderItem(menuItem, comment, numberForItem));
        finish();
    }

    /**
     * When the user clicks the button to cancel the addition of the item
     *
     * @param button the button context
     */
    public void cancelButtonClick(View button) {
        finish();
    }
}
