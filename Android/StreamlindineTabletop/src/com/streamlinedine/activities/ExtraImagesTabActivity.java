package com.streamlinedine.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.streamlinedine.R;
import com.streamlinedine.custom_component.ExtraImageTile;
import com.streamlinedine.menu.item.MenuItem;

/**
 * This activity allows users to view additional images attached to a menu item
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @verion 1.0 3/11/12
 */
public class ExtraImagesTabActivity extends Activity {

    /**
     * Overrides the onCreate method of the activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.extra_images);

        MenuItem menuItem = (MenuItem) getIntent().getExtras().get("MENU_ITEM");

        TableLayout layout = (TableLayout) findViewById(R.id.extra_images_container);
        TableRow imageRow = new TableRow(this);
        TableRow.LayoutParams menuParams = buildAdditionalImageTileParams();

        if (menuItem.getAdditionalImages().size() > 0) {
            for (String additionalImage : menuItem.getAdditionalImages()) {
                addAdditionalImageTileToLayout(imageRow, menuParams, additionalImage);
            }
            layout.addView(imageRow);
        } else {
            addNoImageMessage();
        }
    }

    /**
     * Builds parameters that help format the additional image tiles.
     * @return TableRow.LayoutParams layout parameters for spacing image tiles
     */
    private TableRow.LayoutParams buildAdditionalImageTileParams() {
        TableRow.LayoutParams menuParams = new TableRow.LayoutParams();
        menuParams.span = 1;
        menuParams.setMargins(10, 0, 10, 0);
        return menuParams;
    }

    /**
     * Adds creates and adds the additional image tiles to the view
     * @param imageRow The table layout housing the additional image tiles
     * @param menuParams The layout params for each image tile
     * @param additionalImage The additional image
     */
    private void addAdditionalImageTileToLayout(TableRow imageRow, TableRow.LayoutParams menuParams, String additionalImage) {
        ExtraImageTile extraImageTile = new ExtraImageTile(this);
        imageRow.addView(extraImageTile, menuParams);
        extraImageTile.getLoadingImage().startImageDownload(additionalImage);
    }

    /**
     * If the item has no additional images, a message is shown instead
     */
    public void addNoImageMessage() {
        TextView textView = (TextView) findViewById(R.id.extra_images_no_extra_images);
        textView.setVisibility(View.VISIBLE);
    }
}
