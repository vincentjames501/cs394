package com.streamlinedine.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.streamlinedine.R;
import com.streamlinedine.menu.item.MenuItem;

/**
 * This class allows users to view the nutrition facts of an item
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 3/11/12
 */
public class NutritionFactsTabActivity extends Activity{

    /**
     * Overrides the on create method of the activity. Populates necessary fields and sets
     * content view.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nutrition_facts);

        MenuItem menuItem = (MenuItem)getIntent().getExtras().get("MENU_ITEM");

        populateNutritionData(menuItem);
    }

    /**
     * Populates the content view with obtained nutrition fact data.
     * @param menuItem the menu item whose nutrition information is to be viewed.
     */
    private void populateNutritionData(MenuItem menuItem) {
        for (String nutritionFact : menuItem.getNutritionFactMap().keySet()) {
            int resID = getResources().getIdentifier(nutritionFact, "id", getPackageName());
            TextView nutritionText = (TextView)findViewById(resID);
            if(nutritionText != null) {
                nutritionText.setText(menuItem.getNutritionFactMap().get(nutritionFact));
            }
        }
    }
}
