package com.streamlinedine.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.streamlinedine.R;
import com.streamlinedine.custom_component.LoadingImage;
import com.streamlinedine.menu.item.MenuItem;

import java.text.NumberFormat;

/**
 * This activity simply displays general item info to the user
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 3/11/12
 */
public class GeneralItemInfoTabActivity extends Activity {

    /**
     * Overrides the onCreate method in activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MenuItem menuItem = (MenuItem) getIntent().getExtras().get("MENU_ITEM");
        setContentView(R.layout.general_item_info);

        TextView menuItemName = (TextView) findViewById(R.id.general_item_info_name);
        TextView menuItemLongDescription = (TextView) findViewById(R.id.general_item_info_long_description);
        TextView menuItemPrice = (TextView) findViewById(R.id.general_item_info_price);

        LoadingImage loadingImage = (LoadingImage) findViewById(R.id.general_item_info_image);
        loadingImage.startImageDownload(menuItem.getMainImage());

        menuItemName.setText(menuItem.getName());
        if (!menuItem.getLongDescription().equals("")) {
            menuItemLongDescription.setText(menuItem.getLongDescription() + "\n");
        }
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        menuItemPrice.setText(formatter.format(menuItem.getPrice()));
    }
}
