package com.streamlinedine.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import com.streamlinedine.R;
import com.streamlinedine.custom_component.NumberPicker;
import com.streamlinedine.order.Order;

/**
 * This activity is for editing order items in the user's order.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 3/21/12
 */
public class EditOrderItemActivity extends Activity {

    /**
     * Overrides the onCreate method in Activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_item_to_order);
        initActivity();
    }

    /**
     * Populates the fields with the values of the order item the user is editing
     */
    public void initActivity() {
        EditText commentPopulation = ((EditText) findViewById(R.id.add_item_comment));
        commentPopulation.setText(Order.order.getEditingItem().getComment());

        NumberPicker numItems = ((NumberPicker) findViewById(R.id.add_item_number_picker));
        numItems.setMText(String.valueOf(Order.order.getEditingItem().getQuantity()));

        Button okButton = ((Button) findViewById(R.id.add_item_button));
        okButton.setText("Ok");
    }

    /**
     * When the user clicks to finish editing the order item
     * @param button the button context
     */
    public void addItemButtonClick(View button) {
        int numberForItem = Integer.valueOf(((NumberPicker) findViewById(R.id.add_item_number_picker)).getmText().getText().toString());
        String comment = ((EditText) findViewById(R.id.add_item_comment)).getText().toString();

        Order.order.getEditingItem().setComment(comment);
        Order.order.getEditingItem().setQuantity(numberForItem);
        finish();
    }

    /**
     * When the user clicks the cancel button
     * @param button the button context
     */
    public void cancelButtonClick(View button) {
        finish();
    }
}
