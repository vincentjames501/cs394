package com.streamlinedine.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import com.streamlinedine.R;

/**
 * This activity is the first activity screen that is loaded when the application starts.
 * It immediately launches a login dialog.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 1/31/12
 */
public class MainScreenActivity extends Activity {

    /**
     * Overrides the on start method to display the login dialog activity
     */
    @Override
    protected void onStart() {
        super.onStart();
        Intent loginIntent = new Intent(getBaseContext(), LoginDialogActivity.class);
        startActivityForResult(loginIntent, 1);
    }

    /**
     * Overrides the android on create method. Sets the content view and removes
     * the window title
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);
    }

    /**
     * Prevents the back button from being pressed
     */
    @Override
    public void onBackPressed() {
        return;
    }
}
