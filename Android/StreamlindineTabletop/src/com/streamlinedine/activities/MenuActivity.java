package com.streamlinedine.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ViewFlipper;
import com.streamlinedine.R;
import com.streamlinedine.dialogs.ErrorAlert;
import com.streamlinedine.menu.Menu;
import com.streamlinedine.order.Order;
import com.streamlinedine.tasks.RetrieveMenuTask;
import com.streamlinedine.util.Result;
import com.streamlinedine.util.server_communication.ServerErrorException;
import com.streamlinedine.util.server_communication.SubmitOrderService;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * This class is the menu activity. This activity houses the menu and its pages and menu items.
 * The main part of the application occurs here.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/15/12
 */
public class MenuActivity extends Activity {
    private ViewFlipper flipper;
    private Intent homeMenuIntent;
    private Intent viewOrderIntent;
    private boolean isPaused;
    public static boolean shouldRefresh;

    /**
     * Overrides the activity result function. This occurs when the user chooses to submit
     * their order from this activity.
     * @param requestCode the request code sent to the activity that returned the result
     * @param resultCode the result the activity returned with
     * @param data returned data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Result.CONFIRM) {
            SubmitOrderService submitOrderService = new SubmitOrderService();
            try {
                HttpResponse response = submitOrderService.execute();
                Order.order.clearOrder();
                JSONObject jsonObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                ErrorAlert alert = new ErrorAlert(jsonObject.getString("Message"), this);
                alert.show();
            } catch (ServerErrorException e) {
                ErrorAlert alert = new ErrorAlert(e.getMessage(), this);
                alert.show();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Prevent touch events from being dispatched when this activity is paused
     * @param ev the motion event
     * @return boolean if the event was dispatched
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!isPaused) {
            return super.dispatchTouchEvent(ev);    //To change body of overridden methods use File | Settings | File Templates.
        }
        return false;
    }

    /**
     * Overrides pause method setting a boolean flag to determine if paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
    }

    /**
     * Overrides resume method. If the activity is to be refreshed, the menu is retrieved and rendered
     */
    @Override
    protected void onResume() {
        super.onResume();
        isPaused = false;
        if (shouldRefresh) {
            flipper.removeAllViews();
            buildMenuPages();
            shouldRefresh = false;
        }
    }

    /**
     * Overrides the on create method of activity. Sets all necessary content views.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.menu_layout);
        flipper = (ViewFlipper) findViewById(R.id.view_flipper);
        buildMenuPages();
        homeMenuIntent = new Intent(getBaseContext(), HomeMenuActivity.class);
        homeMenuIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        viewOrderIntent = new Intent(getBaseContext(), ViewOrderActivity.class);

        isPaused = false;
        shouldRefresh = false;
    }

    /**
     * Begins the menu retrieval task
     */
    private void buildMenuPages() {
        new RetrieveMenuTask(this).execute();
    }

    /**
     * User clicks the next arrow button
     * @param view the button context
     */
    public void nextAction(View view) {
        Menu.instance.flipToNextPage();
        prepareViewFlipper(R.anim.slide_in_right, R.anim.slide_out_left);
        flipper.setDisplayedChild(Menu.instance.getCurrentPage());
    }

    /**
     * User clicks the previous arrow button
     * @param view the button context
     */
    public void previousAction(View view) {
        Menu.instance.flipToPreviousPage();
        prepareViewFlipper(R.anim.slide_in_left, R.anim.slide_out_right);
        flipper.setDisplayedChild(Menu.instance.getCurrentPage());
    }

    /**
     * User clicks the home button
     * @param button the button context
     */
    public void homeButtonClick(View button) {
        startActivity(homeMenuIntent);
    }

    /**
     * User clicks the view order button
     * @param button the button context
     */
    public void orderButtonClick(View button) {
        startActivity(viewOrderIntent);
    }

    /**
     * User clicks to submit their order
     * @param button the button context
     */
    public void submitOrderClick(View button) {
        Intent confirmIntent = new Intent(this, ConfirmActivity.class);
        if (Order.order.getOrderItems().size() > 0) {
            confirmIntent.putExtra("POSITIVE", "Ready");
            confirmIntent.putExtra("NEGATIVE", "Cancel");
            confirmIntent.putExtra("MESSAGE", "Are you sure you're ready to submit your order?");
            startActivityForResult(confirmIntent, 1);
        } else {
            confirmIntent.putExtra("POSITIVE", "Ok");
            confirmIntent.putExtra("MESSAGE", "Your order is empty, please add items from the menu");
            startActivity(confirmIntent);
        }
    }

    /**
     * Prepare the view flipper with necessary animations
     * @param animationIn the animation with which the view will move in
     * @param animationOut the animation with which the view will move out
     */
    private void prepareViewFlipper(int animationIn, int animationOut) {
        flipper.setInAnimation(this, animationIn);
        flipper.setOutAnimation(this, animationOut);
    }
}
