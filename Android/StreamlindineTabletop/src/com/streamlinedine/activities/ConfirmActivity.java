package com.streamlinedine.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.streamlinedine.R;
import com.streamlinedine.util.Result;

/**
 * This class is a general confirm/cancel dialog that is useful throughout the application
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/3/12
 */
public class ConfirmActivity extends Activity {

    /**
     * Overrides the onCreate method of the activity
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.confirm_box);
        Button positiveButton = (Button) findViewById(R.id.confirm_dialog_positive);
        Button negativeButton = (Button) findViewById(R.id.confirm_dialog_negative);
        TextView messageText = (TextView) findViewById(R.id.confirm_dialog_text);

        retrieveExtras(positiveButton, negativeButton, messageText);
    }

    /**
     * Retrieves the extras passed through the intent in order to correctly populate the dialog
     *
     * @param positiveButton the positive action button
     * @param negativeButton the negative action button
     * @param messageText    the message text for the dialog
     */
    private void retrieveExtras(Button positiveButton, Button negativeButton, TextView messageText) {
        if (getIntent().hasExtra("POSITIVE")) {
            positiveButton.setText(getIntent().getExtras().getString("POSITIVE"));
        }
        if (getIntent().hasExtra("NEGATIVE")) {
            negativeButton.setText(getIntent().getExtras().getString("NEGATIVE"));
        } else {
            negativeButton.setVisibility(View.GONE);
        }
        if (getIntent().hasExtra("MESSAGE")) {
            messageText.setText(getIntent().getExtras().getString("MESSAGE"));
        }
    }

    /**
     * The user clicks the positive action button. The activity result is set to 1 to implicate a positive result
     *
     * @param view button context
     */
    public void posAction(View view) {
        setResult(Result.CONFIRM);
        finish();
    }

    /**
     * The user clicks the negative action button. The activity result is set to -1 to implicate a negative result
     *
     * @param view button context
     */
    public void negAction(View view) {
        setResult(Result.CANCEL);
        finish();
    }
}
