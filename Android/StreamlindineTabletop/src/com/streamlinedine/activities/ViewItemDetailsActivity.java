package com.streamlinedine.activities;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.*;
import android.widget.TabHost;
import com.streamlinedine.R;
import com.streamlinedine.menu.item.MenuItem;

/**
 * This activity houses the details of a menu item. There are three tabs: General Item Info,
 * Nutrition Facts, and Additional Images. Each tab has its own activity.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/24/12
 */
public class ViewItemDetailsActivity extends TabActivity {

    /**
     * Overrides the on create method in activity. This creates three separate tabs for item details.
     * Tab 1: The general item info tab
     * Tab 2: The item nutrition facts tab
     * Tab 3: The item extra images tab
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        setContentView(R.layout.view_item_details);

        MenuItem menuItem = (MenuItem) getIntent().getExtras().get("MENU_ITEM");

        Resources resources = getResources();
        TabHost tabHost = getTabHost();
        createGeneralItemInfoTab(menuItem, resources, tabHost);

        createNutritionFactsTab(menuItem, resources, tabHost);

        createExtraImagesTab(menuItem, resources, tabHost);
    }

    /**
     * Creates the extra images tab
     * @param menuItem the menu item for which to create the tab
     * @param resources the application resources
     * @param tabHost the tab host
     */
    private void createExtraImagesTab(MenuItem menuItem, Resources resources, TabHost tabHost) {
        Intent intent;
        TabHost.TabSpec tabSpec;
        intent = new Intent().setClass(this, ExtraImagesTabActivity.class);
        intent.putExtra("MENU_ITEM", menuItem);
        tabSpec = tabHost.newTabSpec("extra_images").setIndicator("Images", resources.getDrawable(R.drawable.picture)).setContent(intent);
        tabHost.addTab(tabSpec);
    }

    /**
     * Creates the nutrition facts tab
     * @param menuItem the menu item for which to create the tab
     * @param resources the application resources
     * @param tabHost the tab host
     */
    private void createNutritionFactsTab(MenuItem menuItem, Resources resources, TabHost tabHost) {
        Intent intent;
        TabHost.TabSpec tabSpec;
        intent = new Intent().setClass(this, NutritionFactsTabActivity.class);
        intent.putExtra("MENU_ITEM", menuItem);
        tabSpec = tabHost.newTabSpec("nutrition_facts").setIndicator("Nutrition Facts", resources.getDrawable(R.drawable.nutrition)).setContent(intent);
        tabHost.addTab(tabSpec);
    }

    /**
     * Creates the general item info tab
     * @param menuItem the menu item for which to create the tab
     * @param resources the application resources
     * @param tabHost the tab host
     */
    private void createGeneralItemInfoTab(MenuItem menuItem, Resources resources, TabHost tabHost) {
        Intent intent;
        TabHost.TabSpec tabSpec;
        intent = new Intent().setClass(this, GeneralItemInfoTabActivity.class);
        intent.putExtra("MENU_ITEM", menuItem);
        tabSpec = tabHost.newTabSpec("general_info").setIndicator("General Info", resources.getDrawable(R.drawable.info_icon_tab)).setContent(intent);
        tabHost.addTab(tabSpec);
    }

    /**
     * Overrides the dispatch touch event function. This allows the user to click outside the activity to close it
     * @param ev the motion event
     * @return boolean if the touch event was dispatched
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (MotionEvent.ACTION_OUTSIDE == ev.getAction()) {
            finish();
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }
}
