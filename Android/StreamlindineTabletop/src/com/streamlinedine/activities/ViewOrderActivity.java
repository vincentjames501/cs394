package com.streamlinedine.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import com.streamlinedine.R;
import com.streamlinedine.dialogs.ErrorAlert;
import com.streamlinedine.util.Result;
import com.streamlinedine.util.SubmitOrderAnimationListener;
import com.streamlinedine.util.server_communication.*;
import com.streamlinedine.menu.item.OrderItemRow;
import com.streamlinedine.order.Order;
import com.streamlinedine.order.OrderItem;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * This activity houses the user's order. Each item the user has in their order will be
 * visible.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/14/12
 */
public class ViewOrderActivity extends Activity {

    private LinearLayout orderItemContainer;
    private Intent menuIntent;
    private Intent homeIntent;

    /**
     * Overrides the on resume method for the activity. Refreshed the views, in case
     * the user has since added items to their order
     */
    @Override
    protected void onResume() {
        super.onResume();

        LinearLayout layout = (LinearLayout) findViewById(R.id.view_order_layout);
        layout.removeAllViews();
        buildOrderItems(layout);
    }

    /**
     * Simply adds order item rows to the order item container
     * @param layout the layout container housing the order items
     */
    private void buildOrderItems(LinearLayout layout) {
        List<OrderItem> orderItems = Order.order.getOrderItems();
        for (OrderItem orderItem : orderItems) {
            layout.addView(new OrderItemRow(this, orderItem));
        }
    }

    /**
     * Overrides the oncreate method of the activity. This sets necessary parameters
     * and sets content view
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.view_order_layout);

        orderItemContainer = (LinearLayout) findViewById(R.id.view_order_layout);
        menuIntent = new Intent(getBaseContext(), MenuActivity.class);
        menuIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        homeIntent = new Intent(getBaseContext(), HomeMenuActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
    }

    /**
     * User clicks the home button to return to the home screen
     * @param button the button context
     */
    public void homeButtonClick(View button) {
        startActivity(homeIntent);
    }

    /**
     * User clicks the menu button to return to the menu screen
     * @param view the button context
     */
    public void menuButtonClick(View view) {
        startActivity(menuIntent);
    }

    /**
     * Overrides the activity result method in activity. This is used for the submit
     * confirmation. If the user confirms their order submit, an animation is played, the
     * user is confirmed of their successful order, and they are returned to the menu
     * activity
     * @param requestCode the request code with which the confirm activity was started
     * @param resultCode the result code returned by the confirm activity
     * @param data the data returned by the confirm activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Result.CONFIRM) {
            try {
                SubmitOrderService submitOrderService = new SubmitOrderService();
                HttpResponse response = submitOrderService.execute();
                JSONObject jsonObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                Animation fadeOrderAnimation = AnimationUtils.loadAnimation(ViewOrderActivity.this, R.anim.fade_out);
                orderItemContainer.setAnimation(fadeOrderAnimation);
                fadeOrderAnimation.setAnimationListener(new SubmitOrderAnimationListener(this, orderItemContainer, jsonObject.getString("Message")));
                fadeOrderAnimation.start();
            } catch (ServerErrorException e) {
                ErrorAlert alert = new ErrorAlert(e.getMessage(), this);
                alert.show();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * If the user clicks the submit order button. If the user can submit the order
     * a confirmation dialog is displayed. If the user cannot submit their order, they are
     * informed as such.
     * @param button the button context
     */
    public void submitOrderClick(View button) {
        Intent confirmIntent = new Intent(this, ConfirmActivity.class);
        if (Order.order.getOrderItems().size() > 0) {
            confirmIntent.putExtra("POSITIVE", "Ready");
            confirmIntent.putExtra("NEGATIVE", "Cancel");
            confirmIntent.putExtra("MESSAGE", "Are you sure you're ready to submit your order?");
            startActivityForResult(confirmIntent, 1);
        } else {
            confirmIntent.putExtra("POSITIVE", "Ok");
            confirmIntent.putExtra("MESSAGE", "Your order is empty, please add items from the menu");
            startActivity(confirmIntent);
        }
    }

    /**
     * Gets the menu intent
     * @return Intent the menu intent
     */
    public Intent getMenuIntent() {
        return menuIntent;
    }
}
