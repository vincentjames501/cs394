package com.streamlinedine.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import com.streamlinedine.R;

/**
 * This activity is the launchpad for the application. It features two large tiles for navigation
 * between the menu and the user's order.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 3/6/12
 */
public class HomeMenuActivity extends Activity  {
    private Intent menuIntent;
    private Intent orderIntent;
    private Intent settingsIntent;
    private boolean isPaused;

    /**
     * Overrides the onCreate method in activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.home_main_layout);
        menuIntent = new Intent(getBaseContext(), MenuActivity.class);
        menuIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        orderIntent = new Intent(getBaseContext(), ViewOrderActivity.class);
        settingsIntent = new Intent(getBaseContext(), SettingsActivity.class);
        isPaused = false;
    }

    /**
     * When the activity is paused, sets a boolean flag
     */
    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
    }

    /**
     * When the activity is resumed, sets a boolean flag to false
     */
    @Override
    protected void onResume() {
        super.onResume();
        isPaused = false;
    }

    /**
     * If the activity is paused, this method will ignore the dispatchment of touch events
     * @param ev the motion event that occurred on the activity
     * @return returns a boolean value
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(!isPaused) {
            return super.dispatchTouchEvent(ev);
        } else {
            return false;
        }
    }

    /**
     * If the menu tile is clicked, starts the MenuActivity
     * @param button
     */
    public void menuTileClick(View button) {
        startActivity(menuIntent);
    }

    /**
     * If the order tile is clicked, starts the ViewOrderActivity
     * @param button
     */
    public void orderTileClick(View button) {
        startActivity(orderIntent);
    }

    /**
     * If the settings tile is clicked, starts the SettingsActivity
     * @param button
     */
    public void menuSettingsClick(View button) {
        startActivity(settingsIntent);
    }
}
