package com.streamlinedine.util;

import com.streamlinedine.menu.Menu;
import com.streamlinedine.menu.category.Category;
import com.streamlinedine.menu.item.MenuItem;
import com.streamlinedine.menu.page.MenuPage;
import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for building the java representation of the menu
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/13/12
 */
public class MenuBuilder {
    public static final String MENUS = "menus";
    public static final String MENU_PAGES = "menuPages";
    public static final String NAME = "name";
    public static final String PAGE_NUMBER = "pageNumber";
    public static final String CATEGORIES = "categories";
    public static final String MENU_ITEMS = "menuItems";
    public static final String RATING = "rating";
    public static final String TIMES_ORDERED = "timesOrdered";
    public static final String PRICE = "price";
    private static final String IGNORE_CATEGORY = "MONETARY_OPERATIONS";
    public static final String IMAGE = "image";
    public static final String SHORT_DESCRIPTION = "shortDescription";
    public static final String LONG_DESCRIPTION = "longDescription";
    public static final String ADDITIONAL_IMAGES = "additionalImages";
    public static final String NUTRITION_FACTS = "nutritionFacts";
    public static final String TYPE = "type";
    public static final String FACT = "fact";
    public static final String ID = "id";

    private HttpResponseDecoder responseDecoder;

    /**
     * Default constructor. Initializes the HttpResponseDecoder object
     */
    public MenuBuilder() {
        responseDecoder = new HttpResponseDecoder();
    }

    /**
     * Top level function. Builds and returns the menu object from the JSON
     * @param response the response object that is received from the server
     * @return Menu the menu built from the JSON from the HttpResponse
     * @throws Exception if there was an error converting the menu
     */
    public Menu buildMenu(HttpResponse response) throws Exception {
        Menu menu = new Menu();
        String responseString = responseDecoder.getStringFromResponse(response);
        JSONArray responseArray = new JSONArray(responseString);
        JSONArray menuPages = getMenuPagesFromResponse(responseArray);
        menu.setName(responseArray.getJSONObject(0).getString(NAME));

        for (int i = 0; i < menuPages.length(); i++) {
            MenuPage menuPage = buildMenuPageFromJson(menuPages.getJSONObject(i));
            addMenuItemsFromPageIntoMenuItemMap(menu, menuPage);
            menu.addMenuPage(menuPage);
        }
        return menu;
    }

    /**
     * Adds all items in a menu page into the menu's menu item map
     * @param menu the menu to add the items to
     * @param menuPage the menu page
     */
    private void addMenuItemsFromPageIntoMenuItemMap(Menu menu, MenuPage menuPage) {
        for (Category category : menuPage.getCategories()) {
            for (MenuItem menuItem : category.getMenuItems()) {
                menu.getMenuItemMap().put(menuItem.getItemId(), menuItem);
            }
        }
    }

    /**
     * Builds a menu page object from JSON
     * @param menuPageObject the JSON representation of the MenuPage
     * @return MenuPage a menu page built from the JSON object
     * @throws JSONException if there was an error converting from JSON to Java objects
     */
    public MenuPage buildMenuPageFromJson(JSONObject menuPageObject) throws JSONException {
        MenuPage menuPage = new MenuPage();
        menuPage.setPageNumber(menuPageObject.getInt(PAGE_NUMBER));
        JSONArray categoryArray = menuPageObject.getJSONArray(CATEGORIES);
        for(int i = 0; i < categoryArray.length(); i++) {
            JSONObject categoryObject = categoryArray.getJSONObject(i);
            if(acceptedCategory(categoryObject)) {
                menuPage.addCategory(buildCategoryFromJson(categoryObject));
            }
        }
        return menuPage;
    }

    /**
     * Builds a category object from JSON
     * @param categoryObject the JSON representation of the Category
     * @return Category the category object built from JSON
     * @throws JSONException if there was an error converting from JSON to Java objects
     */
    public Category buildCategoryFromJson(JSONObject categoryObject) throws JSONException {
        Category category = new Category();
        category.setCategoryName(categoryObject.getString(NAME));
        JSONArray menuItemArray = categoryObject.getJSONArray(MENU_ITEMS);
        for(int i = 0; i < menuItemArray.length(); i++) {
            category.addMenuItem(buildMenuItemFromJson(menuItemArray.getJSONObject(i)));
        }
        return category;
    }

    /**
     * If the category is of an accepted type. At present, we ignore categories that are
     * for monetary operations.
     * @param categoryObject the category object being checked
     * @return boolean if the category is accepted
     * @throws JSONException if there was an error retrieving fields from the JSON object
     */
    public boolean acceptedCategory(JSONObject categoryObject) throws JSONException {
        return !categoryObject.getString(NAME).equalsIgnoreCase(IGNORE_CATEGORY);
    }

    /**
     * Gets a JSONArray for all menu pages
     * @param responseArray the JSONArray returned from the server
     * @return JSONArray an array of JSON representations of MenuPages
     * @throws JSONException if there was an error retrieving the specified field
     */
    public JSONArray getMenuPagesFromResponse(JSONArray responseArray) throws JSONException {
        return responseArray.getJSONObject(0).getJSONArray(MENUS).getJSONObject(0).getJSONArray(MENU_PAGES);
    }

    /**
     * Builds a MenuItem object from JSON
     * @param menuItemObject the JSON representation of the MenuItem
     * @return MenuItem the menu item object built from the JSON
     * @throws JSONException if there was an error converting from JSON to Java object
     */
    public MenuItem buildMenuItemFromJson(JSONObject menuItemObject) throws JSONException {
        MenuItem menuItem = new MenuItem();
        menuItem.setItemId(menuItemObject.getInt(ID));
        menuItem.setRating(menuItemObject.getString(RATING));
        menuItem.setTimesOrdered(menuItemObject.getInt(TIMES_ORDERED));
        menuItem.setName(menuItemObject.getString(NAME));
        menuItem.setPrice(Float.valueOf(menuItemObject.getString(PRICE)));
        menuItem.setMainImage(menuItemObject.getString(IMAGE));
        menuItem.setShortDescription(menuItemObject.getString(SHORT_DESCRIPTION));
        menuItem.setLongDescription(menuItemObject.getString(LONG_DESCRIPTION));
        menuItem.setAdditionalImages(buildAdditionalImagesListFromJson(menuItemObject.getJSONArray(ADDITIONAL_IMAGES)));
        menuItem.setNutritionFactMap(buildNutritionFactMap(menuItemObject.getJSONArray(NUTRITION_FACTS)));
        return menuItem;
    }

    /**
     * Builds an array of strings for a MenuItem's additional images
     * @param additionalImagesJsonArray the JSONArray of additional images
     * @return List<String> the list of additional images
     * @throws JSONException if there was an error getting the additional images from the JSONArray
     */
    public List<String> buildAdditionalImagesListFromJson(JSONArray additionalImagesJsonArray) throws JSONException {
        List<String> additionalImages = new ArrayList<String>();
        for(int x = 0; x < additionalImagesJsonArray.length(); x++) {
            additionalImages.add(additionalImagesJsonArray.getJSONObject(x).get(IMAGE).toString());
        }
        return additionalImages;
    }

    /**
     * Builds a map of String to String values representing a MenuItem's nutrition facts
     * @param nutritionFactArray The JSONArray of nutrition facts for a MenuItem
     * @return Map<String, String> the map of String to String values representing a nutrition fact
     * @throws JSONException if there was an error getting the nutrition facts from the JSONArray
     */
    public Map<String, String> buildNutritionFactMap(JSONArray nutritionFactArray) throws JSONException {
        Map<String, String> nutritionFactMap = new HashMap<String, String>();
        for(int x = 0; x < nutritionFactArray.length(); x++) {
            JSONObject nutritionFactObject = nutritionFactArray.getJSONObject(x);
            nutritionFactMap.put(nutritionFactObject.getString(MenuBuilder.TYPE), nutritionFactObject.getString(MenuBuilder.FACT));
        }
        return nutritionFactMap;
    }

    /**
     * Gets the HttpResponseDecoder object
     * @return HttpResponseDecoder the HttpResponseDecoder object
     */
    public HttpResponseDecoder getResponseDecoder() {
        return responseDecoder;
    }

    /**
     * Sets the HttpResponseDecoder object
     * @param responseDecoder the HttpResponseDecoderObject
     */
    public void setResponseDecoder(HttpResponseDecoder responseDecoder) {
        this.responseDecoder = responseDecoder;
    }
}
