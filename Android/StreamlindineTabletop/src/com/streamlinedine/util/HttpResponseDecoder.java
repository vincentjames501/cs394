package com.streamlinedine.util;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Helper class to decode an httpresponse entity
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/16/12
 */
public class HttpResponseDecoder {

    /**
     * Returns the string converted HttpEntity
     * @param response the response from which to get the string entity
     * @return String the converted response entity
     * @throws IOException if there was an error converting the entity
     */
    public String getStringFromResponse(HttpResponse response) throws IOException {
        return EntityUtils.toString(response.getEntity());
    }
}
