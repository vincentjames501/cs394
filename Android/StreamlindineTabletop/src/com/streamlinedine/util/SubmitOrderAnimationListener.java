package com.streamlinedine.util;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import com.streamlinedine.activities.ViewOrderActivity;
import com.streamlinedine.order.Order;

/**
 * Animation listener for removing a user's order from the order view
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/13/12
 */
public class SubmitOrderAnimationListener implements Animation.AnimationListener {
    private LinearLayout orderItemContainer;
    private ViewOrderActivity viewOrderActivity;
    private String message;

    /**
     * Default constructor
     * @param viewOrderActivity the ViewOrderActivity
     * @param orderItemContainer the linear layout that holds all order items
     * @param message the message to be displayed when the animation ends
     */
    public SubmitOrderAnimationListener(ViewOrderActivity viewOrderActivity, LinearLayout orderItemContainer, String message) {
        this.viewOrderActivity = viewOrderActivity;
        this.orderItemContainer = orderItemContainer;
        this.message = message;
    }

    /**
     * Does nothing
     * @param animation the animation
     */
    @Override
    public void onAnimationStart(Animation animation) {

    }

    /**
     * When the animation is completed, an alert is shown and the order is cleared
     * @param animation the animation
     */
    @Override
    public void onAnimationEnd(Animation animation) {
        orderItemContainer.removeAllViews();
        Order.order.clearOrder();
        AlertDialog.Builder builder = new AlertDialog.Builder(viewOrderActivity);
        builder.setMessage(message).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                viewOrderActivity.startActivity(viewOrderActivity.getMenuIntent());
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Does nothing
     * @param animation the animation
     */
    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
