package com.streamlinedine.util.button_listeners;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import com.streamlinedine.R;
import com.streamlinedine.menu.item.OrderItemRow;
import com.streamlinedine.order.Order;
import com.streamlinedine.order.OrderItem;

/**
 * Click listener for removing an order item
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 3/21/12
 */
public class RemoveOrderItemButtonListener implements View.OnClickListener{
    OrderItem orderItem;
    View parentView;
    Context context;

    /**
     * Default constructor
     * @param orderItem the order item associated with this listener
     * @param orderItemRow the orderItemRow that owns this listener
     * @param context the context
     */
    public RemoveOrderItemButtonListener(OrderItem orderItem, OrderItemRow orderItemRow, Context context) {
        this.orderItem = orderItem;
        parentView = orderItemRow;
        this.context = context;
    }

    /**
     * Overrides the onClick method. It removes the OrderItemRow associated with this listener and also
     * removes the associated orderItem.
     * @param view the button context
     */
    public void onClick(View view) {
        LinearLayout linearLayout = (LinearLayout) ((Activity)context).findViewById(R.id.view_order_layout);
        linearLayout.removeView(parentView);
        Order.order.removeOrderItem(orderItem);
    }
}
