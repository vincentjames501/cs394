package com.streamlinedine.util.button_listeners;

import android.content.Intent;
import android.view.View;
import com.streamlinedine.activities.ViewItemDetailsActivity;
import com.streamlinedine.menu.item.MenuItem;

/**
 * Click listener for viewing item details
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/24/12
 */
public class DescriptionButtonListener implements View.OnClickListener {
    private MenuItem menuItem;

    /**
     * Default constructor
     *
     * @param menuItem the menu item associated with this listener
     */
    public DescriptionButtonListener(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    /**
     * Overrides the onClick method. Launches the ViewItemDetailsActivity with this
     * listener's menuItem
     * @param view
     */
    public void onClick(View view) {
        Intent detailsIntent = new Intent(view.getContext(), ViewItemDetailsActivity.class);
        detailsIntent.putExtra("MENU_ITEM", menuItem);
        view.getContext().startActivity(detailsIntent);
    }
}
