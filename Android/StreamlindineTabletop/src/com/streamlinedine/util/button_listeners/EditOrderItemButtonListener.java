package com.streamlinedine.util.button_listeners;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.streamlinedine.activities.EditOrderItemActivity;
import com.streamlinedine.order.Order;
import com.streamlinedine.order.OrderItem;

/**
 * Click listener for editing an order item
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 3/21/12
 */
public class EditOrderItemButtonListener implements View.OnClickListener {

    private Context context;
    private OrderItem orderItem;

    /**
     * Default constructor
     * @param context the context
     * @param orderItem the orderItem for which this listener is associated
     */
    public EditOrderItemButtonListener(Context context, OrderItem orderItem) {
        this.context = context;
        this.orderItem = orderItem;
    }

    /**
     * Overrides the onClick method. This sets the item being edited in the Order
     * and starts the EditOrderItemActivity
     * @param view the button context
     */
    public void onClick(View view) {
        Order.order.setEditingItem(orderItem);
        Intent editIntent = new Intent(context, EditOrderItemActivity.class);
        context.startActivity(editIntent);
    }
}
