package com.streamlinedine.util.button_listeners;

import android.content.Intent;
import android.view.View;
import com.streamlinedine.activities.AddItemToOrderActivity;
import com.streamlinedine.menu.item.MenuItem;

/**
 * Click listener for adding menu items to order
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/23/12
 */
public class AddItemButtonListener implements View.OnClickListener {
    private MenuItem menuItem;

    /**
     * Default constructor
     * @param menuItem the menu item associated with this listener
     */
    public AddItemButtonListener(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    /**
     * Overrides the onClick method. This will launch the AddItemToOrderActivity
     * with this listener's menuItem
     * @param view the button context
     */
    public void onClick(View view) {
        Intent addItemIntent = new Intent(view.getContext(),AddItemToOrderActivity.class);
        addItemIntent.putExtra("MENU_ITEM", menuItem);
        view.getContext().startActivity(addItemIntent);
    }
}
