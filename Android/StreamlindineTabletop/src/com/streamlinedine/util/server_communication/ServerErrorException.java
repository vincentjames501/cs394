package com.streamlinedine.util.server_communication;

/**
 * Custom exception for problems with server communication
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/12/12
 */
public class ServerErrorException extends Exception{

    /**
     * Custom exception in handling server communication
     * @param message the message attached to the exception
     */
    public ServerErrorException(String message) {
        super(message);
    }
}
