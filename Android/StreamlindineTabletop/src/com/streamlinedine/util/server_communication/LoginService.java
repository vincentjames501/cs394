package com.streamlinedine.util.server_communication;

import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Service for logging the tablet device into the system
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/12/12
 */
public class LoginService implements ServiceInterface{
    private String username;
    private String password;
    private HttpPostBuilder postBuilder;
    private StreamlineService streamlineService;

    /**
     * Default constructor
     */
    public LoginService() {
        postBuilder = new HttpPostBuilder();
        streamlineService = new StreamlineService();
        username = "";
        password = "";
    }

    /**
     * Overrides the execute method. Creates an http post method for logging in. Attaches the username
     * and password to the request and executes the request.
     * @return HttpResponse the response returned from the request
     * @throws UnsupportedEncodingException if the name-value pairings were unsuccessfully created
     * @throws ServerErrorException if there was an error with server communication
     */
    @Override
    public HttpResponse execute() throws UnsupportedEncodingException, ServerErrorException {
        HttpPost post = postBuilder.buildHttpPost(StreamlineService.LOGIN);
        List<BasicNameValuePair> nameValues = new ArrayList<BasicNameValuePair>();
        nameValues.add(new BasicNameValuePair("username", username));
        nameValues.add(new BasicNameValuePair("password", password));
        post.setEntity(new UrlEncodedFormEntity(nameValues));
        streamlineService.setRequest(post);
        return streamlineService.execute();
    }

    /**
     * Gets the username
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the password
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the HttpPostBuilder
     * @return HttpPostBuilder the HttpPostBuilder
     */
    public HttpPostBuilder getPostBuilder() {
        return postBuilder;
    }

    /**
     * Sets the HttpPostBuilder
     * @param postBuilder the HttpPostBuilder being set
     */
    public void setPostBuilder(HttpPostBuilder postBuilder) {
        this.postBuilder = postBuilder;
    }

    /**
     * Gets the StreamlineService helper
     * @return StreamlineService the StreamlineService helper
     */
    public StreamlineService getStreamlineService() {
        return streamlineService;
    }

    /**
     * Sets the StreamlinService helper
     * @param streamlineService the StreamlineService helper
     */
    public void setStreamlineService(StreamlineService streamlineService) {
        this.streamlineService = streamlineService;
    }
}
