package com.streamlinedine.util.server_communication;

/**
 * Simply houses the application domain
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/7/12
 */
public class Domain {
    public static String domain = "http://streamlinedine.com";
}
