package com.streamlinedine.util.server_communication;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

/**
 * Service to retrieve the menu from the server
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/12/12
 */
public class GetMenuService implements ServiceInterface {

    private StreamlineService streamlineService;

    /**
     * Default constructor. Creates a new streamline service helper.
     */
    public GetMenuService() {
        streamlineService = new StreamlineService();
    }

    /**
     * Overrides the execute method. Creates an HttpGet request to
     * retrieve the menu.
     * @return HttpResponse the response from the server
     * @throws ServerErrorException can potentially have an error while retrieving the
     * menu.
     */
    public HttpResponse execute() throws ServerErrorException {
        HttpGet getMenuRequest = new HttpGet(StreamlineService.GET_MENU);
        getMenuRequest.addHeader("Cookie", StreamlineService.cookie);
        streamlineService.setRequest(getMenuRequest);
        return streamlineService.execute();
    }

    /**
     * Gets the StreamlineService helper object
     * @return StreamlineService object
     */
    public StreamlineService getStreamlineService() {
        return streamlineService;
    }

    /**
     * Sets the StreamlineService helper object
     * @param streamlineService the StreamlineService object
     */
    public void setStreamlineService(StreamlineService streamlineService) {
        this.streamlineService = streamlineService;
    }
}
