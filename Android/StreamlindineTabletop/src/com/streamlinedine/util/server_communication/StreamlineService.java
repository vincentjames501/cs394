package com.streamlinedine.util.server_communication;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;

/**
 * Helper class for server communication
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/12/12
 */
public class StreamlineService {
    public static final String LOGIN = Domain.domain + "/Menu/doLogin";
    public static final String SUBMIT_ORDER = Domain.domain + "/Menu/createNewOrder";
    public static final String GET_MENU = Domain.domain + "/Menu/getMenu";
    public static final int SUCCESS = 200;
    public static String cookie;

    private HttpUriRequest request;
    private HttpClient client;

    /**
     * Default constructor initializes the client
     */
    public StreamlineService() {
        HttpParams params = new BasicHttpParams();
        client = new DefaultHttpClient(params);
    }

    /**
     * The execute method which performs the HttpUriRequest
     * @return HttpResponse the response returned from the service
     * @throws ServerErrorException if there was an error communicating with the server
     */
    public HttpResponse execute() throws ServerErrorException {
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            throw new ServerErrorException("Error");
        }
        if(response.getStatusLine().getStatusCode() != SUCCESS) {
            throw new ServerErrorException("There was an error completing your request, we're sorry.");
        }
        return response;
    }

    /**
     * Gets the request object
     * @return HttpUriRequest the request object
     */
    public HttpUriRequest getRequest() {
        return request;
    }

    /**
     * Sets the HttpUriRequest object
     * @param request the request object
     */
    public void setRequest(HttpUriRequest request) {
        this.request = request;
    }

    /**
     * Gets the HttpClient
     * @return HttpClient the client
     */
    public HttpClient getClient() {
        return client;
    }

    /**
     * Sets the HttpClient
     * @param client the HttpClient
     */
    public void setClient(HttpClient client) {
        this.client = client;
    }
}
