package com.streamlinedine.util.server_communication;

import org.apache.http.client.methods.HttpPost;

/**
 * Utility class for building an HttpPost object
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/12/12
 */
public class HttpPostBuilder {

    /**
     * Helper function to create a new HttpPost object with a specified method
     * @param method the remote location
     * @return HttpPost the post object created
     */
    public HttpPost buildHttpPost(String method) {
        return new HttpPost(method);
    }
}
