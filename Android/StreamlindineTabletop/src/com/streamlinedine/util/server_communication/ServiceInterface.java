package com.streamlinedine.util.server_communication;

import java.io.UnsupportedEncodingException;

/**
 * Simple interface for handling server communication
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/12/12
 */
public interface ServiceInterface {
    /**
     * Abstract execute method implemented by all ServiceInterfaces
     * @return HttpResponse the response returned from the request
     * @throws UnsupportedEncodingException when adding name value pairs
     * @throws ServerErrorException when communicating with the server
     */
    public org.apache.http.HttpResponse execute() throws UnsupportedEncodingException, ServerErrorException;
}
