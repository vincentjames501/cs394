package com.streamlinedine.util.server_communication;

import com.google.gson.Gson;
import com.streamlinedine.order.Order;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Service for submitting a user's order
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/12/12
 */
public class SubmitOrderService implements ServiceInterface {
    private StreamlineService streamlineService;

    private HttpPostBuilder httpPostBuilder;

    private Gson gson;

    /**
     * Default constructor
     */
    public SubmitOrderService() {
        streamlineService = new StreamlineService();
        httpPostBuilder = new HttpPostBuilder();
        gson = new Gson();
    }

    /**
     * Overrides the execute method. This will create the HttpPost object and submit the order
     * by converting it to JSON.
     * @return HttpResponse a response from the server
     * @throws UnsupportedEncodingException if there was an error adding the name-value pairs attached to the
     * request.
     * @throws ServerErrorException if there was an error communicating with the server.
     */
    public HttpResponse execute() throws UnsupportedEncodingException, ServerErrorException {
        String orderJson = gson.toJson(Order.order.getOrderItems());

        HttpPost post = httpPostBuilder.buildHttpPost(StreamlineService.SUBMIT_ORDER);
        post.addHeader("Cookie", StreamlineService.cookie);
        List<NameValuePair> orderData = new ArrayList<NameValuePair>();
        orderData.add(new BasicNameValuePair("orderItems", orderJson));
        post.setEntity(new UrlEncodedFormEntity(orderData));
        streamlineService.setRequest(post);
        return streamlineService.execute();
    }

    /**
     * Gets the StreamlineService helper
     * @return StreamlineService the StreamlineService object
     */
    public StreamlineService getStreamlineService() {
        return streamlineService;
    }

    /**
     * Sets the StreamlineService object
     * @param streamlineService the StreamlineService helper
     */
    public void setStreamlineService(StreamlineService streamlineService) {
        this.streamlineService = streamlineService;
    }

    /**
     * Gets the HttpPostBuilder object
     * @return the HttpPostBuilder object
     */
    public HttpPostBuilder getHttpPostBuilder() {
        return httpPostBuilder;
    }

    /**
     * Sets the HttpPostBuilder object
     * @param httpPostBuilder the HttpPostBuilder object
     */
    public void setHttpPostBuilder(HttpPostBuilder httpPostBuilder) {
        this.httpPostBuilder = httpPostBuilder;
    }

    /**
     * Get the GSON converter object
     * @return Gson the GSON converter object
     */
    public Gson getGson() {
        return gson;
    }

    /**
     * Sets the GSON converter object
     * @param gson the GSON converter object
     */
    public void setGson(Gson gson) {
        this.gson = gson;
    }
}
