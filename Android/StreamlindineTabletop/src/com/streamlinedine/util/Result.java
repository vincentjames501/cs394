package com.streamlinedine.util;

/**
 * Simple container for dialog responses
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/3/12
 */
public class Result {
    /*
    This represents a confirmation chosen from the ConfirmActivity
     */
    public static int CONFIRM = 1;
    /*
    This represents a negative action chosen from the ConfirmActivity
     */
    public static int CANCEL = -1;
}
