package com.streamlinedine.util.image;

import android.graphics.Bitmap;
import android.graphics.Matrix;

/**
 * Utility for image metrics. Only used for image scaling now.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/24/12
 */
public class ImageMetrics {

    /**
     * Helper function to get a scaled bitmap image
     * @param bitmap the bitmap to scale
     * @param width the width to scale the image to
     * @param height the height to scale the image to
     * @return Bitmap the scaled bitmap
     */
    public static Bitmap getScaledBitmap(Bitmap bitmap, float width, float height) {
        float scaleWidth = (width / bitmap.getWidth());
        float scaleHeight = (height / bitmap.getHeight());

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
    }
}
