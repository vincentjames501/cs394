package com.streamlinedine.util.image;

import android.graphics.Bitmap;
import com.streamlinedine.util.server_communication.Domain;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.graphics.BitmapFactory.decodeStream;

/**
 * Utility for downloading bitmap images
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/24/12
 */
public class ImageDownloader {

    /**
     * This function downloads a bitmap at the provided location
     * @param imageLocation the location of the image to download
     * @return Bitmap the downloaded bitmap
     * @throws IOException can potentially fail to download the image
     */
    public static Bitmap downloadBitmap(String imageLocation) throws IOException {
        URL mainFileURL = new URL(Domain.domain + imageLocation);
        HttpURLConnection connection = (HttpURLConnection) mainFileURL.openConnection();
        connection.setDoInput(true);
        connection.connect();
        InputStream inputStream = connection.getInputStream();
        return decodeStream(inputStream);
    }

    /**
     * Helper function to download a set of bitmaps (currently unused)
     * @param imageLocations a list of image locations
     * @return List<Bitmap> a list of bitmaps
     * @throws IOException can potentially fail to download the images
     */
    public static List<Bitmap> downloadBitmaps(List<String> imageLocations) throws IOException {
        List<Bitmap> images = new ArrayList<Bitmap>();
        for (String imageLocation : imageLocations) {
            images.add(downloadBitmap(imageLocation));
        }
        return images;
    }
}
