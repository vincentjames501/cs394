package com.streamlinedine.util;

/**
 * Unused class for storing user credentials when logged into the device
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/10/12
 */
public class User {
    /**
     * Stores the username of the user logged into the application
     */
    public static String username;
    /**
     * Stores the password of the user logged into the application
     */
    public static String password;
}
