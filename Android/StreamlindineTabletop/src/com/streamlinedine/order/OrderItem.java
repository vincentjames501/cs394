package com.streamlinedine.order;

import com.streamlinedine.menu.Menu;
import com.streamlinedine.menu.item.MenuItem;

/**
 * Represents a user's order item.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/16/12
 */
public class OrderItem {
    private String comments = "";
    private int quantity;
    private Integer id;

    /**
     * Constructor the order item class. Constructs an object based on based values
     *
     * @param menuItem The associated menuItem from the menu for the item that is to be placed on the order
     * @param comment User comment for restrictions on the item
     * @param quantity The amount of a specific item the customer wishes to order
     */
    public OrderItem(MenuItem menuItem, String comment, int quantity) {
        this.id = menuItem.getItemId();
        this.comments = comment;
        this.quantity = quantity;
    }

    /**
     * Accessor for the associated MenuItem that an order item it is linked to
     *
     * @return The MenuItem reference for the order item
     */
    public MenuItem getAssociatedMenuItem() {
        return Menu.instance.getMenuItemMap().get(id);
    }

    /**
     * Accessor for the comment associated with the item
     *
     * @return Comment for the order item
     */
    public String getComment() {
        return comments;
    }

    /**
     * Mutator for the comment associated with item. Sets this.comment to comment
     *
     * @param comment A string for the comment of an order item
     */
    public void setComment(String comment) {
        this.comments = comment;
    }

    /**
     * Accessor for the quantity associated with the item
     *
     * @return Quantity for the order item
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Mutator for the quantity associated with item. Sets this.quantity to quantity
     *
     * @param quantity An integer for the number of repetitions to make this order item
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Accessor for the MenuItem identifier linked with an order item
     *
     * @return MenuItem identifier
     */
    public int getId() {
        return id;
    }

    /**
     * Mutator for the id associated with item. Sets this.id to id
     *
     * @param id An integer for the unique id to a MenuItem
     */
    public void setId(int id) {
        this.id = id;
    }
}
