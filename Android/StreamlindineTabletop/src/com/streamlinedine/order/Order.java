package com.streamlinedine.order;


import java.util.ArrayList;
import java.util.List;

/**
 * Represents the user's order. Has a static instance so as to be
 * accessible throughout the application.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 2/16/12
 */
public class Order {
    public static Order order = new Order();

    private List<OrderItem> orderItems;

    private OrderItem editingItem;

    /**
     * Construtor for Order, initializes list of OrderItems
     */
    public Order() {
        orderItems = new ArrayList<OrderItem>();
    }

    /**
     * Clears the OrderItems in an order
     */
    public void clearOrder() {
        orderItems.clear();
    }

    /**
     * Adds an OrderItem to an Order
     *
     * @param orderItem The OrderItem to add to an Order
     */
    public void addOrderItem(OrderItem orderItem) {
        orderItems.add(orderItem);
    }

    /**
     * Removes an OrderItem from an Order
     *
     * @param orderItem The OrderItem to remove from the ORder
     */
    public void removeOrderItem(OrderItem orderItem) {
        orderItems.remove(orderItem);
    }

    /**
     * Accessor for the OrderItems in an Order
     *
     * @return List of OrderItems
     */
    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    /**
     * Mutator for the OrderItems in an Order, removes all items in an Order
     * to reflect the new OrderItems list, orderItems
     *
     * @param orderItems A list of OrderItems
     */
    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    /**
     * Accessor for the editing item used by the Order Android screen for editing an item in
     * an order
     *
     * @return editing item for the Order page
     */
    public OrderItem getEditingItem() {
        return editingItem;
    }

    /**
     * Mutator for the editing item used by the Order Android screen for editing an item in
     * an order
     *
     * @param editingItem The item that is to be edited on the Order Android screen
     */
    public void setEditingItem(OrderItem editingItem) {
        this.editingItem = editingItem;
    }
}
