package com.streamlinedine.custom_component;


import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import com.streamlinedine.R;

/**
 * This class is used as an additional image tile in the Additional Images tab of the Item Details view.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 3/21/12
 */
public class ExtraImageTile extends LinearLayout {
    private LoadingImage loadingImage;

    /**
     * Default constructor
     * @param context parent context
     */
    public ExtraImageTile(Context context) {
        super(context);
        inflateLayout(context);
        loadingImage = (LoadingImage)findViewById(R.id.extra_image_loading_image_tile);
    }

    /**
     * Inflates the layout with corresponding xml file
     * @param context context to inflate with
     */
    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.extra_image_tile, this);
    }

    /**
     * Get the loading image component of the extra image tile
     * @return
     */
    public LoadingImage getLoadingImage() {
        return loadingImage;
    }

}
