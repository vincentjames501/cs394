package com.streamlinedine.custom_component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.streamlinedine.R;
import com.streamlinedine.tasks.DownloadImageTask;

/**
 * Special image component. While the image is downloading a progress (spinning) dialog is displayed.
 * Once the image is downloaded, the progress dialog is hidden and replaced with the image.
 *
 * @author Will Reynolds
 * @author James Bridges
 * @author Aaron Easter
 * @version 1.0 4/14/12
 */
public class LoadingImage extends LinearLayout{
    private ImageView imageView;
    private ProgressBar progressBar;

    /**
     * Default constructor
     * @param context the context with which to inflate
     */
    public LoadingImage(Context context) {
        super(context);
        inflateLayout(context);
        retrieveComponents();
    }

    /**
     * Additional constructor with attributes
     * @param context the context with which to inflate
     * @param attrs the attributes of the component
     */
    public LoadingImage(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflateLayout(context);
        retrieveComponents();
    }

    /**
     * Additional constructor with attributes and defStyle
     * @param context the context with which to inflate
     * @param attrs the attributes of the component
     * @param defStyle the defStyle
     */
    public LoadingImage(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflateLayout(context);
        retrieveComponents();
    }

    /**
     * This function retrieves and stores the components that make up this composite
     * component.
     */
    private void retrieveComponents() {
        imageView = (ImageView)findViewById(R.id.spinner_image_image);
        progressBar = (ProgressBar)findViewById(R.id.spinner_image_spinner);
    }

    /**
     * Inflates the layout with the corresponding xml file
     * @param context the context with which to inflate
     */
    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.spinner_image, this);
    }

    /**
     * Starts the image downloading
     * @param additionalImage the image location
     */
    public void startImageDownload(String additionalImage) {
        new DownloadImageTask(this, additionalImage).execute();
    }

    /**
     * Gets the image view of this component
     * @return ImageView the image view
     */
    public ImageView getImageView() {
        return imageView;
    }

    /**
     * Gets the progress bar of this component
     * @return ProgressBar the progress bar
     */
    public ProgressBar getProgressBar() {
        return progressBar;
    }
}
