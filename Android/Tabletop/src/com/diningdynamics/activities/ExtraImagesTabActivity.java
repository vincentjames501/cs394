package com.diningdynamics.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.diningdynamics.R;
import com.diningdynamics.custom_component.ExtraImageTile;
import com.diningdynamics.menu.item.MenuItem;
import com.diningdynamics.util.ImageDownloader;
import com.diningdynamics.util.ImageMetrics;

import java.io.IOException;
import java.util.List;

/**
 * User: Will Reynolds
 * Date: 3/11/12
 * Time: 7:06 PM
 */
public class ExtraImagesTabActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.extra_images);

        MenuItem menuItem = (MenuItem) getIntent().getExtras().get("MENU_ITEM");

        TableLayout layout = (TableLayout) findViewById(R.id.extra_images_container);
        TableRow imageRow = new TableRow(this);
        TableRow.LayoutParams menuParams = new TableRow.LayoutParams();
        menuParams.span = 1;
        menuParams.setMargins(10, 0, 10, 0);
        try {
            List<Bitmap> bitmaps = ImageDownloader.downloadBitmaps(menuItem.getAdditionalImages());
            if (bitmaps.size() > 0) {
                for (Bitmap bitmap : bitmaps) {
                    ExtraImageTile extraImageTile = new ExtraImageTile(ImageMetrics.getScaledBitmap(bitmap, 300, 300), this);
                    imageRow.addView(extraImageTile, menuParams);
                }
                layout.addView(imageRow);
            } else {
                addNoImageMessage();
            }
        } catch (IOException e) {
            addNoImageMessage();
        }
    }

    public void addNoImageMessage() {
        TextView textView = (TextView)findViewById(R.id.extra_images_no_extra_images);
        textView.setVisibility(View.VISIBLE);
    }
}
