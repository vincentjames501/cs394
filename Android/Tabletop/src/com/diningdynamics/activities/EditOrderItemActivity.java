package com.diningdynamics.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.diningdynamics.R;
import com.diningdynamics.order.OrderItem;
import com.diningdynamics.util.NumberPicker;

/**
 * User: Will Reynolds
 * Date: 3/21/12
 * Time: 6:57 PM
 */
public class EditOrderItemActivity extends AddItemToOrderActivity {
    private OrderItem orderItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_item_to_order);
        orderItem = (OrderItem) getIntent().getExtras().get("ORDER_ITEM");
    }

    @Override
    public void addItemButtonClick(View button) {
        int numberForItem = Integer.valueOf(((NumberPicker) findViewById(R.id.add_item_number_picker)).getmText().getText().toString());
        String comment = ((EditText) findViewById(R.id.add_item_comment)).getText().toString();

    }
}
