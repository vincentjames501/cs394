package com.diningdynamics.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ViewFlipper;
import com.diningdynamics.R;
import com.diningdynamics.menu.Menu;
import com.diningdynamics.tasks.RetrieveMenuTask;
import com.diningdynamics.util.Result;

/**
 * User: Will Reynolds
 * Date: 2/15/12
 * Time: 2:21 PM
 */
public class MenuActivity extends Activity {
    private ViewFlipper flipper;
    private Intent homeMenuIntent;
    private Intent viewOrderIntent;
    private boolean isPaused;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Result.CONFIRM) {
            //TODO submit order from here
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (!isPaused) {
            return super.dispatchTouchEvent(ev);    //To change body of overridden methods use File | Settings | File Templates.
        }
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isPaused = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_layout);
        flipper = (ViewFlipper) findViewById(R.id.view_flipper);
        buildMenuPages();
        GlobalActivityContainer.menuActivity = this;
        homeMenuIntent = new Intent(getBaseContext(), HomeMenuActivity.class);
        homeMenuIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        viewOrderIntent = new Intent(getBaseContext(), ViewOrderActivity.class);

        isPaused = false;
    }

    private void buildMenuPages() {
        new RetrieveMenuTask(this).execute();
    }

    public void nextAction(View view) {
        Menu.instance.flipToNextPage();
        prepareViewFlipper(R.anim.slide_in_right, R.anim.slide_out_left);
        flipper.setDisplayedChild(Menu.instance.getCurrentPage());
    }

    public void previousAction(View view) {
        Menu.instance.flipToPreviousPage();
        prepareViewFlipper(R.anim.slide_in_left, R.anim.slide_out_right);
        flipper.setDisplayedChild(Menu.instance.getCurrentPage());
    }

    public void homeButtonClick(View button) {
        startActivity(homeMenuIntent);
    }

    public void orderButtonClick(View button) {
        startActivity(viewOrderIntent);
    }

    public void submitOrderClick(View button) {
        Intent confirmIntent = new Intent(this, ConfirmActivity.class);
        confirmIntent.putExtra("POSITIVE", "Ready");
        confirmIntent.putExtra("NEGATIVE", "Cancel");
        confirmIntent.putExtra("MESSAGE", "Are you sure you're ready to submit your order?");
        startActivityForResult(confirmIntent, 1);
    }

    private void prepareViewFlipper(int animationIn, int animationOut) {
        flipper.setInAnimation(this, animationIn);
        flipper.setOutAnimation(this, animationOut);
    }

    @Override
    public void finish() {
        super.finish();
    }
}
