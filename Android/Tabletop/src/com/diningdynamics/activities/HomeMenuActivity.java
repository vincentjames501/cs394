package com.diningdynamics.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.diningdynamics.R;

/**
 * User: Will Reynolds
 * Date: 3/6/12
 * Time: 10:52 AM
 */
public class HomeMenuActivity extends Activity  {
    private Intent menuIntent;
    private Intent orderIntent;
    private Intent settingsIntent;

    private Intent activeIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_main_layout);
        menuIntent = new Intent(getBaseContext(), MenuActivity.class);
        menuIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        orderIntent = new Intent(getBaseContext(), ViewOrderActivity.class);
        settingsIntent = new Intent(getBaseContext(), SettingsActivity.class);
    }

    public void menuTileClick(View button) {
        startActivity(menuIntent);
    }

    public void orderTileClick(View button) {
        startActivity(orderIntent);
    }

    public void menuSettingsClick(View button) {
        startActivity(settingsIntent);
    }
}
