package com.diningdynamics.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import com.diningdynamics.R;
import com.diningdynamics.dialogs.ErrorAlert;
import com.diningdynamics.util.Result;
import com.diningdynamics.util.server_communication.Domain;
import com.diningdynamics.util.server_communication.ServiceCaller;
import com.diningdynamics.menu.item.OrderItemRow;
import com.diningdynamics.order.Order;
import com.diningdynamics.order.OrderItem;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Will Reynolds
 * Date: 2/14/12
 * Time: 1:07 PM
 */
public class ViewOrderActivity extends Activity {

    @Override
    protected void onResume() {
        super.onResume();

        LinearLayout layout = (LinearLayout) findViewById(R.id.view_order_layout);
        layout.removeAllViews();
        List<OrderItem> orderItems = Order.order.getOrderItems();
        int counter = 0;
        for (OrderItem orderItem : orderItems) {
            layout.addView(new OrderItemRow(this, orderItem, counter));
            counter++;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_order_layout);

        LinearLayout layout = (LinearLayout) findViewById(R.id.view_order_layout);
        List<OrderItem> orderItems = Order.order.getOrderItems();
        int counter = 0;
        for (OrderItem orderItem : orderItems) {
            layout.addView(new OrderItemRow(this, orderItem, counter));
        }
    }

    public void homeButtonClick(View button) {
        Intent homeMenuIntent = new Intent(getBaseContext(), HomeMenuActivity.class);
        homeMenuIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(homeMenuIntent);
    }

    public void menuButtonClick(View view) {
        Intent menuIntent = new Intent(getBaseContext(), MenuActivity.class);
        menuIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(menuIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Result.CONFIRM) {
            Gson gson = new Gson();
            String orderJson = gson.toJson(Order.order.getOrderItems());
            HttpParams params = new BasicHttpParams();
            HttpClient client = new DefaultHttpClient(params);
            HttpPost post = new HttpPost(Domain.domain + "/Menu/createNewOrder");
            post.addHeader("Cookie", ServiceCaller.instance.getCookie());
            List<NameValuePair> orderData = new ArrayList<NameValuePair>();
            orderData.add(new BasicNameValuePair("orderItems", orderJson));
            try {
                post.setEntity(new UrlEncodedFormEntity(orderData));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            try {
                HttpResponse response = client.execute(post);
                LinearLayout layout = (LinearLayout) findViewById(R.id.view_order_layout);
                layout.removeAllViews();
                Order.order.clearOrder();
                JSONObject jsonObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                ErrorAlert alert = new ErrorAlert(jsonObject.getString("Message"), this);
                alert.show();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if(resultCode == Result.CANCEL) {

        }
    }

    public void submitOrderClick(View button) {
        Intent confirmIntent = new Intent(this, ConfirmActivity.class);
        confirmIntent.putExtra("POSITIVE", "Ready");
        confirmIntent.putExtra("NEGATIVE", "Cancel");
        confirmIntent.putExtra("MESSAGE","Are you sure you're ready to submit your order?");
        startActivityForResult(confirmIntent, 1);
    }
}
