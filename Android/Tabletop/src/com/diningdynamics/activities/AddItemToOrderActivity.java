package com.diningdynamics.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import com.diningdynamics.R;
import com.diningdynamics.menu.item.MenuItem;
import com.diningdynamics.order.Order;
import com.diningdynamics.order.OrderItem;
import com.diningdynamics.util.NumberPicker;

/**
 * User: Will Reynolds
 * Date: 2/16/12
 * Time: 3:56 PM
 */
public class AddItemToOrderActivity extends Activity {
    private MenuItem menuItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_item_to_order);
        menuItem = (MenuItem)getIntent().getExtras().get("MENU_ITEM");
    }

    public void addItemButtonClick(View button) {
        int numberForItem = Integer.valueOf(((NumberPicker) findViewById(R.id.add_item_number_picker)).getmText().getText().toString());
        String comment = ((EditText)findViewById(R.id.add_item_comment)).getText().toString();
        Order.order.addOrderItem(new OrderItem(menuItem, comment, numberForItem));
        finish();
    }

    public void cancelButtonClick(View button) {
        finish();
    }
}
