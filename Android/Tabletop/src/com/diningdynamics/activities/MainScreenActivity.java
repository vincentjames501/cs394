package com.diningdynamics.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.diningdynamics.R;

public class MainScreenActivity extends Activity {

    @Override
    protected void onStart() {
        super.onStart();
        Intent loginIntent = new Intent(getBaseContext(),LoginDialogActivity.class);
        startActivity(loginIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
