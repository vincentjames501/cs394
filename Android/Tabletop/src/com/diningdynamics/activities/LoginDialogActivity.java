package com.diningdynamics.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import com.diningdynamics.R;
import com.diningdynamics.util.server_communication.Domain;
import com.diningdynamics.util.server_communication.HttpLogin;
import com.diningdynamics.dialogs.ErrorAlert;
import com.diningdynamics.tasks.LoginTask;


/**
 * User: Will Reynolds
 * Date: 1/31/12
 * Time: 1:16 PM
 */
public class LoginDialogActivity extends Activity {

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login_dialog);
    }

    public static void errorHandler(String message, Context handleContext) {
        ErrorAlert alertBox = new ErrorAlert(message, handleContext);
    }

    public void loginClick(View view) {
        try {
            String username = ((EditText) findViewById(R.id.login_username)).getText().toString();
            String password = ((EditText) findViewById(R.id.login_password)).getText().toString();
            if (username.equals("") || password.equals("")) {
                throw new Exception("Error, please provide a username and password");
            }
            attemptLogin(username, password);
        } catch (Exception e) {
            ErrorAlert alert = new ErrorAlert(e.getMessage(), this);
            alert.show();
        }
    }

    private void attemptLogin(String username, String password) {
        HttpLogin login = new HttpLogin();
        login.setLocation(Domain.domain + "/Menu/doLogin");
        login.setUsername(username);
        login.setPassword(password);
        LoginTask loginTask = new LoginTask(LoginDialogActivity.this, login);
        loginTask.execute();
    }

    public void cancelClick(View view) {
        AlertDialog alert = new AlertDialog.Builder(LoginDialogActivity.this).create();
        alert.setTitle("Are you sure?");
        alert.setMessage("Clicking \"Ok\" will close the applicatoin");

        alert.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                setResult(0);
                finish();
            }
        });

        alert.setButton2("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });

        alert.setIcon(R.drawable.info_icon);
        alert.show();
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
