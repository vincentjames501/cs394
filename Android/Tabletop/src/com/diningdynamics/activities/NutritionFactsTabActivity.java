package com.diningdynamics.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.diningdynamics.R;
import com.diningdynamics.menu.item.MenuItem;

/**
 * User: Will Reynolds
 * Date: 3/11/12
 * Time: 7:05 PM
 */
public class NutritionFactsTabActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nutrition_facts);

        MenuItem menuItem = (MenuItem)getIntent().getExtras().get("MENU_ITEM");

        TextView calories = (TextView)findViewById(R.id.nutrition_facts_calories);
        TextView caloriesFromFat = (TextView)findViewById(R.id.nutrition_facts_calories_from_fat);
        TextView totalFat = (TextView)findViewById(R.id.nutrition_facts_total_fat);
        TextView saturatedFat = (TextView)findViewById(R.id.nutrition_facts_saturated_fat);
        TextView transFat = (TextView)findViewById(R.id.nutrition_facts_trans_fat);
        TextView cholesterol = (TextView)findViewById(R.id.nutrition_facts_cholesterol);
        TextView sodium = (TextView)findViewById(R.id.nutrition_facts_sodium);
        TextView totalCarbohydrates = (TextView)findViewById(R.id.nutrition_facts_total_carbohydrates);
        TextView dietaryFiber = (TextView)findViewById(R.id.nutrition_facts_dietary_fiber);
        TextView sugars = (TextView)findViewById(R.id.nutrition_facts_sugars);
        TextView protein = (TextView)findViewById(R.id.nutrition_facts_protein);
        TextView vitaminA = (TextView)findViewById(R.id.nutrition_facts_vitamin_a);
        TextView vitaminC = (TextView)findViewById(R.id.nutrition_facts_vitamin_c);
        TextView calcium = (TextView)findViewById(R.id.nutrition_facts_calcium);
        TextView iron = (TextView)findViewById(R.id.nutrition_facts_iron);

        setNutritionContent(calories, "n_calories", menuItem);
        setNutritionContent(caloriesFromFat, "n_calories_from_fat", menuItem);
        setNutritionContent(totalFat, "n_total_fat", menuItem);
        setNutritionContent(saturatedFat, "n_saturated_fat", menuItem);
        setNutritionContent(transFat, "n_trans_fat", menuItem);
        setNutritionContent(cholesterol, "n_cholesterol", menuItem);
        setNutritionContent(sodium, "n_sodium", menuItem);
        setNutritionContent(totalCarbohydrates, "n_total_carbohydrate", menuItem);
        setNutritionContent(dietaryFiber, "n_dietary_fiber", menuItem);
        setNutritionContent(sugars, "n_sugars", menuItem);
        setNutritionContent(protein, "n_protein", menuItem);
        setNutritionContent(vitaminA, "n_vitamin_a_percent", menuItem);
        setNutritionContent(vitaminC, "n_vitamin_c_percent", menuItem);
        setNutritionContent(calcium, "n_calcium_percent", menuItem);
        setNutritionContent(iron, "n_iron_percent", menuItem);
    }

    private void setNutritionContent(TextView nutritionTextView, String nutritionIdentifier, MenuItem menuItem) {
        if(menuItem.getNutritionFactMap().containsKey(nutritionIdentifier)) {
            nutritionTextView.setText(menuItem.getNutritionFactMap().get(nutritionIdentifier));
        }
    }
}
