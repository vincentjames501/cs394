package com.diningdynamics.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.diningdynamics.R;
import com.diningdynamics.menu.item.MenuItem;
import com.diningdynamics.util.ImageDownloader;
import com.diningdynamics.util.ImageMetrics;

import java.io.IOException;
import java.text.NumberFormat;

/**
 * User: Will Reynolds
 * Date: 3/11/12
 * Time: 4:37 PM
 */
public class GeneralItemInfoTabActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MenuItem menuItem = (MenuItem) getIntent().getExtras().get("MENU_ITEM");
        setContentView(R.layout.general_item_info);

        ImageView mainImage = (ImageView) findViewById(R.id.general_item_info_main_image);
        TextView menuItemName = (TextView) findViewById(R.id.general_item_info_name);
        TextView menuItemLongDescription = (TextView) findViewById(R.id.general_item_info_long_description);
        TextView menuItemPrice = (TextView) findViewById(R.id.general_item_info_price);

        try {
            Bitmap bitmap = ImageMetrics.getScaledBitmap(ImageDownloader.downloadBitmap(menuItem.getMainImage()), 200, 200);
            mainImage.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        menuItemName.setText(menuItem.getName());
        if (!menuItem.getLongDescription().equals("")) {
            menuItemLongDescription.setText(menuItem.getLongDescription() + "\n");
        }
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        menuItemPrice.setText(formatter.format(menuItem.getPrice()));
    }
}
