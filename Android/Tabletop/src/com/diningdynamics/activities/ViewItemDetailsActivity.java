package com.diningdynamics.activities;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.*;
import android.widget.TabHost;
import com.diningdynamics.R;
import com.diningdynamics.menu.item.MenuItem;

/**
 * User: Will Reynolds
 * Date: 2/24/12
 * Time: 3:09 PM
 */
public class ViewItemDetailsActivity extends TabActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        setContentView(R.layout.view_item_details);

        MenuItem menuItem = (MenuItem) getIntent().getExtras().get("MENU_ITEM");

        Resources resources = getResources();
        TabHost tabHost = getTabHost();
        TabHost.TabSpec tabSpec;
        Intent intent;

        intent = new Intent().setClass(this, GeneralItemInfoTabActivity.class);
        intent.putExtra("MENU_ITEM", menuItem);
        tabSpec = tabHost.newTabSpec("general_info").setIndicator("General Info", resources.getDrawable(R.drawable.info_icon_tab)).setContent(intent);
        tabHost.addTab(tabSpec);

        intent = new Intent().setClass(this, NutritionFactsTabActivity.class);
        intent.putExtra("MENU_ITEM", menuItem);
        tabSpec = tabHost.newTabSpec("nutrition_facts").setIndicator("Nutrition Facts", resources.getDrawable(R.drawable.nutrition)).setContent(intent);
        tabHost.addTab(tabSpec);

        intent = new Intent().setClass(this, ExtraImagesTabActivity.class);
        intent.putExtra("MENU_ITEM", menuItem);
        tabSpec = tabHost.newTabSpec("extra_images").setIndicator("Images", resources.getDrawable(R.drawable.picture)).setContent(intent);
        tabHost.addTab(tabSpec);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (MotionEvent.ACTION_OUTSIDE == ev.getAction()) {
            finish();
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
//        System.out.println();
//        if (MotionEvent.ACTION_OUTSIDE == event.getAction()) {
//            finish();
//            return true;
//        }
//        return false;
    }
}
