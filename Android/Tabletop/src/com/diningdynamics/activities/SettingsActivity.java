package com.diningdynamics.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.diningdynamics.R;

/**
 * Created by IntelliJ IDEA.
 * User: Will Reynolds
 * Date: 3/6/12
 * Time: 1:13 PM
 */
public class SettingsActivity extends Activity {
        private Intent mainScreenIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);

        mainScreenIntent = new Intent(getBaseContext(), MainScreenActivity.class);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void logoutClick(View button) {
        startActivity(mainScreenIntent);

    }

    public void cancelClick(View button) {
        super.finish();
    }
}
