package com.diningdynamics.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.diningdynamics.R;

/**
 * Created by IntelliJ IDEA.
 * User: Will Reynolds
 * Date: 4/3/12
 * Time: 2:22 PM
 */
public class ConfirmActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.confirm_box);
        String positive = getIntent().getExtras().getString("POSITIVE");
        String negative = getIntent().getExtras().getString("NEGATIVE");
        String message = getIntent().getExtras().getString("MESSAGE");

        Button positiveButton = (Button) findViewById(R.id.confirm_dialog_positive);
        Button negativeButton = (Button) findViewById(R.id.confirm_dialog_negative);
        TextView messageText = (TextView) findViewById(R.id.confirm_dialog_text);

        if (!positive.equals("")) {
            positiveButton.setText(positive);
        }
        if (!negative.equals("")) {
            negativeButton.setText(negative);
        }
        if (!message.equals("")) {
            messageText.setText(message);
        }
    }

    public void posAction(View view) {
        setResult(1);
        finish();
    }

    public void negAction(View view) {
        setResult(-1);
        finish();
    }
}
