package com.diningdynamics.custom_component;


import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.diningdynamics.R;

/**
 * User: Will Reynolds
 * Date: 3/21/12
 * Time: 11:58 AM
 */
public class ExtraImageTile extends LinearLayout {
    public ExtraImageTile(Bitmap bitmap, Context context) {
        super(context);
        inflateLayout(context);
        ImageView imageView = (ImageView)findViewById(R.id.extra_image_tile_image);
        imageView.setImageBitmap(bitmap);
    }

    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.extra_image_tile, this);
    }
}
