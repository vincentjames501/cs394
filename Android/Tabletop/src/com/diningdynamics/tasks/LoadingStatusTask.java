package com.diningdynamics.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * User: Will Reynolds
 * Date: 2/26/12
 * Time: 4:22 PM
 */
public abstract class LoadingStatusTask extends AsyncTask {
    private ProgressDialog progressDialog;
    private boolean showDialog;

    public LoadingStatusTask(Context context, boolean showDialog, String loadingMessage) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(loadingMessage);
        progressDialog.setIndeterminate(true);
        this.showDialog = showDialog;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (showDialog) {
            progressDialog.show();
        }
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (showDialog) {
            progressDialog.dismiss();
        }
    }
}
