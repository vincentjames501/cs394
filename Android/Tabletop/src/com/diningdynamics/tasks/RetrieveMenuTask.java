package com.diningdynamics.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import com.diningdynamics.util.server_communication.ServiceCaller;
import com.diningdynamics.menu.Menu;
import com.diningdynamics.menu.page.MenuPage;
import com.diningdynamics.menu.category.Category;
import com.diningdynamics.menu.item.MenuItem;
import com.diningdynamics.menu.item.NutritionFact;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * User: Will Reynolds
 * Date: 2/9/12
 * Time: 12:06 PM
 */
public class RetrieveMenuTask extends AsyncTask {
    public static final String IGNORE_CATEGORY = "MONETARY_OPERATIONS";
    private ProgressDialog progressDialog;
    private Context context;

    public RetrieveMenuTask(Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Retrieving Menu");
        progressDialog.setIndeterminate(true);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        progressDialog.dismiss();
        new MenuRenderTask(context).execute();
    }

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            Thread.sleep(2000);
            Menu menu = new Menu();
            ServiceCaller.instance.setRequest("/Menu/getMenu");
            HttpResponse response = ServiceCaller.instance.makeServiceCall();

            String responseString = EntityUtils.toString(response.getEntity());
            JSONArray responseArray = new JSONArray(responseString);
            JSONArray menuPages = responseArray.getJSONObject(0).getJSONArray("menus").getJSONObject(0).getJSONArray("menuPages");
            menu.setName(responseArray.getJSONObject(0).getString("name"));
            for (int i = 0; i < menuPages.length(); i++) {
                MenuPage menuPage = new MenuPage();
                JSONObject menuPageObject = menuPages.getJSONObject(i);
                menuPage.setPageNumber(menuPageObject.getInt("pageNumber"));
                JSONArray categoryArray = menuPageObject.getJSONArray("categories");
                for (int j = 0; j < categoryArray.length(); j++) {
                    JSONObject categoryObject = categoryArray.getJSONObject(j);
                    if (!categoryObject.getString("name").equalsIgnoreCase(IGNORE_CATEGORY)) {
                        Category category = new Category();
                        category.setCategoryName(categoryObject.getString("name"));
                        JSONArray menuItemArray = categoryObject.getJSONArray("menuItems");
                        for (int k = 0; k < menuItemArray.length(); k++) {
                            JSONObject menuItemObject = menuItemArray.getJSONObject(k);
                            MenuItem menuItem = new MenuItem();
                            menuItem.setRating(menuItemObject.getString("rating"));
                            menuItem.setTimesOrdered(menuItemObject.getInt("timesOrdered"));
                            menuItem.setName(menuItemObject.getString("name"));
                            menuItem.setPrice(Float.valueOf(menuItemObject.getString("price")));
                            menuItem.setMainImage(menuItemObject.getString("image"));
                            menuItem.setShortDescription(menuItemObject.getString("shortDescription"));
                            menuItem.setLongDescription(menuItemObject.getString("longDescription"));
                            JSONArray additionalImages = menuItemObject.getJSONArray("additionalImages");
                            for (int x = 0; x < additionalImages.length(); x++) {
                                menuItem.addAdditionalImage(additionalImages.getJSONObject(x).get("image").toString());
                            }
                            JSONArray nutritionFacts = menuItemObject.getJSONArray("nutritionFacts");
                            for (int x = 0; x < nutritionFacts.length(); x++) {
                                JSONObject nutritionObject = nutritionFacts.getJSONObject(x);
                                menuItem.getNutritionFactMap().put(nutritionObject.getString("type"), nutritionObject.getString("fact"));
                                menuItem.addNutritionFact(new NutritionFact(nutritionObject.getString("type"), nutritionObject.getString("fact")));
                            }
                            menuItem.setItemId(menuItemObject.getInt("id"));
                            menu.getMenuItemMap().put(menuItem.getItemId(), menuItem);
                            category.addMenuItem(menuItem);
                        }
                        menuPage.addCategory(category);
                    }
                }
                menu.addMenuPage(menuPage);
            }
            Menu.instance = menu;
            Menu.instance.updateCurrentPage();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
