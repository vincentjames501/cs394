package com.diningdynamics.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import com.diningdynamics.util.server_communication.HttpLogin;
import com.diningdynamics.util.server_communication.ServiceCaller;
import com.diningdynamics.activities.HomeMenuActivity;
import com.diningdynamics.dialogs.ErrorAlert;
import org.apache.http.HttpResponse;

/**
 * User: Will Reynolds
 * Date: 2/2/12
 * Time: 1:44 PM
 */
public class LoginTask extends AsyncTask {

    private Context context;
    private HttpLogin login;
    private ProgressDialog dialog;

    public LoginTask(Context context, HttpLogin login) {
        this.context = context;
        this.login = login;
        this.dialog = null;
    }

    @Override
    protected void onPreExecute() {
        dialog = ProgressDialog.show(context, "Logging In", "", true);
        dialog.show();
    }

    @Override
    protected void onPostExecute(Object o) {
        dialog.dismiss();
        if (o instanceof Exception) {
            ErrorAlert badLogin = new ErrorAlert(((Exception) o).getMessage(), context);
            badLogin.show();
        } else {
            Intent launchMenuIntent = new Intent(context, HomeMenuActivity.class);
            context.startActivity(launchMenuIntent);
//            RetrieveMenuTask retrieveMenuTask = new RetrieveMenuTask(context);
//            retrieveMenuTask.execute();
        }
    }

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            Thread.sleep(2000);
            HttpResponse response = login.login();
            String responseString = (response.getHeaders("Set-Cookie"))[0].toString();
            ServiceCaller.instance.setCookie(responseString.substring(responseString.indexOf("sessionid="), responseString.indexOf(";")));
        } catch (Exception e) {
            return e;
        }
        return null;
    }
}
