package com.diningdynamics.tasks;

import android.app.Activity;
import android.content.Context;
import android.widget.TableRow;
import android.widget.ViewFlipper;
import com.diningdynamics.R;
import com.diningdynamics.menu.Menu;
import com.diningdynamics.menu.category.Category;
import com.diningdynamics.menu.category.CategoryRow;
import com.diningdynamics.menu.item.MenuItem;
import com.diningdynamics.menu.item.MenuItemPlaceholder;
import com.diningdynamics.menu.item.MenuItemRow;
import com.diningdynamics.menu.page.MenuPage;
import com.diningdynamics.menu.page.MenuPageView;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Will Reynolds
 * Date: 2/26/12
 * Time: 4:22 PM
 */
public class MenuRenderTask extends LoadingStatusTask {
    ViewFlipper flipper;
    private Context context;

    public MenuRenderTask(Context context) {
        super(context, true, "Rendering Menu");
        this.context = context;
        this.flipper = (ViewFlipper) ((Activity) context).findViewById(R.id.view_flipper);
    }

    @Override
    protected void onPostExecute(Object o) {
        List<MenuPageView> menuPageViews = (ArrayList<MenuPageView>) o;
        int counter = 0;
        for (MenuPageView menuPageView : menuPageViews) {
            flipper.addView(menuPageView, counter);
            counter++;
        }
        flipper.setDisplayedChild(Menu.instance.getCurrentPage());
        super.onPostExecute(o);
    }

    @Override
    protected Object doInBackground(Object... objects) {
        List<MenuPageView> menuPageViewList = new ArrayList<MenuPageView>();
        int numberColumns = 2;
        TableRow.LayoutParams categoryParams = new TableRow.LayoutParams();
        categoryParams.span = numberColumns;
        TableRow.LayoutParams menuParams = new TableRow.LayoutParams();
        menuParams.span = 1;
        menuParams.setMargins(10, 10, 10, 10);
        int counter = 0;
        for (MenuPage menuPage : Menu.instance.getMenuPages()) {
            MenuPageView menuPageView = new MenuPageView(context);
            for (Category category : menuPage.getCategories()) {
                List<MenuItem> menuItemList = category.getMenuItems();
                buildCategoryRow(categoryParams, menuPageView, category);
                buildInitialMenuItems(numberColumns, menuParams, menuPageView, menuItemList);
                buildLastRow(numberColumns, menuParams, menuPageView, menuItemList);
            }
            menuPageViewList.add(menuPageView);
        }
        return menuPageViewList;
    }

    private void buildLastRow(int numberColumns, TableRow.LayoutParams menuParams, MenuPageView menuPageView, List<MenuItem> menuItems) {
        TableRow lastRow = new TableRow(context);
        int menuItemCount = menuItems.size();
        int numberLeftOver = menuItemCount % numberColumns;

        if (numberLeftOver > 0) {
            int leftOverStart = menuItemCount - numberLeftOver;
            for (int i = 0; i < numberColumns; i++) {
                if (i < numberLeftOver) {
                    MenuItem menuItem = menuItems.get(leftOverStart + i);
                    lastRow.addView(new MenuItemRow(menuItem, context), menuParams);
                } else {
                    lastRow.addView(new MenuItemPlaceholder(context), menuParams);
                }
            }
            menuPageView.addComponent(lastRow);
        }
    }

    private void buildInitialMenuItems(int numberColumns, TableRow.LayoutParams menuParams, MenuPageView menuPageView, List<MenuItem> menuItems) {
        int menuItemCount = menuItems.size();
        int end = (menuItemCount / numberColumns) * numberColumns;
        TableRow menuItemsRow = new TableRow(context);
        for (int i = 0; i < end; i++) {
            MenuItem menuItem = menuItems.get(i);
            menuItemsRow.addView(new MenuItemRow(menuItem, context), menuParams);
            if (i % numberColumns == (numberColumns - 1)) {
                menuPageView.addComponent(menuItemsRow);
                menuItemsRow = new TableRow(context);
            }
        }
    }

    private void buildCategoryRow(TableRow.LayoutParams categoryParams, MenuPageView menuPageView, Category category) {
        TableRow categoryRow = new TableRow(context);
        categoryRow.addView(new CategoryRow(category, context), categoryParams);
        menuPageView.addComponent(categoryRow);
    }
}
