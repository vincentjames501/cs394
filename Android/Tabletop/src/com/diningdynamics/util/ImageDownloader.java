package com.diningdynamics.util;

import android.graphics.Bitmap;
import com.diningdynamics.util.server_communication.Domain;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.graphics.BitmapFactory.decodeStream;

/**
 * User: Will Reynolds
 * Date: 2/24/12
 * Time: 3:17 PM
 */
public class ImageDownloader {
    public static Bitmap downloadBitmap(String imageLocation) throws IOException {
        URL mainFileURL = new URL(Domain.domain + imageLocation);
        HttpURLConnection connection = (HttpURLConnection) mainFileURL.openConnection();
        connection.setDoInput(true);
        connection.connect();
        InputStream inputStream = connection.getInputStream();
        return decodeStream(inputStream);
    }

    public static List<Bitmap> downloadBitmaps(List<String> imageLocations) throws IOException {
        List<Bitmap> images = new ArrayList<Bitmap>();
        for (String imageLocation : imageLocations) {
            images.add(downloadBitmap(imageLocation));
        }
        return images;
    }
}
