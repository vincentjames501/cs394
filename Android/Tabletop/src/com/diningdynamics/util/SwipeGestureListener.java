package com.diningdynamics.util;

import android.app.Activity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;
import com.diningdynamics.R;
import com.diningdynamics.activities.GlobalActivityContainer;
import com.diningdynamics.menu.Menu;

/**
 * User: Will Reynolds
 * Date: 2/15/12
 * Time: 2:58 PM
 */
public class SwipeGestureListener extends GestureDetector.SimpleOnGestureListener implements Animation.AnimationListener {
    private static final float VELOCITY_THRESHOLD = 2000;
    private Activity parentActivity;

    private Animation animationIn;
    private View menuView;

    public SwipeGestureListener() {
        //parentActivity = activity;
//        menuView = activity.findViewById(R.id.menu_section);
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (Math.abs(velocityX) > VELOCITY_THRESHOLD) {
            Animation animationOut;
            ViewFlipper flipper = (ViewFlipper) GlobalActivityContainer.menuActivity.findViewById(R.id.view_flipper);
            if (velocityX > 0) {
                Menu.instance.flipToPreviousPage();
                flipper.setInAnimation(AnimationUtils.loadAnimation(GlobalActivityContainer.menuActivity, R.anim.slide_in_left));
                flipper.setOutAnimation(AnimationUtils.loadAnimation(GlobalActivityContainer.menuActivity, R.anim.slide_out_right));
            } else {
                Menu.instance.flipToNextPage();
                flipper.setInAnimation(AnimationUtils.loadAnimation(GlobalActivityContainer.menuActivity, R.anim.slide_in_right));
                flipper.setOutAnimation(AnimationUtils.loadAnimation(GlobalActivityContainer.menuActivity, R.anim.slide_out_left));
            }
            flipper.setDisplayedChild(Menu.instance.getCurrentPage());
        }
        return super.onFling(e1, e2, velocityX, velocityY);
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        menuView.startAnimation(animationIn);
    }

    public void onAnimationRepeat(Animation animation) {
    }
}
