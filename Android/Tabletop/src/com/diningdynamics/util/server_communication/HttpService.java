package com.diningdynamics.util.server_communication;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

/**
 * User: Will Reynolds
 * Date: 4/3/12
 * Time: 11:04 AM
 */
public abstract class HttpService {
    private HttpUriRequest httpRequest;
    public HttpResponse execute() throws Exception {
        HttpParams params = new BasicHttpParams();
        HttpClient httpClient = new DefaultHttpClient(params);
        return httpClient.execute(httpRequest);
    }

    public HttpUriRequest getHttpRequest() {
        return httpRequest;
    }

    public void setHttpRequest(HttpUriRequest httpRequest) {
        this.httpRequest = httpRequest;
    }
}
