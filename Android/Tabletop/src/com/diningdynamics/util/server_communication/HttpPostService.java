package com.diningdynamics.util.server_communication;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Will Reynolds
 * Date: 4/3/12
 * Time: 11:06 AM
 */
public class HttpPostService extends HttpService {
    List<BasicNameValuePair> nameValues = new ArrayList<BasicNameValuePair>();
    public void addData(String name, String value) {
        nameValues.add(new BasicNameValuePair(name, value));
    }

    public void setRequest(String request) {
        super.setHttpRequest(new HttpPost(Domain.domain + request));
    }

    public void prepare() throws UnsupportedEncodingException {
        ((HttpPost)super.getHttpRequest()).setEntity(new UrlEncodedFormEntity(nameValues));
    }
}
