package com.diningdynamics.util.server_communication;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Will Reynolds
 * Date: 1/31/12
 * Time: 3:37 PM
 */
public class HttpLogin {
    private String username;
    private String password;
    private String location;


    public HttpResponse login() throws Exception {
        HttpParams params = new BasicHttpParams();
        HttpClient client = new DefaultHttpClient(params);
        List<BasicNameValuePair> nameValues = new ArrayList<BasicNameValuePair>();
        nameValues.add(new BasicNameValuePair("username", username));
        nameValues.add(new BasicNameValuePair("password", password));
        HttpPost post = new HttpPost(location);
        post.setEntity(new UrlEncodedFormEntity(nameValues));
        HttpResponse response = client.execute(post);
        return response;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
