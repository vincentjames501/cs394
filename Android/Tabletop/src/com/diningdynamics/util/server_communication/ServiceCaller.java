package com.diningdynamics.util.server_communication;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Will Reynolds
 * Date: 1/31/12
 * Time: 3:29 PM
 */
public class ServiceCaller {
    public static ServiceCaller instance = new ServiceCaller();
    private String cookie;
    private String request;
    private HttpUriRequest httpRequest;

    public ServiceCaller() {
        cookie = "";
    }

    public void setMethod(String method) {
        if (method.equals("POST")) {
            httpRequest = new HttpPost(Domain.domain + request);
        } else if (method.equals("GET")) {
            httpRequest = new HttpGet(Domain.domain + request);
        }
    }

    public HttpResponse execute() throws IOException {
        httpRequest.addHeader("Cookie", cookie);
        HttpParams params = new BasicHttpParams();
        HttpClient client = new DefaultHttpClient(params);
        return client.execute(httpRequest);
    }

    public HttpResponse makeServiceCall() throws Exception {
        if (cookie.equals("")) {
            throw new Exception("You do not have a cookie");
        }
        HttpParams params = new BasicHttpParams();
        HttpClient client = new DefaultHttpClient(params);
        HttpGet get = new HttpGet(Domain.domain + request);
        get.addHeader("Cookie", cookie);
        return client.execute(get);
    }

    public HttpResponse post() throws Exception {
        if (cookie.equals("")) {
            throw new Exception("You do not have a cookie");
        }
        HttpParams params = new BasicHttpParams();
        HttpClient client = new DefaultHttpClient(params);
        List<BasicNameValuePair> nameValues = new ArrayList<BasicNameValuePair>();
        nameValues.add(new BasicNameValuePair("order", "stuff"));
        HttpPost post = new HttpPost(Domain.domain + request);
        post.addHeader("Cookie", cookie);
        return client.execute(post);
    }

    public static ServiceCaller getInstance() {
        return instance;
    }

    public static void setInstance(ServiceCaller instance) {
        ServiceCaller.instance = instance;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
