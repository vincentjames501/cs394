package com.diningdynamics.util.button_listeners;

import android.app.Activity;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.View;
import com.diningdynamics.activities.MenuActivity;
import com.diningdynamics.activities.ViewItemDetailsActivity;
import com.diningdynamics.menu.item.MenuItem;

/**
 * User: Will Reynolds
 * Date: 2/24/12
 * Time: 3:06 PM
 */
public class DescriptionButtonListener implements View.OnClickListener{
    private MenuItem menuItem;
    private boolean canHandle;

    public DescriptionButtonListener(MenuItem menuItem) {
        canHandle = true;
        this.menuItem = menuItem;
    }

    public void onClick(View view) {
        if (canHandle) {
            Intent detailsIntent = new Intent(view.getContext(), ViewItemDetailsActivity.class);
            detailsIntent.putExtra("MENU_ITEM", menuItem);
            view.getContext().startActivity(detailsIntent);
        }
    }
}
