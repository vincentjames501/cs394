package com.diningdynamics.util.button_listeners;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import com.diningdynamics.activities.AddItemToOrderActivity;
import com.diningdynamics.activities.LoginDialogActivity;
import com.diningdynamics.menu.item.MenuItem;

/**
 * User: Will Reynolds
 * Date: 2/23/12
 * Time: 11:27 AM
 */
public class AddItemButtonListener implements View.OnClickListener {
    private MenuItem menuItem;
    public AddItemButtonListener(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    public void onClick(View view) {
        Intent addItemIntent = new Intent(view.getContext(),AddItemToOrderActivity.class);
        addItemIntent.putExtra("MENU_ITEM", menuItem);
        view.getContext().startActivity(addItemIntent);
    }
}
