package com.diningdynamics.util.button_listeners;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import com.diningdynamics.R;
import com.diningdynamics.menu.item.OrderItemRow;
import com.diningdynamics.order.Order;
import com.diningdynamics.order.OrderItem;

/**
 * User: Will Reynolds
 * Date: 3/21/12
 * Time: 6:42 PM
 */
public class RemoveOrderItemButtonListener implements View.OnClickListener{
    OrderItem orderItem;
    View parentView;
    Context context;
    public RemoveOrderItemButtonListener(OrderItem orderItem, OrderItemRow orderItemRow, Context context) {
        this.orderItem = orderItem;
        parentView = orderItemRow;
        this.context = context;
    }

    public void onClick(View view) {
        LinearLayout linearLayout = (LinearLayout) ((Activity)context).findViewById(R.id.view_order_layout);
        linearLayout.removeView(parentView);
        Order.order.removeOrderItem(orderItem);
    }
}
