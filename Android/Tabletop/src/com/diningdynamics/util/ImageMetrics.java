package com.diningdynamics.util;

import android.graphics.Bitmap;
import android.graphics.Matrix;

/**
 * User: Will Reynolds
 * Date: 2/24/12
 * Time: 3:22 PM
 */
public class ImageMetrics {
    public static Bitmap getScaledBitmap(Bitmap bitmap, float width, float height) {
        float scaleWidth = (width / bitmap.getWidth());
        float scaleHeight = (height / bitmap.getHeight());

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
    }
}
