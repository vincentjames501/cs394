package com.diningdynamics.user;

import com.diningdynamics.order.OrderItem;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Will Reynolds
 * Date: 3/19/12
 * Time: 1:03 PM
 */
public class User {
    public static User instance = new User();

    private List<OrderItem> submittedItems;
    private List<OrderItem> orderItems;

    public User() {
        submittedItems = new ArrayList<OrderItem>();
        orderItems = new ArrayList<OrderItem>();
    }

    public void checkOutItemsInCart() {
        submittedItems.addAll(orderItems);
        orderItems.clear();
    }

    public void checkOut() {
        submittedItems.clear();
        orderItems.clear();
    }

    public List<OrderItem> getSubmittedItems() {
        return submittedItems;
    }

    public void setSubmittedItems(List<OrderItem> submittedItems) {
        this.submittedItems = submittedItems;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }
}
