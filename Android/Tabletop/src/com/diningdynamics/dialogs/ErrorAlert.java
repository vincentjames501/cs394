package com.diningdynamics.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * User: Will Reynolds
 * Date: 1/31/12
 * Time: 3:46 PM
 */
public class ErrorAlert extends AlertDialog {
    public ErrorAlert(String message, Context context) {
        super(context);
        setMessage(message);
        setButton("Ok", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
    }
}
