package com.diningdynamics.menu.category;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.diningdynamics.R;

/**
 * User: Will Reynolds
 * Date: 2/16/12
 * Time: 10:27 AM
 */
public class CategoryRow extends LinearLayout {
    private Category category;
    private TextView categoryName;
    private ImageView categoryImage;

    public CategoryRow(Category category, Context context) {
        super(context);
        this.category = category;

        inflateLayout(context);
        categoryName = (TextView)findViewById(R.id.category_name);
        categoryName.setText(category.getCategoryName());
    }

    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.category_row, this);
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public TextView getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(TextView categoryName) {
        this.categoryName = categoryName;
    }

    public ImageView getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(ImageView categoryImage) {
        this.categoryImage = categoryImage;
    }
}
