package com.diningdynamics.menu.category;

import com.diningdynamics.menu.item.MenuItem;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Will Reynolds
 * Date: 1/31/12
 * Time: 11:49 AM
 */
public class Category {
    private String categoryName;
    private List<MenuItem> menuItems;

    public Category(String name) {
        this.categoryName = name;
        menuItems = new ArrayList<MenuItem>();
    }

    public Category() {
        menuItems = new ArrayList<MenuItem>();
    }

    public void addMenuItem(MenuItem menuItem) {
        menuItems.add(menuItem);
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
}
