package com.diningdynamics.menu.item;


import android.content.Context;
import android.widget.ImageView;

/**
 * User: Will Reynolds
 * Date: 1/31/12
 * Time: 12:32 PM
 */
public class MenuItemImageView extends ImageView {
    public MenuItemImageView(Context context) {
        super(context);
    }
}
