package com.diningdynamics.menu.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.diningdynamics.R;
import com.diningdynamics.menu.Menu;
import com.diningdynamics.order.Order;
import com.diningdynamics.order.OrderItem;
import com.diningdynamics.util.button_listeners.EditOrderItemButtonListener;
import com.diningdynamics.util.button_listeners.RemoveOrderItemButtonListener;

/**
 * User: Will Reynolds
 * Date: 2/26/12
 * Time: 8:06 PM
 */
public class OrderItemRow extends LinearLayout{
    private TextView itemName;
    private TextView itemCount;
    private TextView itemComment;
    private OrderItem orderItem;
    private int counter;
    public OrderItemRow(Context context, OrderItem orderItem, int counter) {
        super(context);
        inflateLayout(context);

        itemName = (TextView)findViewById(R.id.order_item_name);
        itemCount = (TextView)findViewById(R.id.order_item_count);
        itemComment = (TextView)findViewById(R.id.order_item_comment);

        Button removeButton = (Button)findViewById(R.id.remove_order_item_button);
        Button editButton = (Button)findViewById(R.id.edit_order_button);

        removeButton.setOnClickListener(new RemoveOrderItemButtonListener(orderItem, this, context));
        editButton.setOnClickListener(new EditOrderItemButtonListener());

        MenuItem menuItem = Menu.instance.getMenuItemMap().get(orderItem.getId());
        itemName.setText(menuItem.getName());
        itemCount.setText(String.valueOf(orderItem.getQuantity()));
        itemComment.setText(orderItem.getComment());

        this.orderItem = orderItem;
    }

    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.order_item_row, this);
    }

    public void editItem(View button) {

    }

    public void removeItem(View button) {
        Order.order.getOrderItems().remove(orderItem);
        LinearLayout layout = (LinearLayout)findViewById(R.id.view_order_layout);
        layout.removeView(this);
    }

    public TextView getItemName() {
        return itemName;
    }

    public void setItemName(TextView itemName) {
        this.itemName = itemName;
    }

    public TextView getItemCount() {
        return itemCount;
    }

    public void setItemCount(TextView itemCount) {
        this.itemCount = itemCount;
    }

    public TextView getItemComment() {
        return itemComment;
    }

    public void setItemComment(TextView itemComment) {
        this.itemComment = itemComment;
    }
}
