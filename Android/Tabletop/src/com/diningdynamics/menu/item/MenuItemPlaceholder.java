package com.diningdynamics.menu.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import com.diningdynamics.R;

/**
 * User: Will Reynolds
 * Date: 2/26/12
 * Time: 2:47 PM
 */
public class MenuItemPlaceholder extends LinearLayout{
    public MenuItemPlaceholder(Context context) {
        super(context);
        inflateLayout(context);
    }

    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.menu_item_placeholder, this);
    }
}
