package com.diningdynamics.menu.page;

import android.content.Context;
import android.view.*;
import android.widget.ScrollView;
import android.widget.TableLayout;
import com.diningdynamics.R;

/**
 * User: Will Reynolds
 * Date: 2/19/12
 * Time: 10:06 AM
 */
public class MenuPageView extends ScrollView {
    private TableLayout layout;

    private GestureDetector detector;
    View.OnTouchListener listener;

    public MenuPageView(Context context) {
        super(context);
        inflateLayout(context);
        layout = (TableLayout)findViewById(R.id.menu_view_layout);
    }

    public void addComponent(View view) {
        layout.addView(view);
        layout.refreshDrawableState();
    }

    private void inflateLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.menu_view, this);
    }
//    private void buildSwipeGestureListener() {
//        detector = new GestureDetector(new SwipeGestureListener());
//        listener = new View.OnTouchListener() {
//            public boolean onTouch(View v, MotionEvent event) {
//                return detector.onTouchEvent(event);
//            }
//        };
//
//        ScrollView scroll = (ScrollView) findViewById(R.id.menu_scroll_view);
//        ViewFlipper layout = (ViewFlipper) findViewById(R.id.view_flipper);
//        layout.setOnClickListener(MenuPageView.this);
//        layout.setOnTouchListener(listener);
//    }

//    public void onClick(View view) {
//    }
}
