package com.diningdynamics.menu.page;

import com.diningdynamics.menu.category.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Will Reynolds
 * Date: 1/31/12
 * Time: 11:49 AM
 */
public class MenuPage {
    private int pageNumber;
    private String name;
    private List<Category> categories;

    public MenuPage(int pageNumber, String name) {
        this.pageNumber = pageNumber;
        this.name = name;
    }

    public MenuPage() {
        categories = new ArrayList<Category>();
    }

    public void addCategory(Category category) {
        categories.add(category);
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
