import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * User: Will Reynolds
 * Date: 4/16/12
 * Time: 12:30 PM
 */
public class HttpResponseDecoder {
    public String getStringFromResponse(HttpResponse response) throws IOException {
        return EntityUtils.toString(response.getEntity());
    }
}
