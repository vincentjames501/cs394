/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 10:56 AM
 */
public class ServerErrorException extends Exception{
    public ServerErrorException(String message) {
        super(message);
    }
}
