import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 11:41 AM
 */
public class LoginService implements ServiceInterface{
    private String username;
    private String password;
    private HttpPostBuilder postBuilder;
    private StreamlineService streamlineService;

    public LoginService() {
        postBuilder = new HttpPostBuilder();
        streamlineService = new StreamlineService();
        username = "";
        password = "";
    }

    @Override
    public HttpResponse execute() throws UnsupportedEncodingException, ServerErrorException {
        HttpPost post = postBuilder.buildHttpPost(StreamlineService.LOGIN);
        List<BasicNameValuePair> nameValues = new ArrayList<BasicNameValuePair>();
        nameValues.add(new BasicNameValuePair("username", username));
        nameValues.add(new BasicNameValuePair("password", password));
        post.setEntity(new UrlEncodedFormEntity(nameValues));
        streamlineService.setRequest(post);
        return streamlineService.execute();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public HttpPostBuilder getPostBuilder() {
        return postBuilder;
    }

    public void setPostBuilder(HttpPostBuilder postBuilder) {
        this.postBuilder = postBuilder;
    }

    public StreamlineService getStreamlineService() {
        return streamlineService;
    }

    public void setStreamlineService(StreamlineService streamlineService) {
        this.streamlineService = streamlineService;
    }
}
