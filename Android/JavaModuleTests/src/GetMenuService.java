import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 3:10 PM
 */
public class GetMenuService implements ServiceInterface {

    private StreamlineService streamlineService;

    public HttpResponse execute() throws ServerErrorException {
        HttpGet getMenuRequest = new HttpGet(StreamlineService.GET_MENU);
        getMenuRequest.addHeader("Cookie", StreamlineService.cookie);
        streamlineService.setRequest(getMenuRequest);
        return streamlineService.execute();
    }

    public StreamlineService getStreamlineService() {
        return streamlineService;
    }

    public void setStreamlineService(StreamlineService streamlineService) {
        this.streamlineService = streamlineService;
    }
}
