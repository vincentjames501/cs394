import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 10:27 AM
 */
public class SubmitOrderService implements ServiceInterface {
    private StreamlineService streamlineService;

    private HttpPostBuilder httpPostBuilder;

    private Gson gson;


    public SubmitOrderService() {
        streamlineService = new StreamlineService();
        httpPostBuilder = new HttpPostBuilder();
        gson = new Gson();
    }

    public HttpResponse execute() throws UnsupportedEncodingException, ServerErrorException {
        String orderJson = gson.toJson(Order.order.getOrderItems());

        HttpPost post = httpPostBuilder.buildHttpPost(StreamlineService.SUBMIT_ORDER);

        List<NameValuePair> orderData = new ArrayList<NameValuePair>();
        orderData.add(new BasicNameValuePair("orderItems", orderJson));
        post.setEntity(new UrlEncodedFormEntity(orderData));
        streamlineService.setRequest(post);
        return streamlineService.execute();
    }

    public StreamlineService getStreamlineService() {
        return streamlineService;
    }

    public void setStreamlineService(StreamlineService streamlineService) {
        this.streamlineService = streamlineService;
    }

    public HttpPostBuilder getHttpPostBuilder() {
        return httpPostBuilder;
    }

    public void setHttpPostBuilder(HttpPostBuilder httpPostBuilder) {
        this.httpPostBuilder = httpPostBuilder;
    }

    public Gson getGson() {
        return gson;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }
}
