

/**
 * User: Will Reynolds
 * Date: 2/16/12
 * Time: 4:01 PM
 */
public class OrderItem {
    private String comments = "";
    private int quantity;
    private Integer id;

    public OrderItem(MenuItem menuItem, String comment, int quantity) {
        this.id = menuItem.getItemId();
        this.comments = comment;
        this.quantity = quantity;
    }

    public MenuItem getAssociatedMenuItem() {
        return Menu.instance.getMenuItemMap().get(id);
    }

    public String getComment() {
        return comments;
    }

    public void setComment(String comment) {
        this.comments = comment;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
