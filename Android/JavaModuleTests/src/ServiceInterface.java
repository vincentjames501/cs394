import com.sun.deploy.net.HttpResponse;

import java.io.UnsupportedEncodingException;

/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 3:08 PM
 */
public interface ServiceInterface {
    public org.apache.http.HttpResponse execute() throws UnsupportedEncodingException, ServerErrorException;
}
