import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;

/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 3:12 PM
 */
public class StreamlineService {
    public static final String LOGIN = Domain.domain + "/Menu/doLogin";
    public static final String SUBMIT_ORDER = Domain.domain + "/Menu/createNewOrder";
    public static final String GET_MENU = Domain.domain + "/Menu/getMenu";
    public static final int SUCCESS = 200;
    public static String cookie;

    private HttpUriRequest request;
    private HttpClient client;

    public HttpResponse execute() throws ServerErrorException {
        HttpParams params = new BasicHttpParams();
        //HttpClient client = new DefaultHttpClient(params);
        HttpResponse response = null;
        try {
            response = client.execute(request);
        } catch (IOException e) {
            throw new ServerErrorException("Error");
        }
        if(response.getStatusLine().getStatusCode() != SUCCESS) {
            throw new ServerErrorException("Error");
        }
        return response;
    }

    public HttpUriRequest getRequest() {
        return request;
    }

    public void setRequest(HttpUriRequest request) {
        this.request = request;
    }

    public HttpClient getClient() {
        return client;
    }

    public void setClient(HttpClient client) {
        this.client = client;
    }
}
