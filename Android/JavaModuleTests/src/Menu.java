
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: Will Reynolds
 * Date: 1/31/12
 * Time: 11:48 AM
 */
public class Menu {

    public static Menu instance = new Menu();

    private String name;
    private List<MenuPage> menuPages;
    private MenuPage currentMenuPage;
    private int currentPage;
    private Map<Integer, MenuItem> menuItemMap;

    public Menu() {
        menuPages = new ArrayList<MenuPage>();
        menuItemMap = new HashMap<Integer, MenuItem>();
    }

    public void flipToNextPage() {
        currentPage = (currentPage + 1) % menuPages.size();
        updateCurrentPage();
    }

    public void flipToPreviousPage() {
        currentPage = ((currentPage - 1) + menuPages.size()) % menuPages.size();
        updateCurrentPage();
    }

    public void updateCurrentPage() {
        currentMenuPage = menuPages.get(currentPage);
    }

    public void addMenuPage(MenuPage menuPage) {
        menuPages.add(menuPage);
    }

    public List<MenuPage> getMenuPages() {
        return menuPages;
    }

    public void setMenuPages(List<MenuPage> menuPages) {
        this.menuPages = menuPages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MenuPage getCurrentMenuPage() {
        return currentMenuPage;
    }

    public void setCurrentMenuPage(MenuPage currentMenuPage) {
        this.currentMenuPage = currentMenuPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public Map<Integer, MenuItem> getMenuItemMap() {
        return menuItemMap;
    }

    public void setMenuItemMap(Map<Integer, MenuItem> menuItemMap) {
        this.menuItemMap = menuItemMap;
    }
}
