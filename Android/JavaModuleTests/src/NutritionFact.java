import java.io.Serializable;

/**
 * User: Will Reynolds
 * Date: 2/9/12
 * Time: 12:43 PM
 */
public class NutritionFact implements Serializable{
    private String type;
    private String fact;

    public NutritionFact() {
    }

    public NutritionFact(String type, String fact) {
        this.type = type;
        this.fact = fact;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFact() {
        return fact;
    }

    public void setFact(String fact) {
        this.fact = fact;
    }
}
