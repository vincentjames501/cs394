import org.apache.http.client.methods.HttpPost;

/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 11:10 PM
 */
public class HttpPostBuilder {
    public HttpPost buildHttpPost(String method) {
        HttpPost post = new HttpPost(method);
        post.addHeader("Cookie", StreamlineService.cookie);
        return post;
    }
}
