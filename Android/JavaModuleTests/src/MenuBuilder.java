import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: Will Reynolds
 * Date: 4/13/12
 * Time: 4:46 PM
 */
public class MenuBuilder {
    public static final String MENUS = "menus";
    public static final String MENU_PAGES = "menuPages";
    public static final String NAME = "name";
    public static final String PAGE_NUMBER = "pageNumber";
    public static final String CATEGORIES = "categories";
    public static final String MENU_ITEMS = "menuItems";
    public static final String RATING = "rating";
    public static final String TIMES_ORDERED = "timesOrdered";
    public static final String PRICE = "price";
    private static final String IGNORE_CATEGORY = "MONETARY_OPERATIONS";
    public static final String IMAGE = "image";
    public static final String SHORT_DESCRIPTION = "shortDescription";
    public static final String LONG_DESCRIPTION = "longDescription";
    public static final String ADDITIONAL_IMAGES = "additionalImages";
    public static final String NUTRITION_FACTS = "nutritionFacts";
    public static final String TYPE = "type";
    public static final String FACT = "fact";
    public static final String ID = "id";

    private HttpResponseDecoder responseDecoder;

    public MenuBuilder() {
        responseDecoder = new HttpResponseDecoder();
    }

    public Menu buildMenu(HttpResponse response) throws Exception {
        Menu menu = new Menu();
        String responseString = responseDecoder.getStringFromResponse(response);
        JSONArray responseArray = new JSONArray(responseString);
        JSONArray menuPages = getMenuPagesFromResponse(responseArray);
        menu.setName(responseArray.getJSONObject(0).getString(NAME));

        for (int i = 0; i < menuPages.length(); i++) {
            MenuPage menuPage = buildMenuPageFromJson(menuPages.getJSONObject(i));
            for (Category category : menuPage.getCategories()) {
                for (MenuItem menuItem : category.getMenuItems()) {
                    menu.getMenuItemMap().put(menuItem.getItemId(), menuItem);
                }
            }
            menu.addMenuPage(menuPage);
        }
        return menu;
    }

    public MenuPage buildMenuPageFromJson(JSONObject menuPageObject) throws JSONException {
        MenuPage menuPage = new MenuPage();
        menuPage.setPageNumber(menuPageObject.getInt(PAGE_NUMBER));
        JSONArray categoryArray = menuPageObject.getJSONArray(CATEGORIES);
        for(int i = 0; i < categoryArray.length(); i++) {
            JSONObject categoryObject = categoryArray.getJSONObject(i);
            if(acceptedCategory(categoryObject)) {
                menuPage.addCategory(buildCategoryFromJson(categoryObject));
            }
        }
        return menuPage;
    }

    public Category buildCategoryFromJson(JSONObject categoryObject) throws JSONException {
        Category category = new Category();
        category.setCategoryName(categoryObject.getString(NAME));
        JSONArray menuItemArray = categoryObject.getJSONArray(MENU_ITEMS);
        for(int i = 0; i < menuItemArray.length(); i++) {
            category.addMenuItem(buildMenuItemFromJson(menuItemArray.getJSONObject(i)));
        }
        return category;
    }

    public boolean acceptedCategory(JSONObject categoryObject) throws JSONException {
        return !categoryObject.getString(NAME).equalsIgnoreCase(IGNORE_CATEGORY);
    }

    public JSONArray getMenuPagesFromResponse(JSONArray responseArray) throws JSONException {
        return responseArray.getJSONObject(0).getJSONArray(MENUS).getJSONObject(0).getJSONArray(MENU_PAGES);
    }

    public MenuItem buildMenuItemFromJson(JSONObject menuItemObject) throws JSONException {
        MenuItem menuItem = new MenuItem();
        menuItem.setItemId(menuItemObject.getInt(ID));
        menuItem.setRating(menuItemObject.getString(RATING));
        menuItem.setTimesOrdered(menuItemObject.getInt(TIMES_ORDERED));
        menuItem.setName(menuItemObject.getString(NAME));
        menuItem.setPrice(Float.valueOf(menuItemObject.getString(PRICE)));
        menuItem.setMainImage(menuItemObject.getString(IMAGE));
        menuItem.setShortDescription(menuItemObject.getString(SHORT_DESCRIPTION));
        menuItem.setLongDescription(menuItemObject.getString(LONG_DESCRIPTION));
        menuItem.setAdditionalImages(buildAdditionalImagesListFromJson(menuItemObject.getJSONArray(ADDITIONAL_IMAGES)));
        menuItem.setNutritionFactMap(buildNutritionFactMap(menuItemObject.getJSONArray(NUTRITION_FACTS)));
        return menuItem;
    }
    
    public List<String> buildAdditionalImagesListFromJson(JSONArray additionalImagesJsonArray) throws JSONException {
        List<String> additionalImages = new ArrayList<String>();
        for(int x = 0; x < additionalImagesJsonArray.length(); x++) {
            additionalImages.add(additionalImagesJsonArray.getJSONObject(x).get(IMAGE).toString());
        }
        return additionalImages;
    }

    public Map<String, String> buildNutritionFactMap(JSONArray nutritionFactArray) throws JSONException {
        Map<String, String> nutritionFactMap = new HashMap<String, String>();
        for(int x = 0; x < nutritionFactArray.length(); x++) {
            JSONObject nutritionFactObject = nutritionFactArray.getJSONObject(x);
            nutritionFactMap.put(nutritionFactObject.getString(MenuBuilder.TYPE), nutritionFactObject.getString(MenuBuilder.FACT));
        }
        return nutritionFactMap;
    }

    public HttpResponseDecoder getResponseDecoder() {
        return responseDecoder;
    }

    public void setResponseDecoder(HttpResponseDecoder responseDecoder) {
        this.responseDecoder = responseDecoder;
    }
}
