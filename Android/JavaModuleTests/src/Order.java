import java.util.ArrayList;
import java.util.List;

/**
 * User: Will Reynolds
 * Date: 2/16/12
 * Time: 3:55 PM
 */
public class Order {
    public static Order order = new Order();

    private List<OrderItem> orderItems;

    private OrderItem editingItem;

    public Order() {
        orderItems = new ArrayList<OrderItem>();
    }

    public void clearOrder() {
        orderItems.clear();
    }

    public void addOrderItem(OrderItem orderItem) {
        orderItems.add(orderItem);
    }

    public void removeOrderItem(OrderItem orderItem) {
        orderItems.remove(orderItem);
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public OrderItem getEditingItem() {
        return editingItem;
    }

    public void setEditingItem(OrderItem editingItem) {
        this.editingItem = editingItem;
    }
}
