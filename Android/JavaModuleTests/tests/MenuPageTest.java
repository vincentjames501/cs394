import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 2:17 PM
 */
public class MenuPageTest {
    
    @Mock
    private Category category;
    
    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddCategory() throws Exception {
        MenuPage menuPage = new MenuPage();
        assertEquals(menuPage.getCategories().size(), 0);
        menuPage.addCategory(category);
        assertTrue(menuPage.getCategories().contains(category));
    }

    @Test
    public void testGetCategories() throws Exception {
        MenuPage menuPage = new MenuPage();
        assertEquals(menuPage.getCategories().size(), 0);
        List<Category> categoryList = new ArrayList<Category>();
        categoryList.add(mock(Category.class));
        menuPage.setCategories(categoryList);
        assertEquals(menuPage.getCategories(), categoryList);
    }

    @Test
    public void testSetCategories() throws Exception {
        MenuPage menuPage = new MenuPage();
        assertEquals(menuPage.getCategories().size(), 0);
        List<Category> categoryList = new ArrayList<Category>();
        categoryList.add(mock(Category.class));
        menuPage.setCategories(categoryList);
        assertEquals(menuPage.getCategories(), categoryList);
    }

    @Test
    public void testGetPageNumber() throws Exception {
        int pageNumber = 10;
        MenuPage menuPage = new MenuPage(pageNumber, "TEST");
        assertNotNull(menuPage.getPageNumber());
        assertEquals(menuPage.getPageNumber(), pageNumber);
    }

    @Test
    public void testSetPageNumber() throws Exception {
        int pageNumberOne = 10;
        int pageNumberTwo = 20;
        MenuPage menuPage = new MenuPage();
        assertEquals(menuPage.getPageNumber(), 0);
        menuPage.setPageNumber(pageNumberOne);
        assertNotNull(menuPage.getPageNumber());
        assertEquals(menuPage.getPageNumber(), pageNumberOne);
        menuPage.setPageNumber(pageNumberTwo);
        assertNotSame(menuPage.getPageNumber(), pageNumberOne);
    }

    @Test
    public void testGetName() throws Exception {
        String testName = "TEST";
        MenuPage menuPage = new MenuPage(10, testName);
        assertNotNull(menuPage.getName());
        assertEquals(menuPage.getName(), testName);
    }

    @Test
    public void testSetName() throws Exception {
        String testName = "TEST";
        MenuPage menuPage = new MenuPage();
        assertNull(menuPage.getName());
        menuPage.setName(testName);
        assertNotNull(menuPage.getName());
        assertEquals(menuPage.getName(), testName);
    }
}
