import org.junit.Test;


import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * User: Michael
 * Date: 4/12/12
 * Time: 2:40 PM
 */
public class OrderTest  {

    @Test
    public void testClearOrder() {
        Order order = new Order();
        OrderItem orderItem = mock(OrderItem.class);
        order.addOrderItem(orderItem);
        order.clearOrder();
        assertEquals(order.getOrderItems().size(), 0);
        assertTrue(!order.getOrderItems().contains(orderItem));
        assertTrue(order.getOrderItems().isEmpty());
    }

    @Test
    public void testAddOrderItem() {
        Order order = new Order();
        OrderItem orderItem = mock(OrderItem.class);
        order.addOrderItem(orderItem);
        assertTrue(order.getOrderItems().contains(orderItem));
        assertTrue(!order.getOrderItems().isEmpty());
        assertEquals(order.getOrderItems().size(), 1);
    }

    @Test
    public void testRemoveOrderItem() {
        Order order = new Order();
        OrderItem item1 = mock(OrderItem.class);
        OrderItem item2 = mock(OrderItem.class);

        order.addOrderItem(item1);
        order.addOrderItem(item2);

        order.removeOrderItem(item1);
        assertEquals(order.getOrderItems().size(), 1);
        assertTrue(!order.getOrderItems().contains(item1));
    }

    @Test
    public void testGetOrderItems() {
        Order order = new Order();
        assertNotNull(order.getOrderItems());
    }

    @Test
    public void testSetOrderItems() {
        List<OrderItem> orderItems = new ArrayList<OrderItem>(2);
        Order order = new Order();
        OrderItem item1 = mock(OrderItem.class);
        OrderItem item2 = mock(OrderItem.class);

        orderItems.add(item1);
        orderItems.add(item2);

        order.setOrderItems(orderItems);

        assertNotNull(order.getOrderItems());
        assertEquals(order.getOrderItems().size(), 2);
        assertTrue(order.getOrderItems().contains(item1));
        assertTrue(order.getOrderItems().contains(item2));
    }

    @Test
    public void testGetEditingItem() {
        Order order = new Order();
        OrderItem orderItem = mock(OrderItem.class);

        order.setEditingItem(orderItem);

        assertNotNull(order.getEditingItem());
        assertTrue(order.getEditingItem().equals(orderItem));
    }

    @Test
    public void testSetEditingItem() {
        Order order = new Order();
        OrderItem orderItem = mock(OrderItem.class);

        order.setEditingItem(orderItem);

        assertNotNull(order.getEditingItem());
        assertTrue(order.getEditingItem().equals(orderItem));
    }
}
