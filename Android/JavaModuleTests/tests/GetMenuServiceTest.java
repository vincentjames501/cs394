import org.apache.http.HttpResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.when;

/**
* User: Will Reynolds
* Date: 4/12/12
* Time: 4:03 PM
*/
public class GetMenuServiceTest {
    @Mock
    private StreamlineService streamlineService;

    @Mock
    HttpResponse response;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testExecute() throws Exception {
        response.setStatusCode(200);
        GetMenuService getMenuService = new GetMenuService();
        getMenuService.setStreamlineService(streamlineService);
        when(streamlineService.execute()).thenReturn(response);
        HttpResponse actualResponse = getMenuService.execute();
        assertNotNull(actualResponse);
        assertEquals(actualResponse, response);
    }

    @Test
    public void testSetStreamlineService() {
        GetMenuService getMenuService = new GetMenuService();
        getMenuService.setStreamlineService(streamlineService);
        assertEquals(getMenuService.getStreamlineService(), streamlineService);
    }

    @Test
    public void testGetStreamlineService() {
        GetMenuService getMenuService = new GetMenuService();
        getMenuService.setStreamlineService(streamlineService);
        assertEquals(getMenuService.getStreamlineService(), streamlineService);
    }
}
