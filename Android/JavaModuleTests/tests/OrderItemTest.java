import org.junit.Test;


import java.util.HashMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * User: Michael
 * Date: 4/12/12
 * Time: 6:04 PM
 */
public class OrderItemTest {

    @Test
    public void testOrderItemWithoutID() {
        final String comment = "Bob";
        final int quantity = 2;
        final int id = 3;

        MenuItem menuItem = new MenuItem();
        menuItem.setItemId(id);
        OrderItem orderItem = new OrderItem(menuItem, comment, quantity);

        assertNotNull(orderItem);
        assertEquals(orderItem.getComment(), comment);
        assertEquals(orderItem.getQuantity(), quantity);
        assertEquals(orderItem.getId(), id);
    }

    @Test
    public void testGetMenu() {
        final String comment = "asdf";
        final int quantity = 3;
        final int id = 1;

        MenuItem menuItem = mock(MenuItem.class);
        when(menuItem.getItemId()).thenReturn(id);
        OrderItem orderItem = new OrderItem(menuItem, comment, quantity);
        Map<Integer,MenuItem> map = new HashMap<Integer, MenuItem>();
        map.put(1, menuItem);

        Menu.instance.setMenuItemMap(map);

        assertNotNull(orderItem.getAssociatedMenuItem());
        assertEquals(orderItem.getAssociatedMenuItem(), menuItem);
    }

    @Test
    public void testGetComment() {
        final String comment = "Bob";
        final int quantity = 2;
        final int id = 3;

        MenuItem menuItem = mock(MenuItem.class);
        menuItem.setItemId(id);
        OrderItem orderItem = new OrderItem(menuItem, comment, quantity);

        assertNotNull(orderItem.getComment());
        assertEquals(orderItem.getComment(), comment);
    }

    @Test
    public void testSetComment() {
        final String comment = "Bob";
        final int quantity = 2;
        final int id = 3;

        MenuItem menuItem = mock(MenuItem.class);
        menuItem.setItemId(id);
        OrderItem orderItem = new OrderItem(menuItem, comment, quantity);
        orderItem.setComment(comment + "new");

        assertNotNull(orderItem.getComment());
        assertEquals(orderItem.getComment(), comment + "new");
    }

    @Test
    public void testGetQuantity() {
        final String comment = "Bob";
        final int quantity = 2;
        final int id = 3;

        MenuItem menuItem = mock(MenuItem.class);
        menuItem.setItemId(id);
        OrderItem orderItem = new OrderItem(menuItem, comment, quantity);

        assertNotNull(orderItem.getQuantity());
        assertEquals(orderItem.getQuantity(), quantity);
    }

    @Test
    public void testSetQuantity() {
        final String comment = "Bob";
        final int quantity = 2;
        final int id = 3;

        MenuItem menuItem = mock(MenuItem.class);
        menuItem.setItemId(id);
        OrderItem orderItem = new OrderItem(menuItem, comment, quantity);
        orderItem.setQuantity(quantity + 1);

        assertNotNull(orderItem.getQuantity());
        assertEquals(orderItem.getQuantity(), quantity + 1);
    }

    @Test
    public void testGetID() {
        final String comment = "Bob";
        final int quantity = 2;
        final int id = 3;

        MenuItem menuItem = mock(MenuItem.class);
        when(menuItem.getItemId()).thenReturn(id);

        OrderItem orderItem = new OrderItem(menuItem, comment, quantity);

        assertNotNull(orderItem.getId());
        assertEquals(orderItem.getId(), id);
    }

    @Test
    public void testSetID() {
        final String comment = "Bob";
        final int quantity = 2;
        final int id = 3;

        MenuItem menuItem = mock(MenuItem.class);
        menuItem.setItemId(id);
        OrderItem orderItem = new OrderItem(menuItem, comment, quantity);
        orderItem.setId(id + 1);

        assertNotNull(orderItem.getId());
        assertEquals(orderItem.getId(), id + 1);
    }
}
