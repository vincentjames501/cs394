import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 4:28 PM
 */
public class StreamlineServiceTest {

    @Mock
    private HttpUriRequest request;

    @Mock
    private HttpResponse response;

    @Mock
    private HttpClient client;

    @Mock
    private StatusLine statusLine;

    private static final String LOGIN = "/Menu/doLogin";
    private static final String DOMAIN = "http://streamlinedine.com";
    private static final String GET_MENU = "/Menu/getMenu";
    private static final String SUBMIT_ORDER = "/Menu/createNewOrder";

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHasServiceMethods() {
        assertNotNull(StreamlineService.LOGIN);
        assertNotNull(StreamlineService.GET_MENU);
        assertNotNull(StreamlineService.SUBMIT_ORDER);
        assertNotNull(StreamlineService.SUCCESS);
        assertEquals(StreamlineService.LOGIN, DOMAIN + LOGIN);
        assertEquals(StreamlineService.GET_MENU, DOMAIN + GET_MENU);
        assertEquals(StreamlineService.SUBMIT_ORDER, DOMAIN + SUBMIT_ORDER);
        assertEquals(StreamlineService.SUCCESS, 200);
    }

    @Test
    public void testExecute() throws Exception {
        StreamlineService service = new StreamlineService();
        service.setRequest(request);
    }

    @Test(expected = ServerErrorException.class)
    public void testBadClientExecution() throws Exception {
        StreamlineService service = new StreamlineService();
        when(client.execute(request)).thenThrow(new IOException());
        service.setRequest(request);
        service.setClient(client);
        service.execute();
    }

    @Test(expected = ServerErrorException.class)
    public void testServerInternalError() throws Exception {
        StreamlineService service = new StreamlineService();
        service.setRequest(request);
        service.setClient(client);
        when(client.execute(request)).thenReturn(response);
        when(response.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(500);
        service.execute();
    }

    @Test
    public void testSuccessfulRequest() throws ServerErrorException, IOException {
        StreamlineService service = new StreamlineService();
        service.setRequest(request);
        service.setClient(client);
        when(client.execute(request)).thenReturn(response);
        when(response.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(200);
        HttpResponse realResponse = service.execute();
        assertNotNull(realResponse);
        assertEquals(realResponse, response);
    }

    @Test
    public void testGetRequest() throws Exception {
        StreamlineService service = new StreamlineService();
        service.setRequest(request);
        assertNotNull(service.getRequest());
        assertEquals(service.getRequest(), request);
    }

    @Test
    public void testSetRequest() throws Exception {
        StreamlineService service = new StreamlineService();
        service.setRequest(request);
        assertNotNull(service.getRequest());
        assertEquals(service.getRequest(), request);
    }

    @Test
    public void testSetClient() throws Exception {
        StreamlineService service = new StreamlineService();
        service.setClient(client);
        assertNotNull(service.getClient());
        assertEquals(service.getClient(), client);
    }

    @Test
    public void testGetClient() throws Exception {
        StreamlineService service = new StreamlineService();
        service.setClient(client);
        assertNotNull(service.getClient());
        assertEquals(service.getClient(), client);
    }
}
