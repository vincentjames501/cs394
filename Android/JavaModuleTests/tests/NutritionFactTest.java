import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 2:37 PM
 */
public class NutritionFactTest {

    @Test
    public void testGetType() throws Exception {
        String type = "TEST";
        NutritionFact fact = new NutritionFact(type, "FACT");
        assertNotNull(fact.getType());
        assertEquals(fact.getType(), type);
    }

    @Test
    public void testSetType() throws Exception {
        String type = "TEST";
        NutritionFact fact = new NutritionFact();
        assertNull(fact.getType());
        fact.setType(type);
        assertEquals(fact.getType(), type);
    }

    @Test
    public void testGetFact() throws Exception {
        String fact = "TEST";
        NutritionFact nutritionFact = new NutritionFact("TYPE", fact);
        assertNotNull(nutritionFact.getFact());
        assertEquals(nutritionFact.getFact(), fact);
    }

    @Test
    public void testSetFact() throws Exception {
        String fact = "TEST";
        NutritionFact nutritionFact = new NutritionFact();
        assertNull(nutritionFact.getFact());
        nutritionFact.setFact(fact);
        assertEquals(nutritionFact.getFact(), fact);
    }
}
