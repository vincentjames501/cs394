import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.UnsupportedEncodingException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 5:34 PM
 */
public class SubmitOrderServiceTest {

    @Mock
    StreamlineService streamlineService;

    @Mock
    HttpResponse response;

    @Mock
    HttpPost post;

    @Mock
    HttpPostBuilder postBuilder;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testExecute() throws Exception {
        SubmitOrderService submitOrderService = new SubmitOrderService();
        submitOrderService.setStreamlineService(streamlineService);
        when(streamlineService.execute()).thenReturn(response);
        HttpResponse actualResponse = submitOrderService.execute();
        assertNotNull(actualResponse);
        assertEquals(actualResponse, response);
    }

    @Test(expected = UnsupportedEncodingException.class)
    public void testBadUrlException() throws Exception {
        SubmitOrderService submitOrderService = new SubmitOrderService();
        submitOrderService.setHttpPostBuilder(postBuilder);
        when(postBuilder.buildHttpPost(anyString())).thenReturn(post);
        submitOrderService.setStreamlineService(streamlineService);
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw new UnsupportedEncodingException();
            }
        }).when(post).setEntity(Matchers.<HttpEntity>any());
        submitOrderService.execute();
    }

    @Test
    public void testGetStreamlineService() throws Exception {
        SubmitOrderService submitOrderService = new SubmitOrderService();
        submitOrderService.setStreamlineService(streamlineService);
        assertNotNull(submitOrderService.getStreamlineService());
        assertEquals(submitOrderService.getStreamlineService(), streamlineService);
    }

    @Test
    public void testSetStreamlineService() throws Exception {
        SubmitOrderService submitOrderService = new SubmitOrderService();
        submitOrderService.setStreamlineService(streamlineService);
        assertEquals(submitOrderService.getStreamlineService(), streamlineService);
    }

    @Test
    public void testGetHttpPostBuilder() {
        SubmitOrderService submitOrderService = new SubmitOrderService();
        assertNotNull(submitOrderService.getHttpPostBuilder());
        submitOrderService.setHttpPostBuilder(postBuilder);
        assertEquals(submitOrderService.getHttpPostBuilder(), postBuilder);
    }

    @Test
    public void testSetHttpPostBuilder() {
        SubmitOrderService submitOrderService = new SubmitOrderService();
        submitOrderService.setHttpPostBuilder(postBuilder);
        assertEquals(submitOrderService.getHttpPostBuilder(), postBuilder);
    }

    @Test
    public void testGetGson() {
        SubmitOrderService submitOrderService = new SubmitOrderService();
        assertNotNull(submitOrderService.getGson());
        Gson testGson = new Gson();
        submitOrderService.setGson(testGson);
        assertEquals(submitOrderService.getGson(), testGson);
    }

    @Test
    public void testSetGson() {
        SubmitOrderService submitOrderService = new SubmitOrderService();
        assertNotNull(submitOrderService.getGson());
        Gson testGson = new Gson();
        submitOrderService.setGson(testGson);
        assertEquals(submitOrderService.getGson(), testGson);
    }
}
