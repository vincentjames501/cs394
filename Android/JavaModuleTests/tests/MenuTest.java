import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Aaron Easter
 * Date: 4/12/12
 * Time: 2:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class MenuTest extends TestCase {
    public void setUp() throws Exception {

    }

    public void tearDown() throws Exception {

    }

    public void testFlipToNextPage() throws Exception {
        Menu testMenu = new Menu();
        List<MenuPage> menuPages = new ArrayList<MenuPage>();
        MenuPage page1 = new MenuPage(), page2 = new MenuPage();
        page1.setPageNumber( 0 );
        page2.setPageNumber( 1 );
        menuPages.add( page1 );
        menuPages.add( page2 );
        testMenu.setMenuPages( menuPages );
        testMenu.setCurrentPage( page1.getPageNumber() );
        testMenu.updateCurrentPage();
        testMenu.flipToNextPage();
        assertEquals( page2.getPageNumber(), testMenu.getCurrentMenuPage().getPageNumber());
        testMenu.flipToNextPage();
        assertEquals( page1.getPageNumber(), testMenu.getCurrentMenuPage().getPageNumber());
    }

    public void testFlipToPreviousPage() throws Exception {
        Menu testMenu = new Menu();
        List<MenuPage> menuPages = new ArrayList<MenuPage>();
        MenuPage page1 = new MenuPage(), page2 = new MenuPage();
        page1.setPageNumber( 0 );
        page2.setPageNumber( 1 );
        menuPages.add( page1 );
        menuPages.add( page2 );
        testMenu.setMenuPages( menuPages );
        testMenu.setCurrentPage( page1.getPageNumber() );
        testMenu.updateCurrentPage();
        testMenu.flipToPreviousPage();
        assertEquals( page2.getPageNumber(), testMenu.getCurrentMenuPage().getPageNumber());
        testMenu.flipToPreviousPage();
        assertEquals( page1.getPageNumber(), testMenu.getCurrentMenuPage().getPageNumber());
    }

    public void testUpdateCurrentPage() throws Exception {
        Menu testMenu = new Menu();
        List<MenuPage> menuPages = new ArrayList<MenuPage>();
        MenuPage page1 = new MenuPage(), page2 = new MenuPage();
        page1.setPageNumber( 0 );
        page2.setPageNumber( 1 );
        menuPages.add( page1 );
        menuPages.add( page2 );
        testMenu.setMenuPages( menuPages );
        testMenu.setCurrentPage( page1.getPageNumber() );
        testMenu.updateCurrentPage();
        assertEquals( page1.getPageNumber(), testMenu.getCurrentMenuPage().getPageNumber());
    }

    public void testAddMenuPage() throws Exception {
        Menu testMenu = new Menu();
        List<MenuPage> pages = new ArrayList<MenuPage>() {};
        MenuPage menuPage = new MenuPage();
        testMenu.setMenuPages(pages);
        testMenu.addMenuPage(menuPage);
        assertTrue( testMenu.getMenuPages().contains( menuPage ) );
    }

    public void testGetMenuPages() throws Exception {
        Menu testMenu = new Menu();
        List<MenuPage> pages = new ArrayList<MenuPage>() {};
        MenuPage menuPage = new MenuPage();
        pages.add( menuPage );
        testMenu.setMenuPages( pages );
        assertEquals( pages, testMenu.getMenuPages() );
    }

    public void testSetMenuPages() throws Exception {
        Menu testMenu = new Menu();
        List<MenuPage> pages = new ArrayList<MenuPage>() {};
        testMenu.setMenuPages( pages );
        assertEquals( pages, testMenu.getMenuPages() );
    }

    public void testGetName() throws Exception {
        Menu testMenu = new Menu();
        String name = "Test";
        testMenu.setName( name );
        assertEquals( name, testMenu.getName() );
    }

    public void testSetName() throws Exception {
        Menu testMenu = new Menu();
        String name = "Test";
        testMenu.setName( name );
        assertEquals( name, testMenu.getName() );
    }

    public void testGetCurrentMenuPage() throws Exception {
        Menu testMenu = new Menu();
        MenuPage page = new MenuPage();
        testMenu.setCurrentMenuPage( page );
        assertEquals( page, testMenu.getCurrentMenuPage() );
    }

    public void testSetCurrentMenuPage() throws Exception {
        Menu testMenu = new Menu();
        MenuPage page = new MenuPage();
        testMenu.setCurrentMenuPage( page );
        assertEquals( page, testMenu.getCurrentMenuPage() );
    }

    public void testGetCurrentPage() throws Exception {
        Menu testMenu = new Menu();
        int pageNumber = 3;
        testMenu.setCurrentPage( pageNumber );
        assertEquals( pageNumber, testMenu.getCurrentPage() );
    }

    public void testSetCurrentPage() throws Exception {
        Menu testMenu = new Menu();
        int pageNumber = 3;
        testMenu.setCurrentPage( pageNumber );
        assertEquals( pageNumber, testMenu.getCurrentPage() );
    }

    public void testGetMenuItemMap() throws Exception {
        Menu testMenu = new Menu();
        Map<Integer, MenuItem> map = new HashMap<Integer, MenuItem>();
        testMenu.setMenuItemMap( map );
        assertEquals( map, testMenu.getMenuItemMap() );
    }

    public void testSetMenuItemMap() throws Exception {
        Menu testMenu = new Menu();
        Map<Integer, MenuItem> map = new HashMap<Integer, MenuItem>();
        testMenu.setMenuItemMap( map );
        assertEquals( map, testMenu.getMenuItemMap() );
    }
}
