import org.apache.http.HttpResponse;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * User: Will Reynolds
 * Date: 4/15/12
 * Time: 2:36 PM
 */
public class MenuBuilderTest {

    @Mock
    private JSONObject menuItemObject;

    @Mock
    private HashMap<String, String> nutritionMap;

    @Mock
    private List<String> additionalImages;

    @Mock
    private HttpResponseDecoder responseDecoder;

    @Mock
    private HttpResponse response;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testBuildMenu() throws Exception {
        String menuJSON = "[\n" +
                "    {\n" +
                "        \"menus\": [\n" +
                "            {\n" +
                "                \"menuPages\": [\n" +
                "                    {\n" +
                "                        \"pageNumber\": 1, \n" +
                "                        \"categories\": [\n" +
                "                            {\n" +
                "                                \"menuItems\": [\n" +
                "                                    {\n" +
                "                                        \"rating\": \"0.0\", \n" +
                "                                        \"timesOrdered\": 0, \n" +
                "                                        \"name\": \"Boca Burger\", \n" +
                "                                        \"image\": \"/media/items/soy-burger_4.jpg\", \n" +
                "                                        \"additionalImages\": [\n" +
                "                                            {\n" +
                "                                                \"image\": \"/media/items/gardenburger_3.png\"\n" +
                "                                            }, \n" +
                "                                            {\n" +
                "                                                \"image\": \"/media/items/soy-burger_3.jpg\"\n" +
                "                                            }\n" +
                "                                        ], \n" +
                "                                        \"price\": \"5.5\", \n" +
                "                                        \"nutritionFacts\": [\n" +
                "                                            {\n" +
                "                                                \"type\": \"n_sugars_percent\", \n" +
                "                                                \"fact\": \"19%\"\n" +
                "                                            }\n" +
                "                                        ], \n" +
                "                                        \"shortDescription\": \"Delicious Veggie Burger\", \n" +
                "                                        \"id\": 1, \n" +
                "                                        \"longDescription\": \"Long description\"\n" +
                "                                    }, \n" +
                "                                    {\n" +
                "                                        \"rating\": \"0.0\", \n" +
                "                                        \"timesOrdered\": 0, \n" +
                "                                        \"name\": \"Bob's Steakburger\", \n" +
                "                                        \"image\": \"/media/items/steakburger.jpg\", \n" +
                "                                        \"additionalImages\": [\n" +
                "                                            {\n" +
                "                                                \"image\": \"/media/items/steakburger_onions.jpg\"\n" +
                "                                            }\n" +
                "                                        ], \n" +
                "                                        \"price\": \"6.5\", \n" +
                "                                        \"nutritionFacts\": [\n" +
                "                                            {\n" +
                "                                                \"type\": \"n_calories\", \n" +
                "                                                \"fact\": \"100\"\n" +
                "                                            }\n" +
                "                                        ], \n" +
                "                                        \"shortDescription\": \"Meat!\", \n" +
                "                                        \"id\": 3, \n" +
                "                                        \"longDescription\": \"\"\n" +
                "                                    }\n" +
                "                                ], \n" +
                "                                \"name\": \"Burgers\"\n" +
                "                            }, \n" +
                "                            {\n" +
                "                                \"menuItems\": [\n" +
                "                                    {\n" +
                "                                        \"rating\": \"0.0\", \n" +
                "                                        \"timesOrdered\": 0, \n" +
                "                                        \"name\": \"French Fries\", \n" +
                "                                        \"image\": \"/media/items/French_fries.jpg\", \n" +
                "                                        \"additionalImages\": [], \n" +
                "                                        \"price\": \"0.99\", \n" +
                "                                        \"nutritionFacts\": [], \n" +
                "                                        \"shortDescription\": \"Golden!\", \n" +
                "                                        \"id\": 4, \n" +
                "                                        \"longDescription\": \"\"\n" +
                "                                    }, \n" +
                "                                    {\n" +
                "                                        \"rating\": \"0.0\", \n" +
                "                                        \"timesOrdered\": 0, \n" +
                "                                        \"name\": \"Curly Fries\", \n" +
                "                                        \"image\": \"/media/items/2168671.jpg\", \n" +
                "                                        \"additionalImages\": [], \n" +
                "                                        \"price\": \"0.99\", \n" +
                "                                        \"nutritionFacts\": [], \n" +
                "                                        \"shortDescription\": \"Curly!\", \n" +
                "                                        \"id\": 5, \n" +
                "                                        \"longDescription\": \"\"\n" +
                "                                    }\n" +
                "                                ], \n" +
                "                                \"name\": \"Sides\"\n" +
                "                            }, \n" +
                "                            {\n" +
                "                                \"menuItems\": [\n" +
                "                                    {\n" +
                "                                        \"rating\": \"0.0\", \n" +
                "                                        \"timesOrdered\": 0, \n" +
                "                                        \"name\": \"Gratituity\", \n" +
                "                                        \"image\": \"/media/items/blank.jpg\", \n" +
                "                                        \"additionalImages\": [], \n" +
                "                                        \"price\": \"0.0\", \n" +
                "                                        \"nutritionFacts\": [], \n" +
                "                                        \"shortDescription\": \"\", \n" +
                "                                        \"id\": 8, \n" +
                "                                        \"longDescription\": \"\"\n" +
                "                                    }, \n" +
                "                                    {\n" +
                "                                        \"rating\": \"0.0\", \n" +
                "                                        \"timesOrdered\": 0, \n" +
                "                                        \"name\": \"Discount\", \n" +
                "                                        \"image\": \"/media/items/blank.jpg\", \n" +
                "                                        \"additionalImages\": [], \n" +
                "                                        \"price\": \"0.0\", \n" +
                "                                        \"nutritionFacts\": [], \n" +
                "                                        \"shortDescription\": \"\", \n" +
                "                                        \"id\": 9, \n" +
                "                                        \"longDescription\": \"\"\n" +
                "                                    }\n" +
                "                                ], \n" +
                "                                \"name\": \"Monetary_Operations\"\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    }, \n" +
                "                    {\n" +
                "                        \"pageNumber\": 2, \n" +
                "                        \"categories\": [\n" +
                "                            {\n" +
                "                                \"menuItems\": [\n" +
                "                                    {\n" +
                "                                        \"rating\": \"0.0\", \n" +
                "                                        \"timesOrdered\": 0, \n" +
                "                                        \"name\": \"Dr. Pepper\", \n" +
                "                                        \"image\": \"/media/items/dr-pepper.gif\", \n" +
                "                                        \"additionalImages\": [], \n" +
                "                                        \"price\": \"2.0\", \n" +
                "                                        \"nutritionFacts\": [\n" +
                "                                            {\n" +
                "                                                \"type\": \"n_calories\", \n" +
                "                                                \"fact\": \"150\"\n" +
                "                                            }\n" +
                "                                        ], \n" +
                "                                        \"shortDescription\": \"Always one of a kind!\", \n" +
                "                                        \"id\": 2, \n" +
                "                                        \"longDescription\": \"\"\n" +
                "                                    }, \n" +
                "                                    {\n" +
                "                                        \"rating\": \"0.0\", \n" +
                "                                        \"timesOrdered\": 0, \n" +
                "                                        \"name\": \"Coca-Cola\", \n" +
                "                                        \"image\": \"/media/items/CocaCola.jpg\", \n" +
                "                                        \"additionalImages\": [], \n" +
                "                                        \"price\": \"2.0\", \n" +
                "                                        \"nutritionFacts\": [\n" +
                "                                            {\n" +
                "                                                \"type\": \"n_calories\", \n" +
                "                                                \"fact\": \"140\"\n" +
                "                                            }\n" +
                "                                        ], \n" +
                "                                        \"shortDescription\": \"Open Happiness\", \n" +
                "                                        \"id\": 6, \n" +
                "                                        \"longDescription\": \"\"\n" +
                "                                    }, \n" +
                "                                    {\n" +
                "                                        \"rating\": \"0.0\", \n" +
                "                                        \"timesOrdered\": 0, \n" +
                "                                        \"name\": \"Pepsi\", \n" +
                "                                        \"image\": \"/media/items/220px-Pepsi_logo.svg.png\", \n" +
                "                                        \"additionalImages\": [], \n" +
                "                                        \"price\": \"2.0\", \n" +
                "                                        \"nutritionFacts\": [\n" +
                "                                            {\n" +
                "                                                \"type\": \"n_calories\", \n" +
                "                                                \"fact\": \"150\"\n" +
                "                                            }\n" +
                "                                        ], \n" +
                "                                        \"shortDescription\": \"Fresh\", \n" +
                "                                        \"id\": 7, \n" +
                "                                        \"longDescription\": \"\"\n" +
                "                                    }\n" +
                "                                ], \n" +
                "                                \"name\": \"Beverages\"\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    }\n" +
                "                ]\n" +
                "            }\n" +
                "        ], \n" +
                "        \"name\": \"Bob's Burgers\"\n" +
                "    }\n" +
                "]";
        when(responseDecoder.getStringFromResponse(Matchers.<HttpResponse>any())).thenReturn(menuJSON);
        MenuBuilder builder = new MenuBuilder();
        builder.setResponseDecoder(responseDecoder);
        assertEquals(builder.getResponseDecoder(), responseDecoder);
        Menu menu = builder.buildMenu(response);
        assertEquals(menu.getName(), "Bob's Burgers");
        assertEquals(menu.getMenuPages().size(), 2);
        assertEquals(menu.getMenuItemMap().size(),7);
        MenuPage menuPageOne = menu.getMenuPages().get(0);
        assertNotNull(menuPageOne);
        assertEquals(menuPageOne.getPageNumber(), 1);
        assertEquals(menuPageOne.getCategories().size(), 2);
        Category categoryOne = menuPageOne.getCategories().get(0);
        assertNotNull(categoryOne);
        assertEquals(categoryOne.getCategoryName(), "Burgers");
        assertEquals(categoryOne.getMenuItems().size(), 2);
        MenuItem menuItemOne = categoryOne.getMenuItems().get(0);
        assertNotNull(menuItemOne);
        assertEquals(menuItemOne.getRating(), "0.0");
        assertEquals(menuItemOne.getTimesOrdered(), 0);
        assertEquals(menuItemOne.getName(), "Boca Burger");
        assertEquals(menuItemOne.getMainImage(), "/media/items/soy-burger_4.jpg");
        assertEquals(menuItemOne.getAdditionalImages().size(), 2);
        assertTrue(menuItemOne.getAdditionalImages().contains("/media/items/gardenburger_3.png"));
        assertTrue(menuItemOne.getAdditionalImages().contains("/media/items/soy-burger_3.jpg"));
        assertEquals(menuItemOne.getPrice(), 5.5f);
        assertEquals(menuItemOne.getNutritionFactMap().size(), 1);
        assertTrue(menuItemOne.getNutritionFactMap().containsKey("n_sugars_percent"));
        assertEquals(menuItemOne.getNutritionFactMap().get("n_sugars_percent"), "19%");
        assertEquals(menuItemOne.getShortDescription(), "Delicious Veggie Burger");
        assertEquals(menuItemOne.getItemId(), 1);
        assertEquals(menuItemOne.getRating(), "0.0");
        assertEquals(menuItemOne.getLongDescription(), "Long description");
    }
}
