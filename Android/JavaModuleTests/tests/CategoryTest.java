import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.*;

/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 1:39 PM
 */
public class CategoryTest {

    @Mock
    private MenuItem menuItem;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddMenuItem() throws Exception {
        Category category = new Category();
        category.addMenuItem(menuItem);
        assertTrue(category.getMenuItems().contains(menuItem));
    }

    @Test
    public void testGetCategoryName() throws Exception {
        String categoryName = "TEST";
        Category category = new Category(categoryName);
        assertNotNull(category.getCategoryName());
    }

    @Test
    public void testSetCategoryName() throws Exception {
        String categoryName = "TEST";
        Category category = new Category();
        assertNull(category.getCategoryName());
        category.setCategoryName(categoryName);
        assertNotNull(category.getCategoryName());
        assertTrue(category.getCategoryName().equals(categoryName));
    }

    @Test
    public void testGetMenuItems() throws Exception {
        Category category = new Category();
        assertEquals(category.getMenuItems().size(), 0);
        List<MenuItem> menuItems = new ArrayList<MenuItem>();
        menuItems.add(menuItem);
        category.setMenuItems(menuItems);
        assertNotNull(category.getMenuItems());
        assertEquals(category.getMenuItems(), menuItems);
    }

    @Test
    public void testSetMenuItems() throws Exception {
        Category category = new Category();
        List<MenuItem> menuItems = new ArrayList<MenuItem>();
        menuItems.add(menuItem);
        category.setMenuItems(menuItems);
        assertNotNull(category.getMenuItems());
        assertEquals(category.getMenuItems(), menuItems);
    }
}
