import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * User: Will Reynolds
 * Date: 4/12/12
 * Time: 2:13 PM
 */
public class DomainTest {

    @Test
    public void testDomain() {
        Domain domain = new Domain();
        assertNotNull(Domain.domain);
        assertEquals(Domain.domain, "http://streamlinedine.com");
    }
}
