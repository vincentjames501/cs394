import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Aaron Easter
 * Date: 4/15/12
 * Time: 5:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class MenuItemTest extends TestCase {
    public void setUp() throws Exception {

    }

    public void tearDown() throws Exception {

    }

    public void testAddNutritionFact() throws Exception {
        MenuItem testItem = new MenuItem();
        List<NutritionFact> nutritionFacts = new ArrayList<NutritionFact>();
        NutritionFact fact1 = new NutritionFact( "name", "foodstuffs");
        NutritionFact fact2 = new NutritionFact( "calories", "all of them");
        nutritionFacts.add( fact1 );
        testItem.setNutritionFacts(nutritionFacts);
        testItem.addNutritionFact( fact2 );
        assertTrue(testItem.getNutritionFacts().contains(fact1));
        assertTrue( testItem.getNutritionFacts().contains( fact2 ) );
    }

    public void testAddAdditionalImage() throws Exception {
        MenuItem testItem = new MenuItem();
        String image = new String( "facepalm.png" );
        testItem.addAdditionalImage( image );
        assertTrue( testItem.getAdditionalImages().contains( image ) );
    }

    public void testGetName() throws Exception {

    }

    public void testSetName() throws Exception {

    }

    public void testGetShortDescription() throws Exception {

    }

    public void testSetShortDescription() throws Exception {

    }

    public void testGetLongDescription() throws Exception {

    }

    public void testSetLongDescription() throws Exception {

    }

    public void testGetPrice() throws Exception {

    }

    public void testSetPrice() throws Exception {

    }

    public void testGetRating() throws Exception {

    }

    public void testSetRating() throws Exception {

    }

    public void testGetTimesOrdered() throws Exception {

    }

    public void testSetTimesOrdered() throws Exception {

    }

    public void testGetNutritionFacts() throws Exception {
        MenuItem testItem = new MenuItem();
        List<NutritionFact> nutritionFacts = new ArrayList<NutritionFact>();
        NutritionFact fact = new NutritionFact( "name", "foodstuffs");
        nutritionFacts.add( fact );
        testItem.setNutritionFacts( nutritionFacts );
        assertTrue( testItem.getNutritionFacts().contains( fact ) );
    }

    public void testSetNutritionFacts() throws Exception {
        MenuItem testItem = new MenuItem();
        List<NutritionFact> nutritionFacts = new ArrayList<NutritionFact>();
        NutritionFact fact = new NutritionFact( "name", "foodstuffs");
        nutritionFacts.add( fact );
        testItem.setNutritionFacts( nutritionFacts );
        assertTrue( testItem.getNutritionFacts().contains( fact ) );
    }

    public void testGetNutritionFactMap() throws Exception {

    }

    public void testSetNutritionFactMap() throws Exception {

    }

    public void testGetMainImage() throws Exception {

    }

    public void testSetMainImage() throws Exception {

    }

    public void testGetAdditionalImages() throws Exception {

    }

    public void testSetAdditionalImages() throws Exception {

    }

    public void testGetItemId() throws Exception {
        MenuItem testItem = new MenuItem();
        testItem.setItemId( 1 );
        assertEquals( 1, testItem.getItemId() );
    }

    public void testSetItemId() throws Exception {
        MenuItem testItem = new MenuItem();
        testItem.setItemId( 1 );
        assertEquals( 1, testItem.getItemId() );
    }
}
