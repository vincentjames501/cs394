import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.UnsupportedEncodingException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

/**
 * User: Will Reynolds
 * Date: 4/13/12
 * Time: 3:35 PM
 */
public class LoginServiceTest {

    @Mock
    private StreamlineService streamlineService;

    @Mock
    private HttpPost post;

    @Mock
    private HttpPostBuilder postBuilder;

    @Mock
    private HttpResponse response;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testExecute() throws Exception {
        LoginService loginService = new LoginService();
        loginService.setUsername("test_username");
        loginService.setPassword("test_password");
        loginService.setStreamlineService(streamlineService);
        when(streamlineService.execute()).thenReturn(response);
        HttpResponse actualResponse = loginService.execute();
        assertNotNull(actualResponse);
        assertEquals(actualResponse, response);
    }

    @Test(expected = UnsupportedEncodingException.class)
    public  void testBadUrlException() throws ServerErrorException, UnsupportedEncodingException {
        LoginService loginService = new LoginService();
        loginService.setUsername("test_username");
        loginService.setPassword("test_password");
        loginService.setStreamlineService(streamlineService);
        loginService.setPostBuilder(postBuilder);
        when(postBuilder.buildHttpPost(anyString())).thenReturn(post);
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw new UnsupportedEncodingException();
            }
        }).when(post).setEntity(Matchers.<HttpEntity>any());
        loginService.execute();
    }

    @Test
    public void testGetUsername() throws Exception {
        String testUsername = "TEST_NAME";
        LoginService loginService = new LoginService();
        assertNotNull(loginService.getUsername());
        assertEquals(loginService.getUsername(), "");
        loginService.setUsername(testUsername);
        assertEquals(loginService.getUsername(), testUsername);
    }

    @Test
    public void testSetUsername() throws Exception {
        String testUsername = "TEST_NAME";
        LoginService loginService = new LoginService();
        assertNotNull(loginService.getUsername());
        assertEquals(loginService.getUsername(), "");
        loginService.setUsername(testUsername);
        assertEquals(loginService.getUsername(), testUsername);
    }

    @Test
    public void testGetPassword() throws Exception {
        String testPassword = "TEST_PASSWORD";
        LoginService loginService = new LoginService();
        assertNotNull(loginService.getPassword());
        assertEquals(loginService.getPassword(), "");
        loginService.setUsername(testPassword);
        assertEquals(loginService.getUsername(), testPassword);
    }

    @Test
    public void testSetPassword() throws Exception {
        String testPassword = "TEST_PASSWORD";
        LoginService loginService = new LoginService();
        assertNotNull(loginService.getPassword());
        assertEquals(loginService.getPassword(), "");
        loginService.setUsername(testPassword);
        assertEquals(loginService.getUsername(), testPassword);
    }

    @Test
    public void testGetPostBuilder() throws Exception {
        LoginService loginService = new LoginService();
        assertNotNull(loginService.getPostBuilder());
        loginService.setPostBuilder(postBuilder);
        assertEquals(loginService.getPostBuilder(), postBuilder);
    }

    @Test
    public void testSetPostBuilder() throws Exception {
        LoginService loginService = new LoginService();
        assertNotNull(loginService.getPostBuilder());
        loginService.setPostBuilder(postBuilder);
        assertEquals(loginService.getPostBuilder(), postBuilder);
    }

    @Test
    public void testGetStreamlineService() {
        LoginService loginService = new LoginService();
        assertNotNull(loginService.getStreamlineService());
        loginService.setStreamlineService(streamlineService);
        assertEquals(loginService.getStreamlineService(), streamlineService);
    }

    @Test
    public void testSetStreamlineService() {
        LoginService loginService = new LoginService();
        assertNotNull(loginService.getStreamlineService());
        loginService.setStreamlineService(streamlineService);
        assertEquals(loginService.getStreamlineService(), streamlineService);
    }
}
