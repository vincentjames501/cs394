var class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_no_data =
[
    [ "setUp", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_no_data.html#a000255159cda4b161dee3d2425a7a137", null ],
    [ "test_finishItem_with_nonexistent_Id", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_no_data.html#a180a9a73c832deb0686583135cb41680", null ],
    [ "test_finishOrder_with_nonexistent_Id", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_no_data.html#a49cb11d91212d45e52869ff5eddc0582", null ],
    [ "test_getCompletedOrders_none", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_no_data.html#a3847d2d724f5060d0715d39495842b88", null ],
    [ "test_getKitchenUpdate_no_itemId", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_no_data.html#a6c8e1c13da4723127f209df96a38557c", null ],
    [ "test_getKitchenUpdate_no_lastUpdateTime", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_no_data.html#a2c2959dd8b24a4436a6f2a0aad14cab9", null ],
    [ "test_getKitchenUpdate_no_orderId", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_no_data.html#a414ea98a8f9a44316a1fd0f4078cca37", null ],
    [ "test_getOrderItems_no_Id", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_no_data.html#aaf4579c529248caa7114813522fea4d3", null ],
    [ "test_kitchen_no_orders", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_no_data.html#a0bcdec3d384dcfcee4f23a08a7727436", null ],
    [ "client", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_no_data.html#afe6dc6d5f5ebb31d4dc40ccdd996c995", null ],
    [ "fixtures", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_no_data.html#a79222ac4e3dc9e35c3a2d6a73ecb2ec5", null ]
];