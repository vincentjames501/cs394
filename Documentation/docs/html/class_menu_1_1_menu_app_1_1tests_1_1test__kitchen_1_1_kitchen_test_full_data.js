var class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_full_data =
[
    [ "setUp", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_full_data.html#a57429aaf90dd796c491a4513f44673c4", null ],
    [ "test_finishItem_completes_order", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_full_data.html#aa924f9f1724870c9f99e2b799e15be1d", null ],
    [ "test_finishItem_does_not_complete_order", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_full_data.html#ae97316062bcfc89795ccccd85f35106f", null ],
    [ "test_finishOrder", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_full_data.html#a40757756ee3d9937c2ffe25afb40a0ec", null ],
    [ "test_getCompletedOrders_multiple", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_full_data.html#a2a1588539bf97e7518684cb317a61b6a", null ],
    [ "test_getKitchenUpdate_complete_item", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_full_data.html#a61e28c31cc418b8192da54f15000cc3b", null ],
    [ "test_getKitchenUpdate_complete_order", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_full_data.html#a3a1e7ec267a2667259cf99490afd0a25", null ],
    [ "test_getOrderItems_multiple_items", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_full_data.html#a8be60353573d5e53fe4ee3acfb8d59b9", null ],
    [ "test_kitchen_many_orders", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_full_data.html#a3bf21aec81c056131a3e49cb61bf5f58", null ],
    [ "client", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_full_data.html#ac2d3197b4ef6d37add5271bdeaf14ba5", null ],
    [ "fixtures", "class_menu_1_1_menu_app_1_1tests_1_1test__kitchen_1_1_kitchen_test_full_data.html#ac436244d2a0859e11b1a2b06391b5e8e", null ]
];