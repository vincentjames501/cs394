var class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management =
[
    [ "setUp", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#ad232e0908748b5f504eb1d11d69e32b6", null ],
    [ "test_apply_coupon_to_order", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#a09743a935798f564cc9872053cd99a8b", null ],
    [ "test_apply_coupon_to_order_negative", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#aaf2396d764636c98514a618ca69cf684", null ],
    [ "test_change_item_price", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#a54e318e5e4e0faecfd0c0a124e84b1d0", null ],
    [ "test_change_item_price_negative", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#a55fc9b985cd69445a00e79b22f097a7d", null ],
    [ "test_close_invalid_order_Id", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#a1dcaa1646db1938e140127551ebce2ff", null ],
    [ "test_close_notFinished", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#a6ccfc3d40ca17bae15bfff13067d06a0", null ],
    [ "test_close_order_by_Id", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#a70ca4edade4e5612969c45dfc45f0e9a", null ],
    [ "test_close_update", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#aeccef70149c0e9961c08f5162430aa00", null ],
    [ "test_get_order_items", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#ade7406ec8106321dbdea255a4a461e69", null ],
    [ "test_get_order_update_multiple", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#a1d9bcb54bf10ec181f2de1374364f5d8", null ],
    [ "test_get_order_update_single", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#a8f9957951963b8f37caf23143634ce46", null ],
    [ "test_get_orders", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#ace1cd4aec06f09e9799d8c85adde43ce", null ],
    [ "client", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#a58f9e0d48a55dee52066c73963f906f4", null ],
    [ "fixtures", "class_menu_1_1_menu_app_1_1tests_1_1test__table__management_1_1_table_management.html#ac9b6a03b5d92b63a203624ed57a05572", null ]
];