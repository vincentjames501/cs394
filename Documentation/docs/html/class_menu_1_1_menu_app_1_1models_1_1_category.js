var class_menu_1_1_menu_app_1_1models_1_1_category =
[
    [ "__unicode__", "class_menu_1_1_menu_app_1_1models_1_1_category.html#a149a3666b2d978b496df7fc995e364e6", null ],
    [ "getItems", "class_menu_1_1_menu_app_1_1models_1_1_category.html#a637c20acb5dd450f2c72d6ce6cc336c8", null ],
    [ "getMenu", "class_menu_1_1_menu_app_1_1models_1_1_category.html#ac7595d027dad13f76a32039d8763b230", null ],
    [ "getMenuPage", "class_menu_1_1_menu_app_1_1models_1_1_category.html#aebccc496c7814d1719533d3289a30818", null ],
    [ "getRestaurant", "class_menu_1_1_menu_app_1_1models_1_1_category.html#a35f5c30ca7f02bac574dcc48d6ebac40", null ],
    [ "menuPage", "class_menu_1_1_menu_app_1_1models_1_1_category.html#a504f58d7cb2ec960ff13d2c72500f873", null ],
    [ "name", "class_menu_1_1_menu_app_1_1models_1_1_category.html#a37534d956ca0e43359a106e396000308", null ]
];