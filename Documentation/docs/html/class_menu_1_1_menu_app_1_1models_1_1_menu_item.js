var class_menu_1_1_menu_app_1_1models_1_1_menu_item =
[
    [ "__unicode__", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#a81e88bcf5702af7fde36b44fd80265c7", null ],
    [ "getMenu", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#a21c9c81a276e07f92857728cd6b63644", null ],
    [ "getMenuPage", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#a17d58892bceba04cf67b6024b088822a", null ],
    [ "getRestaurant", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#a9d222928afee85999ce2d8f5833aa124", null ],
    [ "additionalImages", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#ae16169846347e43a05e7930dfc39113b", null ],
    [ "category", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#a444ec8ac7c05a5f3ac2bacc83669ffbb", null ],
    [ "image", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#a5b2ba557269cd7272e2f7d739edb2c17", null ],
    [ "longDescription", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#a06452083808217cdf9eacf7d8b5b9ec0", null ],
    [ "name", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#abe4c86c96fa72e4c50cc78bb319e768a", null ],
    [ "price", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#a2f3263c2d8ed3f2da3cc42bce6e423ba", null ],
    [ "rating", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#a2d5963dffe02473f8a69cfd3c335fec5", null ],
    [ "shortDescription", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#a3e6a0ebadebc91d8d8df28e5814e91fa", null ],
    [ "timesOrdered", "class_menu_1_1_menu_app_1_1models_1_1_menu_item.html#ad08e6fec1da349ea400c0f746056fd87", null ]
];