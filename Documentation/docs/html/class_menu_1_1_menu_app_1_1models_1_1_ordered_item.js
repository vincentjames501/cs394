var class_menu_1_1_menu_app_1_1models_1_1_ordered_item =
[
    [ "__unicode__", "class_menu_1_1_menu_app_1_1models_1_1_ordered_item.html#ac40587037b909f2303f03cb86b67b6e9", null ],
    [ "getOrder", "class_menu_1_1_menu_app_1_1models_1_1_ordered_item.html#a07f086d6c8274e8a1b18a341a6af5e89", null ],
    [ "comments", "class_menu_1_1_menu_app_1_1models_1_1_ordered_item.html#a1ba664378aa3a3ff02db403d72f6584c", null ],
    [ "item", "class_menu_1_1_menu_app_1_1models_1_1_ordered_item.html#a65cd0926a2ceb70b5d73c5f1e352f713", null ],
    [ "itemPrice", "class_menu_1_1_menu_app_1_1models_1_1_ordered_item.html#ad33243213c9c38e13f66748449de9810", null ],
    [ "itemPriceCharts", "class_menu_1_1_menu_app_1_1models_1_1_ordered_item.html#a73f009d16e6ff0b021c9e3681d14e346", null ],
    [ "order", "class_menu_1_1_menu_app_1_1models_1_1_ordered_item.html#a8459782b45af7c290cd6589cff33a4ae", null ],
    [ "timePrepared", "class_menu_1_1_menu_app_1_1models_1_1_ordered_item.html#a14f869fd4a0902ad19c54c01e79f816a", null ]
];