var class_menu_1_1_menu_app_1_1models_1_1_order =
[
    [ "__unicode__", "class_menu_1_1_menu_app_1_1models_1_1_order.html#ae4d074280d7953613b8619b5f29233a0", null ],
    [ "getOrderedItems", "class_menu_1_1_menu_app_1_1models_1_1_order.html#a17a2874dd8e06861f550741b3188c3ae", null ],
    [ "discount", "class_menu_1_1_menu_app_1_1models_1_1_order.html#abc8b0abd2c7a7488c2574dbabd9a221a", null ],
    [ "table", "class_menu_1_1_menu_app_1_1models_1_1_order.html#a989ec4371789a99e75b0392516e2b79f", null ],
    [ "timeFinished", "class_menu_1_1_menu_app_1_1models_1_1_order.html#a4c684c7214f1896202d102dd3e92b2d4", null ],
    [ "timePaid", "class_menu_1_1_menu_app_1_1models_1_1_order.html#aa315195c3e12f3df120e5d52aa00c22f", null ],
    [ "timePlaced", "class_menu_1_1_menu_app_1_1models_1_1_order.html#a1b233302273a8de59d043b02bd7cb687", null ],
    [ "totalDiscount", "class_menu_1_1_menu_app_1_1models_1_1_order.html#a31b5a75a78ada33efb3172aca98a9df3", null ],
    [ "totalPrice", "class_menu_1_1_menu_app_1_1models_1_1_order.html#a53db47a94e6a5262cdc457b8418b287f", null ],
    [ "waiter", "class_menu_1_1_menu_app_1_1models_1_1_order.html#a5895acbf53cbe831b88008dd2dd9655f", null ]
];