var class_menu_1_1_menu_app_1_1models_1_1_restaurant =
[
    [ "__unicode__", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#a901aec676e774b4e342b713f1c705be4", null ],
    [ "getCategories", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#a7edd2f73c0e2690720c0d1b1b8af09f9", null ],
    [ "getMenu", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#a6414c5e5ca677a9c1461a39868c399f3", null ],
    [ "getMenuItems", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#acb6bda2ae998fa3e1610ea042424c788", null ],
    [ "getMenuPages", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#a47723054001b37497fbc98839b7d76cb", null ],
    [ "getNutritionFacts", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#ab7a5eceb0e4b001d3c9cb415ed7dbb10", null ],
    [ "getRestaurantUsers", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#a33844075982c456b8278628913e7f685", null ],
    [ "city", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#a78b0568e118a6d7f9db1792c9bb649e5", null ],
    [ "federalSalesTax", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#a8eef6b8c8c98891408862955fcfd32fd", null ],
    [ "name", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#ab3568cafcde2f615c5110cdbc11c1988", null ],
    [ "state", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#a4fb1d28b0fca2963d7d6cbf7a4a8f204", null ],
    [ "stateSalesTax", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#a346597a920a431793a90cbba4c9da855", null ],
    [ "street", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#a1534e9119a7c714d1be3368042b7ce31", null ],
    [ "zipcode", "class_menu_1_1_menu_app_1_1models_1_1_restaurant.html#a49fd78e3a9c3caf404932c0f58a0ef1f", null ]
];